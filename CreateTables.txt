CREATE TABLE AuthTokens (
    AuthToken varchar(255) not null,
    Username varchar(255) not null
);

CREATE TABLE Events (
    EventId varchar(255) not null,
    Descendant varchar(255) not null,
    Person varchar(255) not null,
    Latitude decimal(9,6) not null,
    Longitude decimal(9,6) not null,
    Country varchar(255) not null,
    City varchar(255) not null,
    EventType varchar(255) not null,
    Year int not null
);

CREATE TABLE Persons (
    PersonId varchar(255) not null,
    Descendant varchar(255) not null,
    FirstName varchar(255) not null,
    LastName varchar(255) not null,
    Gender char(1),
    Father varchar(255),
    Mother varchar(255),
    Spouse varchar(255)
);

CREATE TABLE Users (
    Username varchar(255) not null,
    Password varchar(255) not null,
    Email varchar(255) not null,
    FirstName varchar(255) not null,
    LastName varchar(255) not null,
    Gender char(1) not null,
    PersonId varchar(255) not null
);