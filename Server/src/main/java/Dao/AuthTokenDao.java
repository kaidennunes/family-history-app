package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Models.AuthToken;

/**
 * Database object class for authentication tokens.
 * Created by Kaiden on 2/10/2018.
 */

public class AuthTokenDao extends Dao {

    /**
     * Default constructor for the class.
     */
    public AuthTokenDao() {

    }

    /**
     * Adds an authentication token to the database.
     *
     * @param authToken The authentication token to be added.
     * @return Returns true if successful, false otherwise.
     */
    public boolean addAuthToken(AuthToken authToken) {
        boolean addedAuthToken = false;
        try {
            String sql = " INSERT INTO AuthTokens (AuthToken, Username) values (?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, authToken.getAuthToken());
            preparedStmt.setString(2, authToken.getUserName());
            try {
                // Execute the preparedstatement
                preparedStmt.execute();
                addedAuthToken = true;
            } catch (SQLException ex) {
                System.out.println("There was a problem adding an authentication token.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return addedAuthToken;
    }

    /**
     * Deletes all authentication tokens in the database.
     *
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllAuthTokens() {
        boolean deletedAuthTokens = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM AuthTokens";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedAuthTokens = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all authentication tokens.");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedAuthTokens;
    }

    /**
     * Finds the username to whom the given authentication token belongs.
     * Also has the effect of determining whether the given authentication token is valid.
     *
     * @param authToken The authentication token to be evaluated.
     * @return Returns the username to whom the authentication token belongs, null if the token isn't in the database.
     */
    public String getAuthToken(String authToken) {
        String userName = null;
        try {
            String query = "SELECT Username FROM AuthTokens WHERE AuthToken = '" + authToken + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                while (rs.next()) {
                    userName = rs.getString("Username");
                }
            } catch (SQLException e) {
                System.out.println("There was an error when checking for the authentication token.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return userName;
    }
}
