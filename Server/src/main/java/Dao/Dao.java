package Dao;

import java.sql.*;
/**
 * Base class for all database object classes.
 * Created by Kaiden on 2/13/2018.
 */

public class Dao {

    static {
        try {
            final String driver = "org.sqlite.JDBC";
            Class.forName(driver);
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Connection to the database.
     */
    protected Connection conn;

    /**
     * Opens a connection to the database.
     * @throws SQLException Throws an exception if there is some problem in the database.
     */
    public void openConnection() throws SQLException {
        openConnectionToDatabase("jdbc:sqlite:Server/FamilyMap.db");
    }

    /**
     * Opens a connection to the database. Used for testing, so as to open the test database.
     * @throws SQLException Throws an exception if there is some problem in the database.
     */
    public void openTestConnection() throws SQLException {
        openConnectionToDatabase("jdbc:sqlite:Server/FamilyMapTest.db");
    }

    /**
     * Opens a connection to the database. Used to implement both the actual and test database
     * @throws SQLException Throws an exception if there is some problem in the database.
     */
    private void openConnectionToDatabase(String connectionURL) throws SQLException {
        try {
            final String CONNECTION_URL = connectionURL;

            // Open a database connection
            conn = DriverManager.getConnection(CONNECTION_URL);

            // Start a transaction
            conn.setAutoCommit(false);
        }
        catch (SQLException e) {
            throw e;
        }
    }


    /**
     * Closes the connection to the database.
     * @param commit Boolean value as to whether to commit the changes made with this connection.
     * @throws SQLException Throws an exception if there is some problem in the database.
     */
    public void closeConnection(boolean commit) throws SQLException {
        try {
            if (commit) {
                conn.commit();
            }
            else {
                conn.rollback();
            }

            conn.close();
            conn = null;
        }
        catch (SQLException e) {
            throw e;
        }
    }
}
