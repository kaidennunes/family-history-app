package Dao;

/**
 * Created by Kaiden on 2/28/2018.
 */

public class Database {

    public UserDao userDao = new UserDao();
    public AuthTokenDao authTokenDao = new AuthTokenDao();
    public EventDao eventDao = new EventDao();
    public PersonDao personDao = new PersonDao();

    public Database() {

    }
}
