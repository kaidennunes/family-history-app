package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Models.Event;

/**
 * Database object class for events.
 * Created by Kaiden on 2/10/2018.
 */

public class EventDao extends Dao {

    /**
     * Default constructor for the class.
     */
    public EventDao() {

    }

    /**
     * Adds an event to the database.
     * @param event The event to be added.
     * @return Returns true if successful, false otherwise.
     */
    public boolean addEvent(Event event) {
        boolean addedEvent = false;
        try {
            String sql = " INSERT INTO Events (EventId, Descendant, Person, Latitude, Longitude, Country, City, EventType, Year) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString (1, event.getEventId());
            preparedStmt.setString (2, event.getDescendant());
            preparedStmt.setString   (3, event.getPerson());
            preparedStmt.setDouble   (4, event.getLatitude());
            preparedStmt.setDouble   (5, event.getLongitude());
            preparedStmt.setString (6, event.getCountry());
            preparedStmt.setString   (7, event.getCity());
            preparedStmt.setString   (8, event.getEventType());
            preparedStmt.setInt   (9, event.getYear());

            try {
                // Execute the preparedstatement
                preparedStmt.execute();
                addedEvent = true;
            } catch (SQLException ex) {
                System.out.println("There was a problem adding an event.");
            }
            finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        }
        catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return addedEvent;
    }

    /**
     * Deletes all events in the database.
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllEvents() {
        boolean deletedEvents = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM Events";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedEvents = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all events.");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedEvents;
    }

    /**
     * Deletes all events associated with the given username.
     * @param username The descendant (by username) whose associated events are to be deleted. Internally, this is the Descendant column.
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllEventsForUsername(String username) {
        boolean deletedEvents = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM Events WHERE Descendant = '" + username + "'";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedEvents = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all events for " + username + ".");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedEvents;
    }

    /**
     * Finds the event information for the given event ID.
     * @param eventId The ID of the event that is to be found.
     * @return Returns the event to be found in the database, null if not in database.
     */
    public Event getEvent(String eventId) {
        Event event = null;
        try {
            String query = "SELECT EventId, Descendant, Person, Latitude, Longitude, Country, City, EventType, Year FROM Events WHERE EventId = '" + eventId + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                while ( rs.next() ) {
                    String databaseEventId = rs.getString("EventId");
                    String databaseDescendant = rs.getString("Descendant");
                    String databasePerson = rs.getString("Person");
                    double databaseLatitude = rs.getDouble("Latitude");
                    double databaseLongitude = rs.getDouble("Longitude");
                    String databaseCountry = rs.getString("Country");
                    String databaseCity = rs.getString("City");
                    String databaseEventType = rs.getString("EventType");
                    int databaseYear = rs.getInt("Year");

                    event = new Event(databaseEventId, databaseDescendant, databasePerson, databaseLatitude, databaseLongitude, databaseCountry, databaseCity, databaseEventType, databaseYear);
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when checking for the event ID.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return event;
    }

    /**
     * Gets all event data for a user.
     * @param userName The username of the user (internally, this is the Descendant column).
     * @return Returns an array of event objects associated with the user, null if there are none.
     */
    public Event[] getAllEventsForUsername(String userName) {
        Event[] events = null;
        try {
            String query = "SELECT EventId, Descendant, Person, Latitude, Longitude, Country, City, EventType, Year FROM Events WHERE Descendant = '" + userName + "'";
            String countQuery = "SELECT COUNT(*) FROM Events WHERE Descendant = '" + userName + "'";
            PreparedStatement preparedStmt = null;
            PreparedStatement countPreparedStmt = null;

            try {
                preparedStmt = conn.prepareStatement(query);
                countPreparedStmt = conn.prepareStatement(countQuery);
                ResultSet rs = preparedStmt.executeQuery();
                ResultSet countRs = countPreparedStmt.executeQuery();
                countRs.next();
                int numberOfEvents = countRs.getInt(1);
                if (numberOfEvents > 0) events = new Event[numberOfEvents];
                int arrayIndex = 0;
                while ( rs.next() ) {
                    String databaseEventId = rs.getString("EventId");
                    String databaseDescendant = rs.getString("Descendant");
                    String databasePerson = rs.getString("Person");
                    double databaseLatitude = rs.getDouble("Latitude");
                    double databaseLongitude = rs.getDouble("Longitude");
                    String databaseCountry = rs.getString("Country");
                    String databaseCity = rs.getString("City");
                    String databaseEventType = rs.getString("EventType");
                    int databaseYear = rs.getInt("Year");

                    Event newEvent = new Event(databaseEventId, databaseDescendant, databasePerson, databaseLatitude, databaseLongitude, databaseCountry, databaseCity, databaseEventType, databaseYear);
                    events[arrayIndex++] = newEvent;
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when searching for all events associated with " + userName + ".");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
                if (countPreparedStmt != null) {
                    countPreparedStmt.close();
                    countPreparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return events;
    }
}
