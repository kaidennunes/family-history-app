package Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Models.Person;

/**
 * Database object class for people.
 * Created by Kaiden on 2/10/2018.
 */

public class PersonDao extends Dao {

    /**
     * Default constructor for the class.
     */
    public PersonDao() {

    }
    /**
     * Adds a person to the database.
     * @param person The person to be added.
     * @return Returns true if successful, false otherwise.
     */
    public boolean addPerson(Person person) {
        boolean addedPerson = false;
        try {
            String sql = " INSERT INTO Persons (PersonId, Descendant, FirstName, LastName, Gender, Father, Mother, Spouse) values (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString (1, person.getPersonId());
            preparedStmt.setString (2, person.getDescendant());
            preparedStmt.setString   (3, person.getFirstName());
            preparedStmt.setString   (4, person.getLastName());
            preparedStmt.setString   (5, String.valueOf(person.getGender()));
            preparedStmt.setString (6, person.getFather());
            preparedStmt.setString   (7, person.getMother());
            preparedStmt.setString   (8, person.getSpouse());
            try {
                // Execute the preparedstatement
                preparedStmt.execute();
                addedPerson = true;
            } catch (SQLException ex) {
                System.out.println("There was a problem adding a person.");
            }
            finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        }
        catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return addedPerson;
    }

    /**
     * Deletes all people in the database.
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllPeople() {
        boolean deletedPeople = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM Persons";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedPeople = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all people.");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedPeople;
    }

    /**
     * Deletes all people associated with the given username.
     * @param userName The descendant (by username) whose associated people are to be deleted. Internally, this is the Descendant column.
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllPeopleForUsername(String userName) {
        boolean deletedPeople = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM Persons WHERE Descendant = '" + userName + "'";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedPeople = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all events for " + userName + ".");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedPeople;
    }

    /**
     * Finds the person information for the given person ID.
     * @param personId The ID of the person that is to be found.
     * @return Returns the person to be found in the database, null if not in database.
     */
    public Person getPerson(String personId) {
        Person person = null;
        try {
            String query = "SELECT PersonId, Descendant, FirstName, LastName, Gender, Father, Mother, Spouse FROM Persons WHERE personId = '" + personId + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                while ( rs.next() ) {
                    String databasePersonId = rs.getString("PersonId");
                    String databaseDescendant = rs.getString("Descendant");
                    String databaseFirstName = rs.getString("FirstName");
                    String databaseLastName = rs.getString("LastName");
                    char databaseGender = rs.getString("Gender").charAt(0);
                    String databaseFather = rs.getString("Father");
                    String databaseMother = rs.getString("Mother");
                    String databaseSpouse = rs.getString("Spouse");

                    person = new Person(databasePersonId, databaseDescendant, databaseFirstName, databaseLastName, databaseGender, databaseFather, databaseMother, databaseSpouse);
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when checking for the person ID.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return person;
    }

    /**
     * Gets all person data for a user.
     * @param userName The descendant (by username) whose associated people are to be found. Internally, this is the Descendant column.
     * @return Returns an array of person objects associated with the user, null if there are none.
     */
    public Person[] getAllPeopleForUsername(String userName) {
        Person[] people = null;
        try {
            String query = "SELECT PersonId, Descendant, FirstName, LastName, Gender, Father, Mother, Spouse FROM Persons WHERE Descendant = '" + userName + "'";
            String countQuery = "SELECT COUNT(*) FROM Persons WHERE Descendant = '" + userName + "'";
            PreparedStatement preparedStmt = null;
            PreparedStatement countPreparedStmt = null;

            try {
                preparedStmt = conn.prepareStatement(query);
                countPreparedStmt = conn.prepareStatement(countQuery);
                ResultSet rs = preparedStmt.executeQuery();
                ResultSet countRs = countPreparedStmt.executeQuery();
                countRs.next();
                int numberOfPeople = countRs.getInt(1);
                if (numberOfPeople > 0) people = new Person[numberOfPeople];
                int arrayIndex = 0;
                while ( rs.next() ) {
                    String databasePersonId = rs.getString("PersonId");
                    String databaseDescendant = rs.getString("Descendant");
                    String databaseFirstName = rs.getString("FirstName");
                    String databaseLastName = rs.getString("LastName");
                    char databaseGender = rs.getString("Gender").charAt(0);
                    String databaseFather = rs.getString("Father");
                    String databaseMother = rs.getString("Mother");
                    String databaseSpouse = rs.getString("Spouse");

                    Person newPerson = new Person(databasePersonId, databaseDescendant, databaseFirstName, databaseLastName, databaseGender, databaseFather, databaseMother, databaseSpouse);
                    people[arrayIndex++] = newPerson;
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when searching for all people associated with " + userName + ".");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
                if (countPreparedStmt != null) {
                    countPreparedStmt.close();
                    countPreparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return people;
    }
}
