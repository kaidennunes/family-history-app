package Dao;

import java.sql.*;
import Models.User;

/**
 * Database object class for users.
 * Created by Kaiden on 2/10/2018.
 */

public class UserDao extends Dao {

    /**
     * Default constructor for the class.
     */
    public UserDao() {

    }

    /**
     * Determines whether a username is already in the database.
     * @param userName The username to check for.
     * @return True if the username is already in the database, false otherwise.
     */
    public boolean existUserName(String userName) {
        boolean exists = false;
        try {
            String query = "SELECT Username FROM Users WHERE Username = '" + userName + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                if (rs.isBeforeFirst() ) {
                    exists = true;
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when checking for the username.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return exists;
    }

    /**
     * Adds a user to the database.
     * @param user The user to be added.
     * @return Returns true if successful, false otherwise.
     */
    public boolean addUser(User user) {
        boolean addedUser = false;
        try {
            String sql = " INSERT INTO Users (Username, Password, Email, FirstName, LastName, Gender, PersonId) values (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString (1, user.getUserName());
            preparedStmt.setString (2, user.getUserPassword());
            preparedStmt.setString   (3, user.getEmail());
            preparedStmt.setString   (4, user.getFirstName());
            preparedStmt.setString   (5, user.getLastName());
            preparedStmt.setString (6, String.valueOf(user.getGender()));
            preparedStmt.setString   (7, user.getPersonId());

            try {
                // Execute the preparedstatement
                preparedStmt.execute();
                addedUser = true;
            } catch (SQLException ex) {
                System.out.println("There was a problem adding a user.");
            }
            finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        }
        catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return addedUser;
    }

    /**
     * Deletes all users in the database.
     * @return Returns true if successful, false otherwise.
     */
    public boolean deleteAllUsers() {
        boolean deletedUsers = false;
        try {
            Statement stmt = conn.createStatement();
            // Use DELETE
            String sql = "DELETE FROM Users";
            try {
                // Execute deletion
                stmt.executeUpdate(sql);
                deletedUsers = true;
            } catch (SQLException ex) {
                System.out.println("There was an error when deleting all users.");
            } finally {
                if (stmt != null) {
                    stmt.close();
                    stmt = null;
                }
            }
        } catch (SQLException ex) {
            System.out.println("There was an unexpected error.");
        }
        return deletedUsers;
    }

    /**
     * Determines whether the user (who is logging in) exists in the database (is a valid user).
     * @param userName Username of the user.
     * @param password Password of the user.
     * @return Returns true if the username and password combination is in the database, false if otherwise.
     */
    public boolean isValidUser(String userName, String password) {
        boolean isValid = false;
        try {
            String query = "SELECT Username FROM Users WHERE Username = '" + userName + "' AND Password = '" + password + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                if (rs.isBeforeFirst() ) {
                    isValid = true;
                }
            } catch (SQLException e ) {
                System.out.println("There was an error when checking for the username.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return isValid;
    }

    /**
     * Gets the user information for the given username.
     * @param userName The username of the user that is to be found.
     * @return Returns the user to be found in the database, null if not in database.
     */
    public User getUser(String userName) {
        User user = null;
        try {
            String query = "SELECT Username, Password, Email, FirstName, LastName, Gender, PersonId FROM Users WHERE Username = '" + userName + "'";
            PreparedStatement preparedStmt = null;
            try {
                preparedStmt = conn.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                while ( rs.next() ) {
                    String databaseUserName = rs.getString("Username");
                    String databasePassword = rs.getString("Password");
                    String databaseEmail = rs.getString("Email");
                    String databaseFirstName = rs.getString("FirstName");
                    String databaseLastName = rs.getString("LastName");
                    char databaseGender = rs.getString("Gender").charAt(0);
                    String databasePersonId = rs.getString("PersonId");
                    user = new User(databaseUserName, databasePassword, databaseEmail, databaseFirstName,databaseLastName, databaseGender, databasePersonId);
                }

            } catch (SQLException e ) {
                System.out.println("There was an error when checking for the username.");
            } finally {
                if (preparedStmt != null) {
                    preparedStmt.close();
                    preparedStmt = null;
                }
            }
        } catch (SQLException e) {
            System.out.println("There was an error!");
        }
        return user;
    }
}
