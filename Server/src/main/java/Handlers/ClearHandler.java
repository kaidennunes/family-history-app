package Handlers;
import java.io.*;
import java.net.*;
import com.sun.net.httpserver.*;

import Response.ClearResponse;
import Services.ClearService;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class ClearHandler extends Handler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a post request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("post")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Create an instance of the clear service
            ClearService clearService = new ClearService();

            // Clear the database
            ClearResponse clearResponse = clearService.clear();

            // Convert the ClearResponse to json
            String jsonResponseData = convertToJson(clearResponse);

            // Send the HTTP response to the client
            if (!clearResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    private String convertToJson(ClearResponse clearResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (clearResponse.getHasError()) {
            json.append("{ \"message\": \"" + clearResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, we will convert the encapsulated data into json
        json.append("{ \"message\": \"" + clearResponse.getMessage() + "\"}");

        // Return the json response data
        return json.toString();
    }
}
