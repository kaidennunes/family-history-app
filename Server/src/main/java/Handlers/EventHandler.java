package Handlers;
import java.io.*;
import java.net.*;
import java.util.Arrays;

import com.sun.net.httpserver.*;

import Models.Event;
import Requests.EventRequest;
import Response.EventResponse;
import Services.EventService;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class EventHandler extends Handler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a get request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("get")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the HTTP request headers
            Headers reqHeaders = exchange.getRequestHeaders();
            // If an "Authorization" header is not present, then deny the user
            if (!reqHeaders.containsKey("Authorization")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Extract the authentication token from the "Authorization" header
            String authToken = reqHeaders.getFirst("Authorization");

            // Get the parameters from the URI
            String requestData = exchange.getRequestURI().toString();

            // Parse out the parameters
            String[] request = parseURIRequest(requestData);

            // Initialize the EventResponse that will hold either an error or success message
            EventResponse eventResponse;

            // If the person parameter is the only thing in the uri, then get all the events of all people related to the logged in user
            if (request.length == 1) {
                // Encapsulate the data in a EventRequest
                EventRequest eventRequest = new EventRequest(authToken);

                // Create an instance of the event service with the encapsulated data
                EventService eventService = new EventService(eventRequest);

                // Get all events in the family history of the logged in user
                eventResponse = eventService.getEvents();
            }
            // Otherwise, get find the event specified in the uri parameters (the second index of the request data should be the event ID)
            else {
                // Encapsulate the data in a EventRequest
                EventRequest eventRequest = new EventRequest(authToken, request[1]);

                // Create an instance of the event service with the encapsulated data
                EventService eventService = new EventService(eventRequest);

                // Get the event with the event ID that the logged in user wants
                eventResponse = eventService.getEvent();
            }

            // Convert the EventResponse to json
            String jsonResponseData = convertToJson(eventResponse);

            // Send the HTTP response to the client
            if (!eventResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    /**
     * Parses out the first two fields of the url parameter.
     *
     * @param requestData The parameters given in the uri.
     * @return Returns an array of Strings with the information. The first index is the event method request.
     */
    private String[] parseURIRequest(String requestData) {
        if (requestData == null) {
            return null;
        }
        String cutRequestData;
        if (requestData.charAt(0) == '/' && requestData.length() > 1) {
            cutRequestData = requestData.substring(1);
        } else {
            cutRequestData = requestData;
        }
        String[] postRequest = cutRequestData.split("/");
        String[] result = new String[postRequest.length];
        for (int i = 0; i < postRequest.length; i++) {
            result[i] = postRequest[i];
        }
        return result;
    }

    private String convertToJson(EventResponse eventResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (eventResponse.getHasError()) {
            json.append("{ \"message\": \"" + eventResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, get the array of event objects
        Event[] events = eventResponse.getEvents();

        // If the array is null or empty, return an empty json array
        if (events == null || events.length == 0) {
            json.append("{\"data\": []}");
            return json.toString();
        }

        // Convert the encapsulated data into json
        for (int i = 0; i < events.length; i++) {
            // If a json object was already added, then put a comma after it
            if (i > 0) {
                json.append(",");
            }
            json.append("{ \"descendant\": \"" + events[i].getDescendant() + "\",");
            json.append("\"eventID\": \"" + events[i].getEventId() + "\",");
            json.append("\"personID\": \"" + events[i].getPerson() + "\",");
            json.append("\"latitude\": \"" + events[i].getLatitude() + "\",");
            json.append("\"longitude\": \"" + events[i].getLongitude() + "\",");
            json.append("\"country\": \"" + events[i].getCountry() + "\",");
            json.append("\"city\": \"" + events[i].getCity() + "\",");
            json.append("\"eventType\": \"" + events[i].getEventType() + "\",");
            json.append("\"year\": \"" + events[i].getYear() + "\"}");
        }

        // If there are multiple objects in the array, encase the json data into an array
        if (events.length > 1) {
            json.insert(0, "{ \"data\": [");
            json.append("]}");

        }

        // Return the json response data
        return json.toString();
    }
}
