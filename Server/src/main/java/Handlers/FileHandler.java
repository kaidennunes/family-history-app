package Handlers;

import java.io.*;
import java.net.*;
import java.nio.file.Files;

import com.google.gson.*;
import com.sun.net.httpserver.*;

/**
 * Created by Kaiden on 2/22/2018.
 */

public class FileHandler extends Handler implements HttpHandler{
    private final String FILE_DIR = "Server/web";

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a get request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("get")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the url of the requested file
            String url = exchange.getRequestURI().toString();
            if (url == null || url.equals("/")) {
                url = "/index.html";
            }

            String path = FILE_DIR + url;
            File file = new File(path);

            // If the file doesn't exist or can't be read, return an error page
            if (!file.exists() || !file.canRead()) {
                url = "/HTML/404.html";
                path = FILE_DIR + url;
                file = new File(path);
            }

            // Send the HTTP response to the client
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

            // Get the response body of the http request
            OutputStream responseBody = exchange.getResponseBody();

            // Send the requested file
            Files.copy(file.toPath(),responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (Exception ex) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Close the response body
            exchange.getResponseBody().close();

            // Display/log the stack trace
            ex.printStackTrace();
        }
    }
}
