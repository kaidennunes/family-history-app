package Handlers;

import java.io.*;
import java.net.*;
import java.util.Arrays;

import com.sun.net.httpserver.*;

import Requests.FillRequest;
import Response.FillResponse;
import Services.FillService;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class FillHandler extends Handler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a post request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("post")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the post parameters from the URI
            String requestData = exchange.getRequestURI().toString();

            // Parse out the parameters
            String[] request = parseURIRequest(requestData);


            // Initialize the FillResponse that will hold either an error or success message
            FillResponse fillResponse;

            // If the post request information is invalid, make an error fill response, otherwise, continue to fulfill the request
            if (!isValidRequest(request)) {
                fillResponse = new FillResponse("Invalid username or generations parameter");
            } else if (request.length == 2) {
                // Encapsulate the data in a FillRequest (using default generation parameters)
                FillRequest fillRequest = new FillRequest(request[1]);

                // Create an instance of the fill service with the encapsulated data
                FillService fillService = new FillService(fillRequest);

                // Fill in the family history of the person
                fillResponse = fillService.fill();
            } else {
                // Encapsulate the data in a FillRequest
                FillRequest fillRequest = new FillRequest(request[1], Integer.parseInt(request[2]));

                // Create an instance of the fill service with the encapsulated data
                FillService fillService = new FillService(fillRequest);

                // Fill in the family history of the person
                fillResponse = fillService.fill();
            }

            // Convert the FillResponse to json
            String jsonResponseData = convertToJson(fillResponse);

            // Send the HTTP response to the client
            if (!fillResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    /**
     * Takes the uri parameters and sees if they are valid for the fill request.
     *
     * @param request The array of the uri parameters.
     * @return True if the parameters are valid, false otherwise.
     */
    private boolean isValidRequest(String[] request) {
        // There should be two or three indexes, but if there are more, it doesn't matter
        if (request.length < 2) {
            return false;
        }
        // The third index (if it exists) should be a non-negative integer (0 is acceptable)
        try {
            if (request.length == 2) {
                return true;
            }
            if (Integer.parseInt(request[2]) < 0) {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * Parses out the first two fields of the url parameter.
     *
     * @param requestData The parameters given in the url.
     * @return Returns an array of Strings with the post information. The first index is the fill method request.
     */
    private String[] parseURIRequest(String requestData) {
        if (requestData == null) {
            return null;
        }
        String cutRequestData;
        if (requestData.charAt(0) == '/' && requestData.length() > 1) {
            cutRequestData = requestData.substring(1);
        } else {
            cutRequestData = requestData;
        }
        String[] postRequest = cutRequestData.split("/");
        String[] result = new String[postRequest.length];
        for (int i = 0; i < postRequest.length; i++) {
            result[i] = postRequest[i];
        }
        return result;
    }

    private String convertToJson(FillResponse fillResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (fillResponse.getHasError()) {
            json.append("{ \"message\": \"" + fillResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, we will convert the encapsulated data into json
        json.append("{ \"message\": \"" + fillResponse.getMessage() + "\"}");

        // Return the json response data
        return json.toString();
    }
}
