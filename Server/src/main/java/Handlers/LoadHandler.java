package Handlers;
import java.io.*;
import java.net.*;

import com.google.gson.*;
import com.sun.net.httpserver.*;

import Models.Event;
import Models.Person;
import Models.User;
import Requests.LoadRequest;
import Requests.LoginRequest;
import Response.LoadResponse;
import Services.LoadService;

import static Json.JsonConverter.convertJsonToObject;
import static Json.JsonConverter.convertObjectToJson;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class LoadHandler extends Handler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a post request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("post")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the request body input stream
            InputStream requestBody = exchange.getRequestBody();

            // Read JSON string from the input stream
            String jsonRequestData = readString(requestBody);

            // Encapsulate the data in a LoadRequest
            LoadRequest loadRequest = convertFromJson(jsonRequestData);

            // Create an instance of the load service with the encapsulated data
            LoadService loadService = new LoadService(loadRequest);

            // Load the given information into the database
            LoadResponse loadResponse = loadService.load();

            // Convert the RegisterResponse to json
            String jsonResponseData = convertToJson(loadResponse);

            // Send the HTTP response to the client
            if (!loadResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    private LoadRequest convertFromJson(String jsonRequestData) {
        // Create an instance of the class
        LoadRequest loadRequest = new LoadRequest(null,null,null);
            loadRequest = (LoadRequest) convertJsonToObject(jsonRequestData, loadRequest);

        // Return the properties encapsulated as a LoginRequest
        return loadRequest;
    }

    private String convertToJson(LoadResponse loadResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (loadResponse.getHasError()) {
            json.append("{ \"message\": \"" + loadResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, we will convert the encapsulated data into json
        json.append("{ \"message\": \"" + loadResponse.getMessage() + "\"}");

        // Return the json response data
        return json.toString();
    }
}
