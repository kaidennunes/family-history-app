package Handlers;

import java.io.*;
import java.net.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.*;

import Requests.LoginRequest;
import Response.LoginResponse;
import Services.LoginService;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class LoginHandler extends Handler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a post request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("post")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the HTTP request headers
            Headers reqHeaders = exchange.getRequestHeaders();

            // If an "Authorization" header is not present, deny them
            if (!reqHeaders.containsKey("Authorization")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the request body input stream
            InputStream requestBody = exchange.getRequestBody();

            // Read JSON string from the input stream
            String jsonRequestData = readString(requestBody);

            // Encapsulate the data in a LoginRequest
            LoginRequest loginRequest = convertFromJson(jsonRequestData);

            // Create an instance of the login service with the encapsulated data
            LoginService loginService = new LoginService(loginRequest);

            // Login the user
            LoginResponse loginResponse = loginService.login();

            // Convert the LoginResponse to json
            String jsonResponseData = convertToJson(loginResponse);

            // Send the HTTP response to the client
            if (!loginResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();


        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    private LoginRequest convertFromJson(String jsonRequestData) {
        // Get the json object
        JsonObject jsonObject = new Gson().fromJson(jsonRequestData, JsonObject.class);

        // Find its properties
        String userName = jsonObject.get("userName").getAsString();
        String userPassword = jsonObject.get("password").getAsString();

        // Return the properties encapsulated as a LoginRequest
        return new LoginRequest(userName, userPassword);
    }

    private String convertToJson(LoginResponse loginResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (loginResponse.getHasError()) {
            json.append("{ \"message\": \"" + loginResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, we will convert the encapsulated data into json
        json.append("{ \"authToken\": \"" + loginResponse.getAuthToken() + "\",");
        json.append("\"userName\": \"" + loginResponse.getUserName() + "\",");
        json.append("\"personId\": \"" + loginResponse.getPersonId() + "\"}");

        // Return the json response data
        return json.toString();
    }
}
