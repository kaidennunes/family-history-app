package Handlers;

import java.io.*;
import java.net.*;
import java.util.Arrays;

import com.sun.net.httpserver.*;

import Models.Person;
import Requests.PersonRequest;
import Response.PersonResponse;
import Services.PersonService;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class PersonHandler extends Handler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a get request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("get")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the HTTP request headers
            Headers reqHeaders = exchange.getRequestHeaders();
            // If an "Authorization" header is not present, then deny the user
            if (!reqHeaders.containsKey("Authorization")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Extract the authentication token from the "Authorization" header
            String authToken = reqHeaders.getFirst("Authorization");

            // Get the parameters from the URI
            String requestData = exchange.getRequestURI().toString();

            // Parse out the parameters
            String[] request = parseURIRequest(requestData);

            // Initialize the PersonResponse that will hold either an error or success message
            PersonResponse personResponse;

            // If the person parameter is the only thing in the uri, then get all the family members of the logged in user
            if (request.length == 1) {
                // Encapsulate the data in a PersonRequest
                PersonRequest personRequest = new PersonRequest(authToken);

                // Create an instance of the person service with the encapsulated data
                PersonService personService = new PersonService(personRequest);

                // Get all people in the family history of the logged in user
                personResponse = personService.getPeople();
            }
            // Otherwise, get find the person specified in the uri parameters (the second index of the request data should be the person ID)
            else {
                // Encapsulate the data in a PersonRequest
                PersonRequest personRequest = new PersonRequest(authToken, request[1]);

                // Create an instance of the person service with the encapsulated data
                PersonService personService = new PersonService(personRequest);

                // Get the person with the person ID that the logged in user wants
                personResponse = personService.getPerson();
            }

            // Convert the FillResponse to json
            String jsonResponseData = convertToJson(personResponse);

            // Send the HTTP response to the client
            if (!personResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    /**
     * Parses out the first two fields of the url parameter.
     *
     * @param requestData The parameters given in the url.
     * @return Returns an array of Strings with the information. The first index is the person method request.
     */
    private String[] parseURIRequest(String requestData) {
        if (requestData == null) {
            return null;
        }
        String cutRequestData;
        if (requestData.charAt(0) == '/' && requestData.length() > 1) {
            cutRequestData = requestData.substring(1);
        } else {
            cutRequestData = requestData;
        }
        String[] postRequest = cutRequestData.split("/");
        String[] result = new String[postRequest.length];
        for (int i = 0; i < postRequest.length; i++) {
            result[i] = postRequest[i];
        }
        return result;
    }

    private String convertToJson(PersonResponse personResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (personResponse.getHasError()) {
            json.append("{ \"message\": \"" + personResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, get the array of person objects
        Person[] people = personResponse.getPersons();

        // If the array is null or empty, return an empty json array
        if (people == null || people.length == 0) {
            json.append("{\"data\": []}");
            return json.toString();
        }

        // Convert the encapsulated data into json
        for (int i = 0; i < people.length; i++) {
            // If a json object was already added, then put a comma after it
            if (i > 0) {
                json.append(",");
            }

            // Start creating the json string
            json.append("{ \"descendant\": \"" + people[i].getDescendant() + "\",");
            json.append("\"personID\": \"" + people[i].getPersonId() + "\",");
            json.append("\"firstName\": \"" + people[i].getFirstName() + "\",");
            json.append("\"lastName\": \"" + people[i].getLastName() + "\",");

            // If none of the optional data members is present, add a brace and move onto the next object
            if (people[i].getFather() == null && people[i].getMother() == null && people[i].getSpouse() == null) {
                json.append("\"gender\": \"" + people[i].getGender() + "\"}");
                continue;
            }

            // This is required, so it must be there
            json.append("\"gender\": \"" + people[i].getGender() + "\",");

            // If none of the other optional data members is present, add a brace and move onto the next object. Otherwise, add the data member if it isn't null
            if (people[i].getMother() == null && people[i].getSpouse() == null) {
                json.append("\"father\": \"" + people[i].getFather() + "\"}");
                continue;
            } else if (people[i].getFather() != null) {
                json.append("\"father\": \"" + people[i].getFather() + "\",");
            }

            // If none of the following optional data members is present, add a brace and move onto the next object. Otherwise, add the data member if it isn't null
            if (people[i].getSpouse() == null) {
                json.append("\"mother\": \"" + people[i].getMother() + "\"}");
                continue;
            } else if (people[i].getMother() != null) {
                json.append("\"mother\": \"" + people[i].getMother() + "\",");
            }

            // Add the optional data member if he exists (he always should exist if the code makes it to this point, but for clarity sake, I will add the check)
            if (people[i].getSpouse() != null) {
                json.append("\"spouse\": \"" + people[i].getSpouse() + "\"}");
            }
        }

        // If there are multiple objects in the array, encase the json data into an array
        if (people.length > 1) {
            json.insert(0, "{ \"data\": [");
            json.append("]}");

        }

        // Return the json response data
        return json.toString();
    }
}
