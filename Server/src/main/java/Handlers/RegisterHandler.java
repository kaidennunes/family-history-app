package Handlers;

import java.io.*;
import java.net.*;

import com.google.gson.*;
import com.sun.net.httpserver.*;

import Requests.RegisterRequest;
import Response.RegisterResponse;
import Services.RegisterService;

import static Json.JsonConverter.convertJsonToObject;

/**
 * Created by Kaiden on 2/10/2018.
 */

public class RegisterHandler extends Handler implements HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        try {
            // If it isn't a post request, deny them
            if (!exchange.getRequestMethod().toLowerCase().equals("post")) {
                // Send the HTTP response to the client
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                // Close the response body output stream
                exchange.getResponseBody().close();
                return;
            }

            // Get the request body input stream
            InputStream requestBody = exchange.getRequestBody();

            // Read JSON string from the input stream
            String jsonRequestData = readString(requestBody);

            // Encapsulate the data in a RegisterRequest
            RegisterRequest registerRequest = convertFromJson(jsonRequestData);

            // Create an instance of the register service with the encapsulated data
            RegisterService registerService = new RegisterService(registerRequest);

            // Register the person
            RegisterResponse registerResponse = registerService.register();

            // Convert the RegisterResponse to json
            String jsonResponseData = convertToJson(registerResponse);

            // Send the HTTP response to the client
            if (!registerResponse.getHasError()) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
            } else {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
            }

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Write the JSON string to the output stream
            writeString(jsonResponseData, responseBody);

            // Close the response body output stream
            responseBody.close();

        } catch (IOException e) {
            // Some kind of internal error has occurred inside the server (not the client's fault), so we return an 'internal server error' status code to the client.
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);

            // Get the response body output stream
            OutputStream responseBody = exchange.getResponseBody();

            // Get the json formatted server error message
            String jsonResponseData = serverErrorJson();

            // Write the server error response data to the response body
            writeString(jsonResponseData, responseBody);

            // Close the response body
            responseBody.close();

            // Display/log the stack trace
            e.printStackTrace();
        }
    }

    private RegisterRequest convertFromJson(String jsonRequestData) {
        // Get the json object
        JsonObject jsonObject = new Gson().fromJson(jsonRequestData, JsonObject.class);
        // Create an instance of the class
        RegisterRequest registerRequest = new RegisterRequest(null,null,null,null, null,' ');

     //   registerRequest = (RegisterRequest) convertJsonToObject(jsonRequestData, registerRequest);

        // Find its properties
        String userName = jsonObject.get("userName").getAsString();
        String userPassword = jsonObject.get("password").getAsString();
        String email = jsonObject.get("email").getAsString();
        String firstName = jsonObject.get("firstName").getAsString();
        String lastName = jsonObject.get("lastName").getAsString();
        String genderString = jsonObject.get("gender").getAsString();
        char gender;
        if (genderString == null) {
            gender = ' ';
        } else {
            gender = genderString.charAt(0);
        }

        // Return the properties encapsulated as a RegisterRequest
        return new RegisterRequest(userName, userPassword, email, firstName, lastName, gender);
    }

    private String convertToJson(RegisterResponse registerResponse) {
        StringBuilder json = new StringBuilder();

        // If there is an error, return that error.
        if (registerResponse.getHasError()) {
            json.append("{ \"message\": \"" + registerResponse.getErrorMessage() + "\"}");
            return json.toString();
        }

        // Otherwise, we will convert the encapsulated data into json
        json.append("{ \"authToken\": \"" + registerResponse.getAuthToken() + "\",");
        json.append("\"userName\": \"" + registerResponse.getUserName() + "\",");
        json.append("\"personId\": \"" + registerResponse.getPersonId() + "\"}");

        // Return the json response data
        return json.toString();
    }
}
