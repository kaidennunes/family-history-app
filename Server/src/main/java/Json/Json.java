package Json;

import Models.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static Json.JsonConverter.convertJsonToObject;


/**
 * This class loads in json data from files and stores it to be used for testing and fill purposes.
 * Created by Kaiden on 3/3/2018.
 */

public class Json {
    private final static Path pathFNames = Paths.get("Server/json/fnames.json");
    private final static Path pathMNames = Paths.get("Server/json/mnames.json");
    private final static Path pathSNames = Paths.get("Server/json/snames.json");
    private final static Path pathLocations = Paths.get("Server/json/locations.json");

    private static final Random RANDOM = new Random();
    private static Names maleFirstNames = new Names();
    private static Names femaleFirstNames = new Names();
    private static Names lastNames = new Names();
    private static Locations locations = new Locations();

    /**
     * Reads in the json data into the StringBuilder
     */
    public static void loadJson() {
        StringBuilder jsonFNames = new StringBuilder();
        // Read in the female first names
        try {
            List<String> lines = Files.readAllLines(pathFNames);
            for (String line : lines) {
                jsonFNames.append(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        StringBuilder jsonSNames = new StringBuilder();
        // Read in the last names
        try {
            List<String> lines = Files.readAllLines(pathSNames);
            for (String line : lines) {
                jsonSNames.append(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        StringBuilder jsonMNames = new StringBuilder();
        // Read in the male first names
        try {
            List<String> lines = Files.readAllLines(pathMNames);
            for (String line : lines) {
                jsonMNames.append(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        StringBuilder jsonLocations = new StringBuilder();
        // Read in the locations
        try {
            List<String> lines = Files.readAllLines(pathLocations);
            for (String line : lines) {
                jsonLocations.append(line);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }


        // Convert the json into objects
        maleFirstNames = (Names) convertJsonToObject(jsonMNames.toString(), maleFirstNames);
        femaleFirstNames = (Names) convertJsonToObject(jsonFNames.toString(), femaleFirstNames);
        lastNames = (Names) convertJsonToObject(jsonSNames.toString(), lastNames);
        locations = (Locations) convertJsonToObject(jsonLocations.toString(), locations);
    }

    public static Person getUniqueMan(String uniquePersonId, String descendant, String uniqueSpouseId) {
        return getUniquePerson(uniquePersonId, descendant, uniqueSpouseId, 'm');
    }

    public static Person getUniqueWoman(String uniquePersonId, String descendant, String uniqueSpouseId) {
        return getUniquePerson(uniquePersonId, descendant, uniqueSpouseId, 'f');
    }

    public static Person getUniquePerson(String uniquePersonId, String descendant, String uniqueSpouseId, char gender) {
        // Get a random first name
        String firstName;
        if (Character.toLowerCase(gender) == 'm') {
            firstName = getRandomName(maleFirstNames);
        } else {
            firstName = getRandomName(femaleFirstNames);
        }

        // Get a random last name
        String lastName = getRandomName(lastNames);

        // Get the relationship unique IDs
        String uniqueFatherId = UUID.randomUUID().toString();
        String uniqueMotherId = UUID.randomUUID().toString();

        // Create and return the person object
        return new Person(uniquePersonId, descendant, firstName, lastName, gender, uniqueFatherId, uniqueMotherId, uniqueSpouseId);
    }

    private static String getRandomName(Names names) {
        int randomIndex = RANDOM.nextInt(names.getNames().length);
        return names.getNames()[randomIndex];
    }

    public static Location getRandomLocation() {
        int randomIndex = RANDOM.nextInt(locations.getLocations().length);
        return locations.getLocations()[randomIndex];
    }
}
