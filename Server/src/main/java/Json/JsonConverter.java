package Json;

import com.google.gson.Gson;

/**
 * This class converts json to objects
 * Created by Kaiden on 3/3/2018.
 */

public class JsonConverter {
    public static Object convertJsonToObject(String json, Object object) {
        return new Gson().fromJson(json, object.getClass());
    }

    public static String convertObjectToJson(Object object) {
        return new Gson().toJson(object, object.getClass());
    }
}
