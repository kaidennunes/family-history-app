package Json;

/**
 * Created by Kaiden on 3/3/2018.
 */

public class Location {
    private String country;
    private String city;
    private double latitude;
    private double longitude;

    public Location() {
        this.setCountry(null);
        this.setCity(null);
        this.setLatitude(0);
        this.setLongitude(0);
    }

    public Location(String country, String city, double latitude, double longitude) {
        this.setCountry(country);
        this.setCity(city);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
