package Json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kaiden on 3/3/2018.
 */

public class Locations {
    @SerializedName("data")
    private Location[] locations;

    public Locations() {
        setLocations(null);
    }

    public Locations(Location[] locations) {
        this.setLocations(locations);
    }

    public Location[] getLocations() {
        return locations;
    }

    public void setLocations(Location[] locations) {
        this.locations = locations;
    }
}
