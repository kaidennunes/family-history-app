package Json;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Kaiden on 3/3/2018.
 */

public class Names {
    @SerializedName("data")
    private String[] names;

    public Names() {
        this.setNames(null);
    }

    public Names(String[] data) {
        this.setNames(data);
    }

    public String[] getNames() {
        return names;
    }

    public void setNames(String[] names) {
        this.names = names;
    }
}
