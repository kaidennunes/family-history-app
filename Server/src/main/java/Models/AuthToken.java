package Models;

/**
 * Model class for authentication token information.
 * Created by Kaiden on 2/10/2018.
 */

public class AuthToken {

    /**
     * Unique authentication token for the client (non-empty string).
     */
    private String authToken;
    /**
     * Username to which the client is associated with (non-empty string).
     */
    private String userName;
    /**
     * Constructor for the authentication token data.
     * @param authToken Unique authentication token for the client.
     * @param userName Username to which the client is associated with.
     */
    public AuthToken(String authToken, String userName) {
        this.setAuthToken(authToken);
        this.setUserName(userName);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o.getClass() != this.getClass()) {
            return false;
        }

        AuthToken object = (AuthToken) o;

        if (!object.getAuthToken().equals(this.getAuthToken())) {
            return false;
        }

        if (!object.getUserName().equals(this.getUserName())) {
            return false;
        }

        return true;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
