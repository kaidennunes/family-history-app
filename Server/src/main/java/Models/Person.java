package Models;

import com.google.gson.annotations.SerializedName;

/**
 * Model class for person data.
 * Created by Kaiden on 2/10/2018.
 */

public class Person {
    /**
     * Unique identifier for this person (non-empty string).
     */
    @SerializedName("personID")
    private String personId;
    /**
     * User (Username) to which this person belongs.
     */
    private String descendant;

    /**
     * Person’s first name (non-empty string).
     */
    private String firstName;

    /**
     * Person’s last name (non-empty string).
     */
    private String lastName;

    /**
     * Gender of Person(m or f).
     */
    private char gender;

    /**
     * personId of person’s father (possibly null).
     */
    private String father;

    /**
     * personId of person’s mother (possibly null).
     */
    private String mother;

    /**
     * personId of person’s spouse (possibly null).
     */
    private String spouse;
    /**
     * Constructor for person data.
     * @param personId Unique identifier for this person (non-empty string).
     * @param descendant User (Username) to which this person belongs.
     * @param firstName Person’s first name (non-empty string).
     * @param lastName Person’s last name (non-empty string).
     * @param gender Gender's of Person (m or f).
     * @param father personId of person’s father (possibly null).
     * @param mother personId of person’s mother (possibly null).
     * @param spouse personId of person’s spouse (possibly null).
     */
    public Person(String personId, String descendant, String firstName, String lastName, char gender, String father, String mother, String spouse) {
        this.setPersonId(personId);
        this.setDescendant(descendant);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
        this.setFather(father);
        this.setMother(mother);
        this.setSpouse(spouse);
    }
    /**
     * Constructor for person data.
     * @param personId Unique identifier for this person (non-empty string).
     * @param descendant User (Username) to which this person belongs.
     * @param firstName Person’s first name (non-empty string).
     * @param lastName Person’s last name (non-empty string).
     * @param gender Person’s gender ('m' or 'f').
     * @param father personId of person’s father (possibly null).
     * @param mother personId of person’s mother (possibly null).
     */
    public Person(String personId, String descendant, String firstName, String lastName, char gender, String father, String mother) {
        this.setPersonId(personId);
        this.setDescendant(descendant);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
        this.setFather(father);
        this.setMother(mother);
    }
    /**
     * Constructor for person data.
     * @param personId Unique identifier for this person (non-empty string).
     * @param descendant User (Username) to which this person belongs.
     * @param firstName Person’s first name (non-empty string).
     * @param lastName Person’s last name (non-empty string).
     * @param gender Person’s gender ('m' or 'f').
     * @param father personId of person’s father.
     */
    public Person(String personId, String descendant, String firstName, String lastName, char gender, String father) {
        this.setPersonId(personId);
        this.setDescendant(descendant);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
        this.setFather(father);
    }
    /**
     * Constructor for person data.
     * @param personId Unique identifier for this person (non-empty string).
     * @param descendant User (Username) to which this person belongs.
     * @param firstName Person’s first name (non-empty string).
     * @param lastName Person’s last name (non-empty string).
     * @param gender Person’s gender ('m' or 'f').
     */
    public Person(String personId, String descendant, String firstName, String lastName, char gender) {
        this.setPersonId(personId);
        this.setDescendant(descendant);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o.getClass() != this.getClass()) {
            return false;
        }

        Person object = (Person) o;

        if (!object.getPersonId().equals(this.getPersonId())) {
            return false;
        }

        if (!object.getDescendant().equals(this.getDescendant())) {
            return false;
        }

        if (!object.getFirstName().equals(this.getFirstName())) {
            return false;
        }

        if (!object.getLastName().equals(this.getLastName())) {
            return false;
        }

        if (object.getGender() != this.getGender()) {
            return false;
        }

        if (object.getFather() == null) {
            if (this.getFather() != null) {
                return false;
            }
        } else if (!object.getFather().equals(this.getFather())) {
                return false;
        }

        if (object.getMother() == null) {
            if (this.getMother() != null) {
                return false;
            }
        } else if (!object.getMother().equals(this.getMother())) {
            return false;
        }

        if (object.getSpouse() == null) {
            if (this.getSpouse() != null) {
                return false;
            }
        } else if (!object.getSpouse().equals(this.getSpouse())) {
            return false;
        }
        return true;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getDescendant() {
        return descendant;
    }

    public void setDescendant(String descendant) {
        this.descendant = descendant;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }
}
