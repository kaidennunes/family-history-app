package Models;

import com.google.gson.annotations.SerializedName;
import com.sun.org.apache.xpath.internal.operations.Equals;

/**
 * Model class for user data.
 * Created by Kaiden on 2/10/2018.
 */

public class User {
    /**
     * Unique user name (non-empty string).
     */
    private String userName;
    /**
     * User’s password (non-empty string).
     */
    @SerializedName("password")
    private String userPassword;
    /**
     * User’s email address (non-empty string).
     */
    private String email;
    /**
     * User’s first name (non-empty string).
     */
    private String firstName;
    /**
     * User’s last name (non-empty string).
     */
    private String lastName;
    /**
     * User’s gender (string: 'f' or 'm')
     */
    private char gender;
    /**
     * Unique Person ID assigned to this user’s generated Person object(non-empty string).
     */
    @SerializedName("personID")
    private String personId;
    /**
     * Constructor for the user data.
     * @param userName Unique user name (non-empty string).
     * @param userPassword User’s password (non-empty string).
     * @param email User’s first name (non-empty string).
     * @param firstName User’s last name (non-empty string).
     * @param lastName User’s last name (non-empty string).
     * @param gender User’s gender (string: 'f' or 'm')
     * @param personId Unique Person ID assigned to this user’s generated Person object(non-empty string).
     */
    public User(String userName, String userPassword, String email, String firstName, String lastName, char gender, String personId) {
        this.setUserName(userName);
        this.setUserPassword(userPassword);
        this.setEmail(email);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
        this.setPersonId(personId);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o.getClass() != this.getClass()) {
            return false;
        }

        User object = (User) o;

        if (!object.getUserName().equals(this.getUserName())) {
            return false;
        }

        if (!object.getUserPassword().equals(this.getUserPassword())) {
            return false;
        }

        if (!object.getEmail().equals(this.getEmail())) {
            return false;
        }

        if (!object.getFirstName().equals(this.getFirstName())) {
            return false;
        }

        if (object.getGender() != this.getGender()) {
            return false;
        }

        if (!object.getUserName().equals(this.getUserName())) {
            return false;
        }

        if (!object.getPersonId().equals(this.getPersonId())) {
            return false;
        }

        return true;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
