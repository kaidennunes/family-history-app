package Requests;

/**
 * Class for the request information for finding (for the logged in user) an event or all events in the database.
 * Created by Kaiden on 2/10/2018.
 */

public class EventRequest {
    /**
     * Authentication token of the logged in user.
     */
    private String authToken;
    /**
     * Unique identifier for the event to be found.
     */
    private String eventId;
    /**
     * Boolean value for whether all events should be found or not.
     * If true, userName is the descendant's username for the person data for whom all events are to be found in the database.
     * If false, userName is the username of the user who is the descendant of the person whose event data is to be found.
     */
    private boolean findAll;

    /**
     * Constructor for a event request for finding their information in the database
     *
     * @param authToken Authentication token of the logged in user.
     * @param eventId   Unique identifier for the event to be found.
     */
    public EventRequest(String authToken, String eventId) {
        this.setAuthToken(authToken);
        this.setEventId(eventId);
        this.setFindAll(false);
    }

    /**
     * Constructor for a event request for finding their information in the database
     *
     * @param authToken Authentication token of the logged in user.
     */
    public EventRequest(String authToken) {
        this.setAuthToken(authToken);
        this.setFindAll(true);
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public boolean isFindAll() {
        return findAll;
    }

    public void setFindAll(boolean findAll) {
        this.findAll = findAll;
    }
}
