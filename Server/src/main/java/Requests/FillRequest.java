package Requests;

/**
 * Class for the request information for filling in the generation information for a user.
 * Created by Kaiden on 2/10/2018.
 */

public class FillRequest {
    /**
     * The default number of generations that will be generated if a number is not given.
     */
    static final private int defaultNumberOfGenerations = 4;
    /**
     * Unique user name (non-empty string).
     */
    private String userName;

    /**
     * Number of generations to fill for the given user.
     */
    private int numberOfGenerations;

    /**
     * Constructor for the fill request information for a given user.
     * @param userName The userName whose generation information will be filled.
     * @param numberOfGenerations The number of generations to fill for the given user.
     */
    public FillRequest(String userName, int numberOfGenerations) {
        this.setUserName(userName);
        this.setNumberOfGenerations(numberOfGenerations);
    }

    /**
     * Default constructor for the fill request information for a given user.
     * @param userName The userName whose generation information will be filled.
     */
    public FillRequest(String userName) {
        this.setUserName(userName);
        this.setNumberOfGenerations(defaultNumberOfGenerations);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getNumberOfGenerations() {
        return numberOfGenerations;
    }

    public void setNumberOfGenerations(int numberOfGenerations) {
        this.numberOfGenerations = numberOfGenerations;
    }
}
