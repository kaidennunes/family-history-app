package Requests;

import Models.Event;
import Models.Person;
import Models.User;

/**
 * Class for the request information for loading the information for user(s).
 * Created by Kaiden on 2/10/2018.
 */

public class LoadRequest {
    /**
     * Array of user objects to be loaded into the database.
     */
    private User[] users;
    /**
     * Array of person objects to be loaded into the database.
     */
    private Person[] persons;
    /**
     * Array of event objects to be loaded into the database.
     */
    private Event[] events;

    /**
     * Constructor for the load request to load up the database with the given information.
     * @param users Array of user objects to be loaded into the database.
     * @param persons Array of person objects to be loaded into the database.
     * @param events Array of event objects to be loaded into the database.
     */
    public LoadRequest(User[] users, Person[] persons, Event[] events) {
        this.setUsers(users);
        this.setPersons(persons);
        this.setEvents(events);
    }

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public Person[] getPersons() {
        return persons;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }

    public Event[] getEvents() {
        return events;
    }

    public void setEvents(Event[] events) {
        this.events = events;
    }
}
