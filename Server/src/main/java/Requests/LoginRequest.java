package Requests;

import com.google.gson.annotations.SerializedName;

/**
 * Class for the request information for logging in a user.
 * Created by Kaiden on 2/10/2018.
 */

public class LoginRequest {
    /**
     * Unique user name (non-empty string).
     */
    private String userName;
    /**
     * User’s password (non-empty string).
     */
    @SerializedName("password")
    private String userPassword;

    /**
     * Constructor for the register request data.
     * @param userName Unique user name (non-empty string).
     * @param userPassword User’s password (non-empty string).
     */
    public LoginRequest(String userName, String userPassword) {
        this.setUserName(userName);
        this.setUserPassword(userPassword);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
}
