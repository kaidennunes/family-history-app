package Requests;

/**
 * Class for the request information for finding (for the logged in user) a person or all people in the database.
 * Created by Kaiden on 2/10/2018.
 */

public class PersonRequest {
    /**
     * Authentication token of the logged in user.
     */
    private String authToken;
    /**
     * Unique identifier for the person to be found.
     */
    private String personId;
    /**
     * Boolean value for whether all people should be found or not.
     * If true, userName is the descendant's username for the people to be found in the database.
     * If false, userName is the username of the user who is the descendant of the person to be found.
     */
    private boolean findAll;

    /**
     * Constructor for a person request for finding their information in the database
     *
     * @param authToken Authentication token of the logged in user.
     * @param personId Unique identifier for the person to be found.
     */
    public PersonRequest(String authToken, String personId) {
        this.setAuthToken(authToken);
        this.setPersonId(personId);
        this.setFindAll(false);
    }

    /**
     * Constructor for a person request for finding their information in the database
     *
     * @param authToken Authentication token of the logged in user.
     */
    public PersonRequest(String authToken) {
        this.setAuthToken(authToken);
        this.setFindAll(true);
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public boolean isFindAll() {
        return findAll;
    }

    public void setFindAll(boolean findAll) {
        this.findAll = findAll;
    }


}
