package Requests;

import com.google.gson.annotations.SerializedName;

import Response.RegisterResponse;

/**
 * Class for the request information for registering a user.
 * Created by Kaiden on 2/10/2018.
 */

public class RegisterRequest {
    /**
     * Unique user name (non-empty string).
     */
    private String userName;
    /**
     * User’s password (non-empty string).
     */
    @SerializedName("password")
    private String userPassword;
    /**
     * User’s email address (non-empty string).
     */
    private String email;
    /**
     * User’s first name (non-empty string).
     */
    private String firstName;
    /**
     * User’s last name (non-empty string).
     */
    private String lastName;
    /**
     * User’s gender (string: 'f' or 'm')
     */
    private char gender;

    /**
     * Constructor for the register request data.
     * @param userName Unique user name (non-empty string).
     * @param userPassword User’s password (non-empty string).
     * @param email User’s first name (non-empty string).
     * @param firstName User’s last name (non-empty string).
     * @param lastName User’s last name (non-empty string).
     * @param gender User’s gender (string: 'f' or 'm')
     */
    public RegisterRequest(String userName, String userPassword, String email, String firstName, String lastName, char gender) {
        this.setUserName(userName);
        this.setUserPassword(userPassword);
        this.setEmail(email);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setGender(gender);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }
}
