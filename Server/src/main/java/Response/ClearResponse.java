package Response;

/**
 * Encapsulates the response information for clearing the database.
 * Created by Kaiden on 2/10/2018.
 */

public class ClearResponse extends Response {
    /**
     * String describing that the clear was successful.
     */
    private String message = "Clear succeeded";

    /**
     * Constructor for the response class for clearing successfully.
     */
    public ClearResponse() {}

    /**
     * Constructor for the response class for clearing with error(s).
     * @param errorMessage Non-empty string describing the error.
     */
    public ClearResponse(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Gets String for describing whether the clear was successful.
     * @return The String for descriping that the clear was successful.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets String for describing that the clear was successful.
     * @param message The success message to be set.
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
