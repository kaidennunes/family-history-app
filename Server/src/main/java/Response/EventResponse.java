package Response;

import Models.Event;

/**
 * Encapsulates the response information for
 * Created by Kaiden on 2/10/2018.
 */

public class EventResponse extends Response {

    /**
     * Array of person objects in the database for the given user.
     */
    private Event[] events;

    public EventResponse() {

    }
    /**
     * Constructor for the response class for returning a set of event data in the database successfully.
     * @param events Array of person objects that were found.
     */
    public EventResponse(Event[] events) {
        this.setEvents(events);
    }
    /**
     * Constructor for the response class for returning a set of event data in the database, with error(s).
     * @param errorMessage Non-empty string describing the error.
     */
    public EventResponse(String errorMessage){super(errorMessage);}

    public Event[] getEvents() {
        return events;
    }

    public void setEvents(Event[] events) {
        this.events = events;
    }
}
