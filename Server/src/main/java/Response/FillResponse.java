package Response;

/**
 * Encapsulates the response information for filling the database for a given user.
 * Created by Kaiden on 2/10/2018.
 */

public class FillResponse extends Response {
    /**
     * Message describing how many people and events were added to the database for the given user.
     */
    private String message;

    /**
     * Default constructor for FillResponse class
     */
    public FillResponse() {

    }

    /**
     * Constructor for the response class for filling the database for a user successfully.
     *
     * @param numberOfPeopleAdded Number of people added to the database.
     * @param numberOfEventsAdded Number of events added to the database.
     */
    public FillResponse(int numberOfPeopleAdded, int numberOfEventsAdded) {
        this.setMessage(numberOfPeopleAdded, numberOfEventsAdded);
    }


    /**
     * This method adds to the string message contained in the data container explaining how many people and events were added to the database.
     *
     * @param numberOfPeopleAdded Number of people added to the database.
     * @param numberOfEventsAdded Number of events added to the database.
     */
    public void setMessage(int numberOfPeopleAdded, int numberOfEventsAdded) {
        StringBuilder builtMessage = new StringBuilder();
        builtMessage.append("Successfully added ");
        builtMessage.append(numberOfPeopleAdded);
        builtMessage.append(" persons and ");
        builtMessage.append(numberOfEventsAdded);
        builtMessage.append(" events to the database.");
        this.message = builtMessage.toString();
    }

    /**
     * Constructor for the response class for filling the database for a user, with error(s).
     *
     * @param errorMessage Non-empty string describing the error.
     */
    public FillResponse(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Gets the message describing how many people and events were added to the database for the given user.
     *
     * @return Message describing how many people and events were added to the database for the given user.
     */
    public String getMessage() {
        return message;
    }
}
