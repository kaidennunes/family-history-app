package Response;

/**
 * Encapsulates the response information for loading the database with the given user, person, and event data.
 * Created by Kaiden on 2/10/2018.
 */

public class LoadResponse extends Response {
    /**
     * Message describing how many people and events were added to the database for the given user.
     */
    private String message;

    /**
     * Default constructor for LoadResponse class
     */
    public LoadResponse() {

    }

    /**
     * Constructor for the response class for filling the database for a user successfully.
     *
     * @param numberOfUsersAdded  Number of users added to the database.
     * @param numberOfPeopleAdded Number of people added to the database.
     * @param numberOfEventsAdded Number of events added to the database.
     */
    public LoadResponse(int numberOfUsersAdded, int numberOfPeopleAdded, int numberOfEventsAdded) {
        this.setMessage(numberOfUsersAdded, numberOfPeopleAdded, numberOfEventsAdded);
    }

    /**
     * This method adds to the string message contained in the data container explaining how many people and events were added to the database.
     *
     * @param numberOfUsersAdded  Number of users added to the database.
     * @param numberOfPeopleAdded Number of people added to the database.
     * @param numberOfEventsAdded Number of events added to the database.
     */
    public void setMessage(int numberOfUsersAdded, int numberOfPeopleAdded, int numberOfEventsAdded) {
        StringBuilder builtMessage = new StringBuilder();
        builtMessage.append("Successfully added ");
        builtMessage.append(numberOfUsersAdded);
        builtMessage.append(" users, ");
        builtMessage.append(numberOfPeopleAdded);
        builtMessage.append(" persons and ");
        builtMessage.append(numberOfEventsAdded);
        builtMessage.append(" events to the database.");
        this.message = builtMessage.toString();
    }

    /**
     * Constructor for the response class for loading the database with a given user, person and event data, with error(s).
     *
     * @param errorMessage Non-empty string describing the error.
     */
    public LoadResponse(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Gets the message describing how many people and events were added to the database for the given user.
     *
     * @return Message describing how many people and events were added to the database for the given user.
     */
    public String getMessage() {
        return message;
    }
}
