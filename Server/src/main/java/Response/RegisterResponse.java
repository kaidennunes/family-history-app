package Response;

/**
 * Encapsulates the response information for registration.
 * Created by Kaiden on 2/10/2018.
 */

public class RegisterResponse extends Response {

    /**
     * Non-empty auth token string
     */
    private String authToken;

    /**
     * User name passed in with request
     */
    private String userName;

    /**
     * Non-empty string containing the Person ID of the user’s generated Person object.
     */
    private String personId;

    /**
     * Constructor for the response class for registering successfully.
     * @param authToken Non-empty auth token string
     * @param userName User name passed in with request
     * @param personId Non-empty string containing the Person ID of the user’s generated Person object
     */
    public RegisterResponse(String authToken, String userName, String personId) {
        this.setAuthToken(authToken);
        this.setUserName(userName);
        this.setPersonId(personId);
    }

    /**
     * Constructor for the response class for registering with error(s).
     * @param errorMessage Non-empty string describing the error.
     */
    public RegisterResponse(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Gets non-empty auth token string.
     * @return The auhentication token
     */
    public String getAuthToken() {
        return authToken;
    }

    /**
     * Sets non-empty auth token string.
     * @param authToken The authentication token to be set.
     */
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    /**
     * Gets user name passed in with request.
     * @return  The user's name
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name passed in with request.
     * @param userName The user name to be set.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Gets non-empty string containing the Person ID of the user’s generated Person object.
     * @return The person's ID.
     */
    public String getPersonId() {
        return personId;
    }

    /**
     * Sets non-empty string containing the Person ID of the user’s generated Person object.
     * @param personId The person's ID to be set.
     */
    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
