package Server;

import static Json.Json.loadJson;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

import Handlers.ClearHandler;
import Handlers.EventHandler;
import Handlers.FileHandler;
import Handlers.FillHandler;
import Handlers.LoadHandler;
import Handlers.LoginHandler;
import Handlers.PersonHandler;
import Handlers.RegisterHandler;

/**
 * Created by Kaiden on 2/20/2018.
 */

public class Server {

    private static final int MAX_WAITING_CONNECTIONS = 12;

    private HttpServer server;

    /**
     * This method initializes and runs the server.
     *
     * @param portNumber The port number on which the server should accept incoming client connections
     */
    private void run(String portNumber) {

        System.out.println("Initializing HTTP Server");

        try {
            server = HttpServer.create(
                    new InetSocketAddress(Integer.parseInt(portNumber)),
                    MAX_WAITING_CONNECTIONS);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        server.setExecutor(null);

        System.out.println("Creating contexts");

        server.createContext("/user/register", new RegisterHandler());

        server.createContext("/user/login", new LoginHandler());

        server.createContext("/clear", new ClearHandler());

        server.createContext("/fill", new FillHandler());

        server.createContext("/load", new LoadHandler());

        server.createContext("/person", new PersonHandler());

        server.createContext("/event", new EventHandler());

        server.createContext("/", new FileHandler());

        System.out.println("Starting server");

        server.start();

        System.out.println("Server started");
    }

    public static void main(String[] args) {
        // Get the port number
        String portNumber = args[0];

        // Start the server
        new Server().run(portNumber);

        // Load up the json used for the fill service
        loadJson();
    }
}
