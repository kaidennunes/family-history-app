package Services;

import java.sql.SQLException;

import javax.xml.crypto.Data;

import Dao.Database;
import Response.ClearResponse;

/**
 * Class for servicing the clear request.
 * Created by Kaiden on 2/10/2018.
 */

public class ClearService extends Service {

    /**
     * Default constructor for the clear service class.
     */
    public ClearService() {

    }

    /**
     * Deletes ALL data from the database, including user accounts, auth tokens, and generated person and event data. Used for testing
     *
     * @return Returns a ClearResponse, either with either a success or error message.
     */
    public ClearResponse clearTest() {
        return implementClear(true);
    }

    /**
     * Deletes ALL data from the database, including user accounts, auth tokens, and generated person and event data.
     *
     * @return Returns a ClearResponse, either with either a success or error message.
     */
    public ClearResponse clear() {
        return implementClear(false);
    }

    /**
     * Implements the clear methods.
     *
     * @param test Whether the method is being called to test or to clear the actual database
     * @return Returns a ClearResponse, either with either a success or error message.
     */
    private ClearResponse implementClear(boolean test) {

        // Create the encapsulated data holder
        ClearResponse clearResponse = new ClearResponse();

        // Create an instance of the database class
        Database database = new Database();

        try {
            // Assume success
            boolean cleared = true;

            // Open the user database connection
            if (test) {
                database.userDao.openTestConnection();
            } else {
                database.userDao.openConnection();
            }

            // Clear everything in the user table
            if (!database.userDao.deleteAllUsers()) {
                cleared = false;
            }

            // Close the user database connection
            database.userDao.closeConnection(true);

            // Open the authentication token database connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Clear everything in the authentication token table
            if (!database.authTokenDao.deleteAllAuthTokens()) {
                cleared = false;
            }

            // Close the authentication token database connection
            database.authTokenDao.closeConnection(true);

            // Open the event database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Clear everything in the event table
            if (!database.eventDao.deleteAllEvents()) {
                cleared = false;
            }

            // Close the event database connection
            database.eventDao.closeConnection(true);

            // Open the person database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Clear everything in the person table
            if (!database.personDao.deleteAllPeople()) {
                cleared = false;
            }

            // Close the person database connection
            database.personDao.closeConnection(true);

            // If the database wasn't cleared for some reason, let the user know
            if (!cleared) {
                setErrorMessage(clearResponse, "Internal server error");
                return clearResponse;
            }

        } catch (SQLException ex) {
            setErrorMessage(clearResponse, "Internal server error");
            ex.printStackTrace();
        }

        return clearResponse;
    }

}
