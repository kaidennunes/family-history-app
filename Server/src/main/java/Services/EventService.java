package Services;

import java.sql.SQLException;

import javax.xml.crypto.Data;

import Dao.Database;
import Models.Event;
import Requests.EventRequest;
import Response.EventResponse;

/**
 * Class for servicing the event request.
 * Created by Kaiden on 2/10/2018.
 */

public class EventService extends Service {
    private EventRequest eventRequest;

    /**
     * Default constructor for the person service class.
     *
     * @param eventRequest The EventRequest to be made.
     */
    public EventService(EventRequest eventRequest) {
        this.eventRequest = eventRequest;
    }

    /**
     * Gets the event data for the EventRequest. Used for testing
     *
     * @return Returns a EventResponse, with either an error message or an array of Event objects with size of 1.
     */
    public EventResponse getEventTest() {
        return implementGetEvent(true);
    }

    /**
     * Gets the event data for the EventRequest.
     *
     * @return Returns a EventResponse, with either an error message or an array of Event objects with size of 1.
     */
    public EventResponse getEvent() {
        return implementGetEvent(false);
    }

    /**
     * Implements the get event methods.
     *
     * @param test Whether the method is being called to test or to view the actual database
     * @return Returns a EventResponse, with either an error message or an array of Event objects with size of 1.
     */
    private EventResponse implementGetEvent(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        EventResponse eventResponse = new EventResponse();

        try {
            // Try to open the database connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Get the username with which the authentication token is associated
            String username = database.authTokenDao.getAuthToken(eventRequest.getAuthToken());

            // Close the database connection
            database.authTokenDao.closeConnection(false);

            // If the authentication token is invalid, tell the user that and stop processing the request
            if (username == null) {
                setErrorMessage(eventResponse, "Invalid auth token");
                return eventResponse;
            }

            // Try to open the database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Find the event with the event ID in the request
            Event event = database.eventDao.getEvent(eventRequest.getEventId());

            // Close the database connection
            database.eventDao.closeConnection(false);

            // If an event with that ID doesn't exists tell the user that and stop processing the request
            if (event == null) {
                setErrorMessage(eventResponse, "Invalid eventID parameter");
                return eventResponse;
            }

            // See if user is authorized to view the event
            if (!username.equals(event.getDescendant())) {
                setErrorMessage(eventResponse, "Requested event does not belong to this user");
                return eventResponse;
            }

            // Put the event in an event array
            Event[] events = {event};

            // Use the event array to set the events in the EventResponse
            eventResponse.setEvents(events);

        } catch (SQLException ex) {
            setErrorMessage(eventResponse, "Internal server error");
        }
        return eventResponse;
    }


    /**
     * Gets the event data for the EventRequest. Used for testing
     *
     * @return Returns a EventResponse, with either an error message or an array of all Event objects for all people with the logged in user as a descendant.
     */
    public EventResponse getEventsTest() {
        return implementGetEvents(true);
    }

    /**
     * Gets the event data for the EventRequest.
     *
     * @return Returns a EventResponse, with either an error message or an array of all Event objects for all people with the logged in user as a descendant.
     */
    public EventResponse getEvents() {
        return implementGetEvents(false);
    }

    /**
     * Implements the get events methods.
     *
     * @param test Whether the method is being called to test or to view the actual database
     * @return Returns a EventResponse, with either an error message or an array of all Event objects for all people with the logged in user as a descendant.
     */
    private EventResponse implementGetEvents(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        EventResponse eventResponse = new EventResponse();

        try {
            // Try to open the database connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Get the username with which the authentication token is associated
            String username = database.authTokenDao.getAuthToken(eventRequest.getAuthToken());

            // Close the database connection
            database.authTokenDao.closeConnection(false);

            // If the authentication token is invalid, tell the user that and stop processing the request
            if (username == null) {
                setErrorMessage(eventResponse, "Invalid auth token");
                return eventResponse;
            }

            // Try to open the database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Find the all events associated with the user
            Event[] events = database.eventDao.getAllEventsForUsername(username);

            // Close the database connection
            database.eventDao.closeConnection(false);

            /*
            // If there were no events, tell the user that
            if (events == null) {
                setErrorMessage(eventResponse, "Invalid eventID parameter");
                return eventResponse;
            }
            /**/

            // Use the event array to set the events in the EventResponse
            eventResponse.setEvents(events);

        } catch (SQLException ex) {
            setErrorMessage(eventResponse, "Internal server error");
        }
        return eventResponse;
    }
}
