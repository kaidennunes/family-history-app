package Services;

import java.sql.SQLException;
import java.util.Random;
import java.util.UUID;
import java.util.Calendar;
import java.util.concurrent.ThreadLocalRandom;

import Dao.Database;
import Json.Location;
import Models.Event;
import Models.Person;
import Models.User;
import Requests.FillRequest;
import Response.FillResponse;

import static Json.Json.*;

/**
 * Class for servicing the fill request.
 * Created by Kaiden on 2/10/2018.
 */

public class FillService extends Service {
    private int NUMBER_OF_EVENTS_PER_PERSON = 3;
    private FillRequest fillRequest;
    private int numberOfPeopleGenerated = 0;
    private int numberOfEventsGenerated = 0;

    private static final Random RANDOM = new Random();
    private static final int GENERATION_LIFETIME = 25;
    private static final int MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE = 15;
    private static final int RANDOM_BIRTH_VARIANCE = 5;
    private static final int RANDOM_DEATH_VARIANCE = 10;
    private static final int FUTURE_VARIANCE = 5;

    private final String[] EVENT_TYPES = new String[]{"Birth", "Baptism", "Christening", "Marriage", "Death"};

    /**
     * Default constructor for the fill service class.
     *
     * @param fillRequest The FillRequest to be made.
     */
    public FillService(FillRequest fillRequest) {
        this.fillRequest = fillRequest;
    }

    /**
     * Populates the server's database with generated data for the specified user name.
     * The required "username" parameter must be a user already registered with the server. If there
     * is any data in the database already associated with the given user name, it is deleted. Used for testing
     *
     * @return Returns a FillRequest, either with either a success or error message.
     */
    public FillResponse fillTest() {
        return implementFill(true);
    }

    /**
     * Populates the server's database with generated data for the specified user name.
     * The required "username" parameter must be a user already registered with the server. If there
     * is any data in the database already associated with the given user name, it is deleted.
     *
     * @return Returns a FillRequest, either with either a success or error message.
     */
    public FillResponse fill() {
        return implementFill(false);
    }

    private FillResponse implementFill(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        FillResponse fillResponse = new FillResponse();

        try {
            // Try to open the database connection
            if (test) {
                database.userDao.openTestConnection();
            } else {
                database.userDao.openConnection();
            }

            // Get the user
            User user = database.userDao.getUser(fillRequest.getUserName());

            // Close the database connection
            database.userDao.closeConnection(false);

            // If the user doesn't exist in the database, stop processing the request and return an error
            if (user == null) {
                setErrorMessage(fillResponse, "Invalid username or generations parameter");
                return fillResponse;
            }

            // Open the database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Delete any people data the user might have
            database.personDao.deleteAllPeopleForUsername(fillRequest.getUserName());

            // Close the database connection
            database.personDao.closeConnection(true);

            // Open the database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Delete any event data the user might have
            database.eventDao.deleteAllEventsForUsername(fillRequest.getUserName());

            // Close the database connection
            database.eventDao.closeConnection(true);

            // Find how many people should be generated
            int numberOfPeopleToGenerate = calculateNumberOfPeopleToGenerate(0, 0);


            // Initialize the array that will hold the people to be added to the database
            Person[] people = new Person[numberOfPeopleToGenerate];

            // Initialize the array that will hold the events to be added to the database
            Event[] events = new Event[numberOfPeopleToGenerate * NUMBER_OF_EVENTS_PER_PERSON];

            // Randomly generate the information to fill in the people and events of the user's family history
            generatePeopleAndEvents(user, people, events);

            // Open the database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Add the people to the database
            for (int i = 0; i < people.length; i++) {
                database.personDao.addPerson(people[i]);
            }

            // Close the database connection
            database.personDao.closeConnection(true);


            // Open the database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Add the events to the database
            for (int i = 0; i < events.length; i++) {
                database.eventDao.addEvent(events[i]);
            }

            // Close the database connection
            database.eventDao.closeConnection(true);

            // Report in the FillResponse how many people and events were added to the database
            fillResponse.setMessage(people.length, events.length);

        } catch (SQLException ex) {
            setErrorMessage(fillResponse, "Internal server error");
        }
        return fillResponse;
    }

    private void generatePeopleAndEvents(User user, Person[] people, Event[] events) {
        // Create a unique ID for the father and mother of the user (the user's spouse won't be shown)
        String uniqueFatherId = UUID.randomUUID().toString();
        String uniqueMotherId = UUID.randomUUID().toString();

        // Make the first person in the array be the user
        people[0] = new Person(user.getPersonId(), user.getUserName(), user.getFirstName(), user.getLastName(), user.getGender(), uniqueFatherId, uniqueMotherId);

        // Increment the counter of the number of people generated
        numberOfPeopleGenerated++;

        // Find the current year
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        // Generate the event data for the user
        generateEventDataForUser(events, currentYear - GENERATION_LIFETIME, user);

        // Generate the people and event data for everyone
        generatePeopleAndEventsHelper(people, events, 1, uniqueFatherId, uniqueMotherId, currentYear - (GENERATION_LIFETIME * 2));
    }

    private void generatePeopleAndEventsHelper(Person[] people, Event[] events, int generationNumberToAdd, String uniqueFatherId, String uniqueMotherId, int year) {
        // Stop generating people and events when the number of generations to be generated has been reached
        if (generationNumberToAdd > fillRequest.getNumberOfGenerations()) {
            people[numberOfPeopleGenerated - 1].setFather(null);
            people[numberOfPeopleGenerated - 1].setMother(null);
            return;
        }

        // Generate the father of the previous person
        Person father = getUniqueMan(uniqueFatherId, fillRequest.getUserName(), uniqueMotherId);

        // Add the father to the array
        people[numberOfPeopleGenerated] = father;

        // Increment the counter of the number of people generated
        numberOfPeopleGenerated++;

        // Generate the parents of the father
        generatePeopleAndEventsHelper(people, events, generationNumberToAdd + 1, father.getFather(), father.getMother(), year - GENERATION_LIFETIME);

        // Generate the mother of the previous person
        Person mother = getUniqueWoman(uniqueMotherId, fillRequest.getUserName(), uniqueFatherId);

        // Add the mother to the array
        people[numberOfPeopleGenerated] = mother;

        // Increment the counter of the number of people generated
        numberOfPeopleGenerated++;

        // Generate the parents of the mother
        generatePeopleAndEventsHelper(people, events, generationNumberToAdd + 1, mother.getFather(), mother.getMother(), year - GENERATION_LIFETIME);

        // Generate events for this couple, keeping track of whether they were married
        boolean wereMarried = generateEventsForCouple(events, father, mother, year);

        // If they weren't married, remove their spouse ID (because they weren't actually married)
        if (!wereMarried) {
            father.setSpouse(null);
            mother.setSpouse(null);
        }
    }


    private boolean generateEventsForCouple(Event[] events, Person father, Person mother, int startingYear) {
        // Keep track of whether they were married
        boolean wereMarried = false;

        // Create an instance of a possible marriage event so, if the mother also gets the marriage event, they match up
        Event marriageEvent = null;

        // Initialize the variables to track the number of events for each person
        int numberOfEventsForFather = 0;
        int numberOfEventsForMother = 0;

        // Keep track of the birth dates
        Integer fatherBirthDate = null;
        Integer motherBirthDate = null;

        // Keep track of the death dates
        Integer fatherDeathDate = null;
        Integer motherDeathDate = null;

        // Create random events for the father
        while (numberOfEventsForFather < NUMBER_OF_EVENTS_PER_PERSON) {
            // Randomly create an event type
            String eventType = generateRandomEventType();

            // If the event is a duplicate death or birth event, try again.
            if ((eventType.equals("Birth") && fatherBirthDate != null) || (eventType.equals("Death") && fatherDeathDate != null)) {
                continue;
            }

            // Create a random unique ID for the event
            String uniqueEventId = UUID.randomUUID().toString();

            // Randomly create a location in the world
            Location location = getRandomLocation();

            // Create an event to be added to the array
            Event event = new Event(uniqueEventId, father.getDescendant(), father.getPersonId(), location.getLatitude(), location.getLongitude(), location.getCountry(), location.getCity(), eventType);

            // Initialize the variable to hold the year
            int year;

            // Populate the year based on the event type
            if (eventType.equals("Birth")) {
                year = generateRandomBirthYear(startingYear);
                fatherBirthDate = year;
            } else if (eventType.equals("Death")) {
                year = generateRandomDeathYear(startingYear);
                fatherDeathDate = year;
            } else if (eventType.equals("Marriage")) {
                year = generateRandomMarriageYear(startingYear);
                // If this was the first marriage event for the father, save it to use for the mother later, if she gets a marriage event
                if (marriageEvent == null) {
                    marriageEvent = event;
                }
            } else {
                year = generateRandomYear(startingYear);
            }

            // Further fine tune the year by making sure the marriage is a reasonable time after the birth
            if (fatherBirthDate != null && eventType.equals("Marriage") && !(year > MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE + fatherBirthDate)) {
                year += MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE;
            }

            // Further fine tune the year by making sure the event is before his death
            while (fatherDeathDate != null && year >= fatherDeathDate) {
                year -= RANDOM_DEATH_VARIANCE;
            }

            // Get the current year
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            // Further fine tune the year by making sure the event is not in the future
            while (year > currentYear) {
                year -= FUTURE_VARIANCE;
            }

            // Set the year of the event
            event.setYear(year);

            // Add the event to array of events
            events[numberOfEventsGenerated++] = event;

            // Increment the counter of the number of events generated for the father
            numberOfEventsForFather++;
        }

        // Create random events for the mother
        while (numberOfEventsForMother < NUMBER_OF_EVENTS_PER_PERSON) {
            // Randomly create an event type
            String eventType = generateRandomEventType();

            // If the event is a duplicate death or birth event, try again.
            if ((eventType.equals("Birth") && motherBirthDate != null) || (eventType.equals("Death") && motherDeathDate != null)) {
                continue;
            }

            // Create a random unique ID for the event
            String uniqueEventId = UUID.randomUUID().toString();

            // Randomly create a location in the world
            Location location = getRandomLocation();

            // Create an event to be added to the array
            Event event = new Event(uniqueEventId, mother.getDescendant(), mother.getPersonId(), location.getLatitude(), location.getLongitude(), location.getCountry(), location.getCity(), eventType);

            // Initialize the variable to hold the year
            int year;

            // Populate the year based on the event type
            if (eventType.equals("Birth")) {
                year = generateRandomBirthYear(startingYear);
                motherBirthDate = year;
            } else if (eventType.equals("Death")) {
                year = generateRandomDeathYear(startingYear);
                motherDeathDate = year;
            }
            // If the event is marriage and the father was married, and this was the first marriage event for the woman, pretend that they were married to each other
            else if (eventType.equals("Marriage") && marriageEvent != null && !wereMarried) {
                // Mark that the couple was married
                wereMarried = true;

                // Change the event to match the marriage event of the father
                event.setPerson(mother.getPersonId());
                event.setLatitude(marriageEvent.getLatitude());
                event.setLongitude(marriageEvent.getLongitude());
                event.setCountry(marriageEvent.getCountry());
                event.setCity(marriageEvent.getCity());
                event.setYear(marriageEvent.getYear());

                // Set the year variable for further fine tuning (if the year gets changed because of the birth, marriage, and death moderation, that's okay, pretend it was a different marriage in the same place)
                year = event.getYear();
            } else if (eventType.equals("Marriage")) {
                year = generateRandomMarriageYear(startingYear);
            } else {
                year = generateRandomYear(startingYear);
            }

            // Further fine tune the year by making sure the marriage is a reasonable time after her birth
            if (motherBirthDate != null && eventType.equals("Marriage") && !(year > MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE + motherBirthDate)) {
                year += MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE;
            }

            // Further fine tune the year by making sure the event is before her death
            while (motherDeathDate != null && year >= motherDeathDate) {
                year -= RANDOM_DEATH_VARIANCE;
            }

            // Get the current year
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            // Further fine tune the year by making sure the event is not in the future
            while (year > currentYear) {
                year -= FUTURE_VARIANCE;
            }

            // Set the year of the event
            event.setYear(year);

            // Add the event to array of events
            events[numberOfEventsGenerated++] = event;

            // Increment the counter of the number of events generated for the mother
            numberOfEventsForMother++;
        }
        return wereMarried;
    }

    private void generateEventDataForUser(Event[] events, int startingYear, User user) {
        // Keep track of the birth date
        Integer birthDate = null;

        // Create random events for the father
        while (numberOfEventsGenerated < NUMBER_OF_EVENTS_PER_PERSON) {
            // Randomly create an event type
            String eventType = generateRandomEventType();

            // If the event is a duplicate birth event, or a death date (the user can't be dead), try again.
            if ((eventType.equals("Birth") && birthDate != null) || eventType.equals("Death")) {
                continue;
            }

            // Create a random unique ID for the event
            String uniqueEventId = UUID.randomUUID().toString();

            // Randomly create a location in the world
            Location location = getRandomLocation();

            // Create an event to be added to the array
            Event event = new Event(uniqueEventId, user.getUserName(), user.getPersonId(), location.getLatitude(), location.getLongitude(), location.getCountry(), location.getCity(), eventType);

            // Initialize the variable to hold the year
            int year;

            // Populate the year based on the event type
            if (eventType.equals("Birth")) {
                year = generateRandomBirthYear(startingYear);
                birthDate = year;
            } else if (eventType.equals("Marriage")) {
                year = generateRandomMarriageYear(startingYear);
            } else {
                year = generateRandomYear(startingYear);
            }

            // Further fine tune the year by making sure the marriage is a reasonable time after the birth
            if (birthDate != null && eventType.equals("Marriage") && !(year > MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE + birthDate)) {
                year -= MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE;
            }

            // Get the current year
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            // Further fine tune the year by making sure the event is not in the future
            while (year > currentYear) {
                year -= FUTURE_VARIANCE;
            }

            // Set the year of the event
            event.setYear(year);

            // Add the event to array of events
            events[numberOfEventsGenerated++] = event;
        }
    }

    /**
     * Generates a random event type from the array of possible event choices.
     *
     * @return A random life event as a String.
     */
    private String generateRandomEventType() {
        int randomIndex = RANDOM.nextInt(EVENT_TYPES.length);
        return EVENT_TYPES[randomIndex];
    }

    /**
     * Randomly choose a year within the given range of the const.
     *
     * @param startingYear The starting year of the generation, which is the basis for all calculations.
     * @return A reasonable randomly generated year.
     */
    private int generateRandomYear(int startingYear) {
        int randomNumber = RANDOM.nextInt(GENERATION_LIFETIME);
        return startingYear + randomNumber;
    }

    /**
     * Randomly choose a reasonable year for the marriage date
     *
     * @param startingYear The starting year of the generation, which is the basis for all calculations.
     * @return A reasonable randomly generated year.
     */
    private int generateRandomMarriageYear(int startingYear) {
        // Generate a random marriage year
        int randomNumber = RANDOM.nextInt(GENERATION_LIFETIME);

        // If the year is not acceptable, add the minimum number of years between birth and death to the number
        if (randomNumber < MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE) {
            randomNumber += MIN_TIME_BETWEEN_BIRTH_AND_MARRIAGE;
        }

        // Return the year based on the base year given.
        return startingYear + randomNumber;
    }

    /**
     * Randomly choose a reasonable year for the death date
     *
     * @param startingYear The starting year of the generation, which is the basis for all calculations.
     * @return A reasonable randomly generated year.
     */
    private int generateRandomDeathYear(int startingYear) {
        // Get a random number between the last half of the possible life
        int randomNumber = RANDOM.nextInt(GENERATION_LIFETIME) + GENERATION_LIFETIME;

        // Randomly get another number to vary the death year
        int randomDeathNumber = RANDOM.nextInt(RANDOM_DEATH_VARIANCE);

        // Return a randomly generate year for the death
        return startingYear + randomNumber + randomDeathNumber;
    }

    /**
     * Randomly choose a reasonable year for the birth date.
     *
     * @param startingYear The starting year of the generation, which is the basis for all calculations.
     * @return A reasonable randomly generated year.
     */
    private int generateRandomBirthYear(int startingYear) {
        // Get a random number in a range
        int randomNumber = RANDOM.nextInt(RANDOM_BIRTH_VARIANCE);

        // Get another random number based on the generation lifetime
        int randomGenerationNumber = RANDOM.nextInt(GENERATION_LIFETIME);

        // Return a reasonable birth year
        return startingYear - randomGenerationNumber + randomNumber;
    }

    private int calculateNumberOfPeopleToGenerate(int numberOfPeopleToGenerate, int generationNumberToAdd) {
        if (generationNumberToAdd > fillRequest.getNumberOfGenerations()) {
            return numberOfPeopleToGenerate;
        }
        int addedPeopleToGenerate = numberOfPeopleToGenerate + (int) Math.round(Math.pow(2, generationNumberToAdd++));
        return calculateNumberOfPeopleToGenerate(addedPeopleToGenerate, generationNumberToAdd);
    }
}
