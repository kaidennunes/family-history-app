package Services;

import java.sql.SQLException;

import Dao.Database;
import Models.Event;
import Models.Person;
import Models.User;
import Requests.LoadRequest;
import Response.ClearResponse;
import Response.LoadResponse;

/**
 * Class for servicing the load request.
 * Created by Kaiden on 2/10/2018.
 */

public class LoadService extends Service {
    private LoadRequest loadRequest;
    /**
     * Default constructor for the load service class.
     *
     * @param loadRequest The LoadRequest to be made.
     */
    public LoadService(LoadRequest loadRequest) {
        this.loadRequest = loadRequest;
    }

    /**
     * Clears all data from the database (just like the /clear API), and then loads the posted user, person, and event data into the database. Used for testing
     *
     * @return Returns a LoadResponse, either with an error message or with an three separate arrays of user, person, and event objects.
     */
    public LoadResponse loadTest() {
        return implementLoad(true);
    }

    /**
     * Clears all data from the database (just like the /clear API), and then loads the posted user, person, and event data into the database.
     *
     * @return Returns a LoadResponse, either with an error message or with an three separate arrays of user, person, and event objects.
     */
    public LoadResponse load() {
        return implementLoad(false);
    }

    /**
     * Implements the load methods
     *
     * @param test Whether the method is being called to test or to load the actual database
     * @return Returns a LoadResponse, either with an error message or with an three separate arrays of user, person, and event objects.
     */
    private LoadResponse implementLoad(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        LoadResponse loadResponse = new LoadResponse();

        // Create an instance of the clear service
        ClearService clearService = new ClearService();

        // Clear the database
        ClearResponse clearResponse;
        if (test) {
            clearResponse = clearService.clearTest();
        } else {
            clearResponse = clearService.clear();
        }

        // If there was an error, let the user know and stop processing
        if (clearResponse.getHasError()) {
            setErrorMessage(loadResponse, clearResponse.getErrorMessage());
            return loadResponse;
        }

        try {
            // Extract the array of users, people, and events from the encapsulated data
            User[] users = loadRequest.getUsers();
            Person[] people = loadRequest.getPersons();
            Event[] events = loadRequest.getEvents();

            // Open the database connection
            if (test) {
                database.userDao.openTestConnection();
            } else {
                database.userDao.openConnection();
            }

            // Add all the users to the database
            for(int i=0;i<users.length;i++) {
                database.userDao.addUser(users[i]);
            }

            // Close the database connection
            database.userDao.closeConnection(true);

            // Open the database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Add all the people to the database
            for(int i=0;i<people.length;i++) {
                database.personDao.addPerson(people[i]);
            }

            // Close the database connection
            database.personDao.closeConnection(true);

            // Open the database connection
            if (test) {
                database.eventDao.openTestConnection();
            } else {
                database.eventDao.openConnection();
            }

            // Add all the events to the database
            for(int i=0;i<events.length;i++) {
                database.eventDao.addEvent(events[i]);
            }

            // Close the database connection
            database.eventDao.closeConnection(true);

            // Set the response of the data container (success, how many users, people, and events were added)
            loadResponse.setMessage(users.length,people.length,events.length);

        } catch (SQLException ex) {
            setErrorMessage(loadResponse, "Internal server error");
        }
        return loadResponse;
    }
}
