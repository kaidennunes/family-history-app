package Services;

import java.sql.SQLException;
import java.util.UUID;

import javax.xml.crypto.Data;

import Dao.Database;
import Models.AuthToken;
import Models.User;
import Requests.LoginRequest;
import Response.LoginResponse;

/**
 * Class for servicing the login request.
 * Created by Kaiden on 2/10/2018.
 */

public class LoginService extends Service {
    private LoginRequest loginRequest;

    /**
     * Default constructor for the login service class.
     *
     * @param loginRequest The LoginRequest to be made.
     */
    public LoginService(LoginRequest loginRequest) {
        this.loginRequest = loginRequest;
    }

    /**
     * Logs in the user. Used for testing
     *
     * @return Returns a LoginResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    public LoginResponse loginTest() {
        return implementLogin(true);
    }

    /**
     * Logs in the user.
     *
     * @return Returns a LoginResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    public LoginResponse login() {
        return implementLogin(false);
    }

    /**
     * Implements the login methods
     *
     * @param test Whether the method is being called to test or to view the actual database
     * @return Returns a LoginResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    private LoginResponse implementLogin(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        LoginResponse loginResponse = new LoginResponse("", "", "");

        // Decapsulate the data
        String userName = loginRequest.getUserName();
        String userPassword = loginRequest.getUserPassword();

        // Check to see if any of the data fields are null. If they are, stop processing and return an error encapsulated in a LoginResponse
        if (userName == null || userPassword == null) {
            setErrorMessage(loginResponse, "Request property missing or has invalid value");
            return loginResponse;
        }
        try {
            // Open the user table connection
            if (test) {
                database.userDao.openTestConnection();
            } else {
                database.userDao.openConnection();
            }

            // If the username and password combination doesn't exist, stop processing and return an error encapsulated in a LoginResponse
            if (!database.userDao.isValidUser(userName, userPassword)) {
                setErrorMessage(loginResponse, "User does not exist or password is invalid");
                database.userDao.closeConnection(false);
                return loginResponse;
            }

            // Get the user
            User user = database.userDao.getUser(userName);

            // Close the user table connection
            database.userDao.closeConnection(false);

            // Create an authentication token string
            String authTokenString = UUID.randomUUID().toString();

            // Create an authentication token object
            AuthToken authToken = new AuthToken(authTokenString, userName);

            // Opens the authentication token table connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Add the authentication token
            database.authTokenDao.addAuthToken(authToken);

            // Close the authentication token table connection
            database.authTokenDao.closeConnection(true);

            // Encapsulate the response data (for success)
            loginResponse.setAuthToken(authTokenString);
            loginResponse.setUserName(userName);
            loginResponse.setPersonId(user.getPersonId());

        } catch (SQLException ex) {
            setErrorMessage(loginResponse, "Internal server error");
        }

        // Return the encapsulated response data
        return loginResponse;
    }
}
