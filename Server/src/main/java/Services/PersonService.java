package Services;

import java.sql.SQLException;

import Dao.Database;
import Models.Person;
import Requests.PersonRequest;
import Response.PersonResponse;

/**
 * Class for servicing the person request.
 * Created by Kaiden on 2/10/2018.
 */

public class PersonService extends Service {
    private PersonRequest personRequest;

    /**
     * Default constructor for the person service class.
     *
     * @param personRequest The PersonRequest to be made.
     */
    public PersonService(PersonRequest personRequest) {
        this.personRequest = personRequest;
    }

    /**
     * Gets the person data for the given PersonRequest. Used for testing.
     *
     * @return Returns a PersonResponse, with either an error message or an array of Person objects with size of 1.
     */
    public PersonResponse getPersonTest() {
        return implementGetPerson(true);
    }

    /**
     * Gets the person data for the given PersonRequest.
     *
     * @return Returns a PersonResponse, with either an error message or an array of Person objects with size of 1.
     */
    public PersonResponse getPerson() {
        return implementGetPerson(false);
    }


    /**
     * Implements the get person methods.
     *
     * @param test Whether the method is being called to test or to view the actual database
     * @return Returns a PersonResponse, with either an error message or an array of Person objects with size of 1.
     */
    private PersonResponse implementGetPerson(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        PersonResponse personResponse = new PersonResponse();

        try {
            // Try to open the database connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Get the username with which the authentication token is associated
            String username = database.authTokenDao.getAuthToken(personRequest.getAuthToken());

            // Close the database connection
            database.authTokenDao.closeConnection(false);

            // If the authentication token is invalid, tell the user that and stop processing the request
            if (username == null) {
                setErrorMessage(personResponse, "Invalid auth token");
                return personResponse;
            }

            // Try to open the database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Find the person with the person ID in the request
            Person person = database.personDao.getPerson(personRequest.getPersonId());

            // Close the database connection
            database.personDao.closeConnection(false);

            // If an person with that ID doesn't exists tell the user that and stop processing the request
            if (person == null) {
                setErrorMessage(personResponse, "Invalid personID parameter");
                return personResponse;
            }

            // See if user is authorized to view the person
            if (!username.equals(person.getDescendant())) {
                setErrorMessage(personResponse, "Requested person does not belong to this user");
                return personResponse;
            }

            // Put the person in an person array
            Person[] people = {person};

            // Use the person array to set the people in the EventResponse
            personResponse.setPersons(people);

        } catch (SQLException ex) {
            setErrorMessage(personResponse, "Internal server error");
        }
        return personResponse;
    }

    /**
     * Gets the person data for the given PersonRequest. Used for testing
     *
     * @return Returns a PersonResponse, with either an error message or an array of Person objects for all people with the logged in user as a descendant.
     */
    public PersonResponse getPeopleTest() {
        return implementGetPeople(true);
    }

    /**
     * Gets the person data for the given PersonRequest.
     *
     * @return Returns a PersonResponse, with either an error message or an array of Person objects for all people with the logged in user as a descendant.
     */
    public PersonResponse getPeople() {
        return implementGetPeople(false);
    }

    /**
     * Implements the get people methods.
     *
     * @param test Whether the method is being called to test or to view the actual database
     * @return Returns a PersonResponse, with either an error message or an array of Person objects for all people with the logged in user as a descendant.
     */
    private PersonResponse implementGetPeople(boolean test) {
        // Create an instance of the database class
        Database database = new Database();

        // Create the encapsulated data holder
        PersonResponse personResponse = new PersonResponse();

        try {
            // Try to open the database connection
            if (test) {
                database.authTokenDao.openTestConnection();
            } else {
                database.authTokenDao.openConnection();
            }

            // Get the username with which the authentication token is associated
            String username = database.authTokenDao.getAuthToken(personRequest.getAuthToken());

            // Close the database connection
            database.authTokenDao.closeConnection(false);

            // If the authentication token is invalid, tell the user that and stop processing the request
            if (username == null) {
                setErrorMessage(personResponse, "Invalid auth token");
                return personResponse;
            }

            // Try to open the database connection
            if (test) {
                database.personDao.openTestConnection();
            } else {
                database.personDao.openConnection();
            }

            // Find the all people associated with the user
            Person[] people = database.personDao.getAllPeopleForUsername(username);

            // Close the database connection
            database.personDao.closeConnection(false);

            /*
            // If there were no people, tell the user that
            if (people == null) {
                setErrorMessage(personResponse, "Invalid eventID parameter");
                return personResponse;
            }
            /**/

            // Use the event array to set the people in the EventResponse
            personResponse.setPersons(people);

        } catch (SQLException ex) {
            setErrorMessage(personResponse, "Internal server error");
        }
        return personResponse;
    }
}
