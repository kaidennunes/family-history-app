package Services;

import Dao.Database;
import Models.User;
import Requests.FillRequest;
import Requests.LoginRequest;
import Requests.RegisterRequest;
import Response.FillResponse;
import Response.LoginResponse;
import Response.RegisterResponse;

import java.sql.SQLException;
import java.util.UUID;

/**
 * Class for servicing the register request.
 * Created by Kaiden on 2/10/2018.
 */

public class RegisterService extends Service {
    private RegisterRequest registerRequest;

    /**
     * Default constructor for the register service class.
     *
     * @param registerRequest The RegisterRequest to be made.
     */
    public RegisterService(RegisterRequest registerRequest) {
        this.registerRequest = registerRequest;
    }

    /**
     * Creates a new user account, generates 4 generations of ancestor data for the new user, and logs the user in. Used for testing
     *
     * @return Returns a RegisterResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    public RegisterResponse registerTest() {
        return implementRegister(true);
    }

    /**
     * Creates a new user account, generates 4 generations of ancestor data for the new user, and logs the user in.
     *
     * @return Returns a RegisterResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    public RegisterResponse register() {
        return implementRegister(false);
    }

    /**
     * Implements the register methods.
     *
     * @param test Whether the method is being called to test or to register an actual user
     * @return Returns a RegisterResponse, either with an error message or with authentication token, username, and personId of the user.
     */
    private RegisterResponse implementRegister(boolean test) {
        Database database = new Database();

        // Create the encapsulated data holder
        RegisterResponse registerResponse = new RegisterResponse("", "", "");

        try {
            // Try to open the database connection
            if (test) {
                database.userDao.openTestConnection();
            } else {
                database.userDao.openConnection();
            }

            // If the user already exists, stop processing and return an error encapsulated in a RegisterResponse
            if (database.userDao.existUserName(registerRequest.getUserName())) {
                setErrorMessage(registerResponse, "Username already taken by another user");
                database.userDao.closeConnection(false);
                return registerResponse;
            }

            // Decapsulate the data
            String userName = registerRequest.getUserName();
            String userPassword = registerRequest.getUserPassword();
            String email = registerRequest.getEmail();
            String firstName = registerRequest.getFirstName();
            String lastName = registerRequest.getLastName();
            char gender = registerRequest.getGender();

            // Check to see if any of the data fields are null. If they are, stop processing and return an error encapsulated in a RegisterResponse
            if (userName == null || userPassword == null || email == null || firstName == null || lastName == null || gender == ' ') {
                setErrorMessage(registerResponse, "Request property missing or has invalid value");
                database.userDao.closeConnection(false);
                return registerResponse;
            }

            // Otherwise, try to register the user
            String personId = UUID.randomUUID().toString();

            // Create the user encapsulated in the User model class
            User user = new User(userName, userPassword, email, firstName, lastName, gender, personId);

            // Try to add the user to the database
            if (!database.userDao.addUser(user)) {
                setErrorMessage(registerResponse, "Internal server error");
                database.userDao.closeConnection(false);
                return registerResponse;
            }

            // Close the database connection
            database.userDao.closeConnection(true);

            // Encapsulate the fill information into a FillRequest
            FillRequest fillRequest = new FillRequest(userName);

            // Create an instance of the fill service
            FillService fillService = new FillService(fillRequest);

            // Fill in the user genealogy
            FillResponse fillResponse;
            if (test) {
                fillResponse = fillService.fillTest();
            } else {
                fillResponse = fillService.fill();
            }

            // If there was an error, let the user know, but don't delete the user from the database
            if (fillResponse.getHasError()) {
                setErrorMessage(registerResponse, fillResponse.getErrorMessage());
                return registerResponse;
            }

            // Otherwise, encapsulate the login information into a LoginRequest
            LoginRequest loginRequest = new LoginRequest(userName, userPassword);

            // Create an instance of the login service
            LoginService loginService = new LoginService(loginRequest);

            // Login the user
            LoginResponse loginResponse;
            if (test) {
                loginResponse = loginService.loginTest();
            } else {
                loginResponse = loginService.login();
            }

            // If there was an error, let the user know, but don't delete the user or his family history info from the database
            if (loginResponse.getHasError()) {
                setErrorMessage(registerResponse, loginResponse.getErrorMessage());
                return registerResponse;
            }

            // Encapsulate the response data (for success)
            registerResponse.setAuthToken(loginResponse.getAuthToken());
            registerResponse.setUserName(userName);
            registerResponse.setPersonId(personId);

        } catch (SQLException ex) {
            setErrorMessage(registerResponse, "Internal server error");
        }
        // Return the encapsulated response data
        return registerResponse;
    }
}
