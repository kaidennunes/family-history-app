package Services;

import Response.Response;

/**
 * Created by Kaiden on 2/25/2018.
 */

public class Service {
    protected void setErrorMessage(Response response, String errorMessage) {
        response.setHasError(true);
        response.setErrorMessage(errorMessage);
    }
}
