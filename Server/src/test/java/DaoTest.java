import org.junit.Test;

import java.sql.SQLException;

import Dao.Database;
import Models.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * A test database is used that has the same tables as the real database. If the real database tables were to change, just copy the database file over and del
 * Created by Kaiden on 2/16/2018.
 */

public class DaoTest {

    @Test
    public void userDaoTest() {
        Database testDatabase = new Database();
        try {
            testDatabase.userDao.openTestConnection();

            testDatabase.userDao.deleteAllUsers();

            User testUser = new User("test", "testing", "test@byu.edu", "Tester", "Testering", 'f', "123456789");
            testDatabase.userDao.addUser(testUser);

            assertTrue(testDatabase.userDao.existUserName("test"));
            assertFalse(testDatabase.userDao.existUserName("testing"));

            User testUserDuplicate = testDatabase.userDao.getUser("test");
            assertTrue(testUser.equals(testUserDuplicate));

            assertFalse(testDatabase.userDao.isValidUser("test", ""));
            assertFalse(testDatabase.userDao.isValidUser("", "testing"));
            assertTrue(testDatabase.userDao.isValidUser("test", "testing"));

            testDatabase.userDao.deleteAllUsers();

            assertFalse(testDatabase.userDao.existUserName("test"));
            assertFalse(testDatabase.userDao.existUserName("testing"));

            assertFalse(testDatabase.userDao.isValidUser("test", ""));
            assertFalse(testDatabase.userDao.isValidUser("", "testing"));
            assertFalse(testDatabase.userDao.isValidUser("test", "testing"));

            testDatabase.userDao.closeConnection(false);

        } catch (SQLException ex) {
            System.out.println("ERROR!");
        }
    }

    @Test
    public void personDaoTest() {
        Database testDatabase = new Database();
        try {
            testDatabase.personDao.openTestConnection();

            testDatabase.personDao.deleteAllPeople();

            Person testPerson = new Person("123456789", "tester", "testering", "name", 'f', "man", "woman", "guy");

            assertTrue(testDatabase.personDao.getPerson("123456789") == null);

            testDatabase.personDao.addPerson(testPerson);

            Person testPersonDuplicate = testDatabase.personDao.getPerson("123456789");

            assertTrue(testPerson.equals(testPersonDuplicate));

            Person testPerson2 = new Person("987654321", "test", "testering", "name", 'f', "man", "woman", "guy");

            testDatabase.personDao.addPerson(testPerson2);
            assertFalse(testDatabase.personDao.getPerson("987654321").equals(testPerson));

            Person testPerson3 = new Person("987654321", "tester", "testering", "name", 'f', "man", "woman", "guy");

            testDatabase.personDao.addPerson(testPerson3);

            Person[] persons = testDatabase.personDao.getAllPeopleForUsername("tester");
            Person[] persons2 = testDatabase.personDao.getAllPeopleForUsername("test");

            assertTrue(persons.length == 2);

            assertTrue(persons[0].equals(testPerson));
            assertTrue(persons[1].equals(testPerson3));

            assertTrue(persons2.length == 1);

            assertTrue(persons2[0].equals(testPerson2));

            assertFalse(persons2[0].equals(persons[0]));

            testDatabase.personDao.deleteAllPeopleForUsername("tester");

            Person[] deletedPersons = testDatabase.personDao.getAllPeopleForUsername("tester");

            assertTrue(deletedPersons == null);

            Person[] morePersons = testDatabase.personDao.getAllPeopleForUsername("test");
            for (int i = 0; i < persons2.length; i++) {
                assertTrue(persons2[i].equals(morePersons[0]));
            }

            testDatabase.personDao.addPerson(testPerson);
            testDatabase.personDao.deleteAllPeople();

            assertTrue(testDatabase.personDao.getAllPeopleForUsername("test") == null);
            assertTrue(testDatabase.personDao.getAllPeopleForUsername("tester") == null);

            assertTrue(testDatabase.personDao.getPerson("123456789") == null);


            testDatabase.personDao.closeConnection(false);

        } catch (SQLException ex) {
            System.out.println("ERROR!");
        }
    }

    @Test
    public void authTokenDaoTest() {
        Database testDatabase = new Database();
        try {
            testDatabase.authTokenDao.openTestConnection();

            testDatabase.authTokenDao.deleteAllAuthTokens();

            AuthToken testAuthToken = new AuthToken("123456789", "test");

            assertFalse("test".equals(testDatabase.authTokenDao.getAuthToken("123456789")));
            assertTrue(testDatabase.authTokenDao.getAuthToken("123456789") == null);

            testDatabase.authTokenDao.addAuthToken(testAuthToken);

            assertTrue(testDatabase.authTokenDao.getAuthToken("123456789").equals("test"));
            assertFalse(testDatabase.authTokenDao.getAuthToken("123456789").equals(" "));
            assertTrue(testDatabase.authTokenDao.getAuthToken("987654321") == null);

            testDatabase.authTokenDao.deleteAllAuthTokens();
            assertFalse("test".equals(testDatabase.authTokenDao.getAuthToken("123456789")));
            assertTrue(testDatabase.authTokenDao.getAuthToken("123456789") == null);

            testDatabase.authTokenDao.closeConnection(false);

        } catch (SQLException ex) {
            System.out.println("ERROR!");
        }
    }

    @Test
    public void eventDaoTest() {
        Database testDatabase = new Database();
        try {
            testDatabase.eventDao.openTestConnection();

            testDatabase.eventDao.deleteAllEvents();

            Event testEvent = new Event("123456789", "tester", "testering", 12.3, 15.6, "home", "place", "tests", 2018);

            assertTrue(testDatabase.eventDao.getEvent("123456789") == null);

            testDatabase.eventDao.addEvent(testEvent);

            Event testEventDuplicate = testDatabase.eventDao.getEvent("123456789");

            assertTrue(testEvent.equals(testEventDuplicate));

            Event testEvent2 = new Event("987654321", "test", "testering", 12.3, 15.6, "home", "place", "tests", 2018);

            testDatabase.eventDao.addEvent(testEvent2);
            assertFalse(testDatabase.eventDao.getEvent("987654321").equals(testEvent));

            Event testEvent3 = new Event("456789123", "tester", "testering", 12.3, 15.6, "home", "place", "tests", 2018);

            testDatabase.eventDao.addEvent(testEvent3);

            Event[] events = testDatabase.eventDao.getAllEventsForUsername("tester");
            Event[] events2 = testDatabase.eventDao.getAllEventsForUsername("test");

            assertTrue(events.length == 2);

            assertTrue(events[0].equals(testEvent));
            assertTrue(events[1].equals(testEvent3));

            assertTrue(events2.length == 1);

            assertTrue(events2[0].equals(testEvent2));

            assertFalse(events2[0].equals(events[0]));

            testDatabase.eventDao.deleteAllEventsForUsername("tester");

            Event[] deletedEvents = testDatabase.eventDao.getAllEventsForUsername("tester");

            assertTrue(deletedEvents == null);

            Event[] moreEvents = testDatabase.eventDao.getAllEventsForUsername("test");
            for (int i = 0; i < events2.length; i++) {
                assertTrue(events2[i].equals(moreEvents[0]));
            }

            testDatabase.eventDao.addEvent(testEvent);
            testDatabase.eventDao.deleteAllEvents();

            assertTrue(testDatabase.eventDao.getAllEventsForUsername("test") == null);
            assertTrue(testDatabase.eventDao.getAllEventsForUsername("tester") == null);

            assertTrue(testDatabase.eventDao.getEvent("123456789") == null);

            testDatabase.eventDao.closeConnection(false);

        } catch (SQLException ex) {
            System.out.println("ERROR!");
        }
    }
}
