import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.junit.Test;

import java.io.*;
import java.net.*;

import Handlers.*;
import Server.Server;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Kaiden on 2/20/2018.
 */

public class HandlersTest {
    private final String serverHost = "192.168.0.102";
    private final String serverPort = "8000";

    @Test
    public void clearHandlerTest() {

    }

    @Test
    public void eventHandlerTest() {

    }

    @Test
    public void fillHandlerTest() {

    }

    @Test
    public void loadHandlerTest() {

    }

    @Test
    public void loginHandlerTest() {

    }

    @Test
    public void personHandlerTest() {

    }

    @Test
    public void registerHandlerTest() {
        /*
        Server server = new Server();
        String[] args = new String[1];
        args[0] = serverPort;
        server.main(args);

        try {
            // Create a URL indicating where the server is running, and which
            // web API operation we want to call
            URL url = new URL("http://" + serverHost + ":" + serverPort + "/user/register");

            // Start constructing our HTTP request
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            http.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            http.setDoOutput(true);

            http.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
            http.connect();

            // This is the JSON string we will send in the HTTP request body
            String reqData = "{"
                    + "\"userName\": \"susan\","
                    + "\"password\": \"mysecret\","
                    + "\"email\": \"susan@gmail.com\","
                    + "\"firstName\": \"Susan\","
                    + "\"lastName\": \"Ellis\","
                    + "\"gender\": \"f\""
                    + "}";

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = http.getOutputStream();

            // Create an instance of the default handler
            Handler handler = new Handler();

            // Write the JSON data to the request body with the default handler
            handler.writeString(reqData, reqBody);

            // Close the request body output stream, indicating that the request is complete
            reqBody.close();

            // Log the results
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // The HTTP response status code indicates success, so print a success message
                System.out.println("User successfully registered.");
            } else {
                // The HTTP response status code indicates an error occurred, so print out the message from the HTTP response
                System.out.println("ERROR: " + http.getResponseMessage());
            }
        } catch (IOException e) {
            // An exception was thrown, so display the exception's stack trace
            e.printStackTrace();
        }
            /**/

    }

    @Test
    public void handlerTest() {

    }
}
