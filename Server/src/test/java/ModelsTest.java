import org.junit.Test;

import Models.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Kaiden on 2/16/2018.
 */

public class ModelsTest {

    @Test
    public void userModelTest() {
        User testUser = new User("test", "testing", "test@byu.edu", "Tester", "Testering", 'f', "123456789");
        assertTrue(testUser.getUserName().equals("test"));
        assertTrue(testUser.getUserPassword().equals("testing"));
        assertTrue(testUser.getEmail().equals("test@byu.edu"));
        assertTrue(testUser.getFirstName().equals("Tester"));
        assertTrue(testUser.getLastName().equals("Testering"));
        assertTrue(testUser.getGender() == 'f');
        assertTrue(testUser.getPersonId().equals("123456789"));

        User testUser2 = new User("test", "testing", "test@byu.edu", "Tester", "Testering", 'f', "123456789");
        assertTrue(testUser.equals(testUser));
        assertTrue(testUser.equals(testUser2));

        testUser.setUserName("AnotherTest");
        testUser.setUserPassword("FunTest");
        testUser.setEmail("testering@byu.edu");
        testUser.setFirstName("Great");
        testUser.setLastName("Tests");
        testUser.setGender('m');
        testUser.setPersonId("987654321");

        assertFalse(testUser.getUserName().equals("test"));
        assertFalse(testUser.getUserPassword().equals("testing"));
        assertFalse(testUser.getEmail().equals("test@byu.edu"));
        assertFalse(testUser.getFirstName().equals("Tester"));
        assertFalse(testUser.getLastName().equals("Testering"));
        assertFalse(testUser.getGender() == 'f');
        assertFalse(testUser.getPersonId().equals("123456789"));

        assertFalse(testUser.equals(testUser2));

        assertTrue(testUser.getUserName().equals("AnotherTest"));
        assertTrue(testUser.getUserPassword().equals("FunTest"));
        assertTrue(testUser.getEmail().equals("testering@byu.edu"));
        assertTrue(testUser.getFirstName().equals("Great"));
        assertTrue(testUser.getLastName().equals("Tests"));
        assertTrue(testUser.getGender() == 'm');
        assertTrue(testUser.getPersonId().equals("987654321"));
    }

    @Test
    public void personModelTest() {
        Person testPerson1 = new Person("987654321", "tester", "testering", "name", 'f', "man", "woman", "guy");

        assertTrue(testPerson1.getPersonId().equals("987654321"));
        assertTrue(testPerson1.getDescendant().equals("tester"));
        assertTrue(testPerson1.getFirstName().equals("testering"));
        assertTrue(testPerson1.getLastName().equals("name"));
        assertTrue(testPerson1.getGender() == 'f');
        assertTrue(testPerson1.getFather().equals("man"));
        assertTrue(testPerson1.getMother().equals("woman"));
        assertTrue(testPerson1.getSpouse().equals("guy"));


        Person testPerson1Duplicate = new Person("987654321", "tester", "testering", "name", 'f', "man", "woman", "guy");
        assertTrue(testPerson1.equals(testPerson1));
        assertTrue(testPerson1.equals(testPerson1Duplicate));

        testPerson1.setPersonId("123456789");
        testPerson1.setDescendant("another");
        testPerson1.setFirstName("first");
        testPerson1.setLastName("last");
        testPerson1.setGender('m');
        testPerson1.setFather("father");
        testPerson1.setMother("mother");
        testPerson1.setSpouse("spouse");

        assertFalse(testPerson1.getPersonId().equals("987654321"));
        assertFalse(testPerson1.getDescendant().equals("tester"));
        assertFalse(testPerson1.getFirstName().equals("testering"));
        assertFalse(testPerson1.getLastName().equals("name"));
        assertFalse(testPerson1.getGender() == 'f');
        assertFalse(testPerson1.getFather().equals("man"));
        assertFalse(testPerson1.getMother().equals("woman"));
        assertFalse(testPerson1.getSpouse().equals("guy"));


        assertFalse(testPerson1.equals(testPerson1Duplicate));

        assertTrue(testPerson1.getPersonId().equals("123456789"));
        assertTrue(testPerson1.getDescendant().equals("another"));
        assertTrue(testPerson1.getFirstName().equals("first"));
        assertTrue(testPerson1.getLastName().equals("last"));
        assertTrue(testPerson1.getGender() == 'm');
        assertTrue(testPerson1.getFather().equals("father"));
        assertTrue(testPerson1.getMother().equals("mother"));
        assertTrue(testPerson1.getSpouse().equals("spouse"));


        Person testPerson2 = new Person("987654321", "tester", "testering", "name", 'f', "man", "woman");


        assertTrue(testPerson2.getPersonId().equals("987654321"));
        assertTrue(testPerson2.getDescendant().equals("tester"));
        assertTrue(testPerson2.getFirstName().equals("testering"));
        assertTrue(testPerson2.getLastName().equals("name"));
        assertTrue(testPerson2.getGender() == 'f');
        assertTrue(testPerson2.getFather().equals("man"));
        assertTrue(testPerson2.getMother().equals("woman"));
        assertTrue(testPerson2.getSpouse() == null);

        testPerson2.setPersonId("123456789");
        testPerson2.setDescendant("another");
        testPerson2.setFirstName("first");
        testPerson2.setLastName("last");
        testPerson2.setGender('m');
        testPerson2.setFather("father");
        testPerson2.setMother("mother");
        testPerson2.setSpouse("spouse");

        assertFalse(testPerson2.getPersonId().equals("987654321"));
        assertFalse(testPerson2.getDescendant().equals("tester"));
        assertFalse(testPerson2.getFirstName().equals("testering"));
        assertFalse(testPerson2.getLastName().equals("name"));
        assertFalse(testPerson2.getGender() == 'f');
        assertFalse(testPerson2.getFather().equals("man"));
        assertFalse(testPerson2.getMother().equals("woman"));
        assertFalse(testPerson2.getSpouse() == null);

        assertTrue(testPerson2.getPersonId().equals("123456789"));
        assertTrue(testPerson2.getDescendant().equals("another"));
        assertTrue(testPerson2.getFirstName().equals("first"));
        assertTrue(testPerson2.getLastName().equals("last"));
        assertTrue(testPerson1.getGender() == 'm');
        assertTrue(testPerson2.getFather().equals("father"));
        assertTrue(testPerson2.getMother().equals("mother"));
        assertTrue(testPerson2.getSpouse().equals("spouse"));

        Person testPerson3 = new Person("987654321", "tester", "testering", "name", 'f', "man");


        assertTrue(testPerson3.getPersonId().equals("987654321"));
        assertTrue(testPerson3.getDescendant().equals("tester"));
        assertTrue(testPerson3.getFirstName().equals("testering"));
        assertTrue(testPerson3.getLastName().equals("name"));
        assertTrue(testPerson3.getGender() == 'f');
        assertTrue(testPerson3.getFather().equals("man"));
        assertTrue(testPerson3.getMother() == null);
        assertTrue(testPerson3.getSpouse() == null);

        testPerson3.setPersonId("123456789");
        testPerson3.setDescendant("another");
        testPerson3.setFirstName("first");
        testPerson3.setLastName("last");
        testPerson3.setGender('m');
        testPerson3.setFather("father");
        testPerson3.setMother("mother");
        testPerson3.setSpouse("spouse");

        assertFalse(testPerson3.getPersonId().equals("987654321"));
        assertFalse(testPerson3.getDescendant().equals("tester"));
        assertFalse(testPerson3.getFirstName().equals("testering"));
        assertFalse(testPerson3.getLastName().equals("name"));
        assertFalse(testPerson3.getGender() == 'f');
        assertFalse(testPerson3.getFather().equals("man"));
        assertFalse(testPerson3.getMother() == null);
        assertFalse(testPerson3.getSpouse() == null);

        assertTrue(testPerson3.getPersonId().equals("123456789"));
        assertTrue(testPerson3.getDescendant().equals("another"));
        assertTrue(testPerson3.getFirstName().equals("first"));
        assertTrue(testPerson3.getLastName().equals("last"));
        assertTrue(testPerson3.getGender() == 'm');
        assertTrue(testPerson3.getFather().equals("father"));
        assertTrue(testPerson3.getMother().equals("mother"));
        assertTrue(testPerson3.getSpouse().equals("spouse"));

        Person testPerson4 = new Person("987654321", "tester", "testering", "name", 'f');

        assertTrue(testPerson4.getPersonId().equals("987654321"));
        assertTrue(testPerson4.getDescendant().equals("tester"));
        assertTrue(testPerson4.getFirstName().equals("testering"));
        assertTrue(testPerson4.getLastName().equals("name"));
        assertTrue(testPerson4.getGender() == 'f');
        assertTrue(testPerson4.getFather() == null);
        assertTrue(testPerson4.getMother() == null);
        assertTrue(testPerson4.getSpouse() == null);


        Person testPerson4Duplicate = new Person("987654321", "tester", "testering", "name", 'f');
        assertTrue(testPerson4.equals(testPerson4));
        assertTrue(testPerson4Duplicate.equals(testPerson4));

        testPerson4.setPersonId("123456789");
        testPerson4.setDescendant("another");
        testPerson4.setFirstName("first");
        testPerson4.setLastName("last");
        testPerson4.setGender('m');
        testPerson4.setFather("father");
        testPerson4.setMother("mother");
        testPerson4.setSpouse("spouse");

        assertFalse(testPerson4.getPersonId().equals("987654321"));
        assertFalse(testPerson4.getDescendant().equals("tester"));
        assertFalse(testPerson4.getFirstName().equals("testering"));
        assertFalse(testPerson4.getLastName().equals("name"));
        assertFalse(testPerson4.getGender() == 'f');
        assertFalse(testPerson4.getFather() == null);
        assertFalse(testPerson4.getMother() == null);
        assertFalse(testPerson4.getSpouse() == null);

        assertTrue(testPerson4.getPersonId().equals("123456789"));
        assertTrue(testPerson4.getDescendant().equals("another"));
        assertTrue(testPerson4.getFirstName().equals("first"));
        assertTrue(testPerson4.getLastName().equals("last"));
        assertTrue(testPerson4.getGender() == 'm');
        assertTrue(testPerson4.getFather().equals("father"));
        assertTrue(testPerson4.getMother().equals("mother"));
        assertTrue(testPerson4.getSpouse().equals("spouse"));
    }

    @Test
    public void eventModelTest() {
        Event testEvent = new Event("123456789", "tester", "testering", 12.3, 15.6, "home", "place", "tests", 2018);
        assertTrue(testEvent.getEventId().equals("123456789"));
        assertTrue(testEvent.getDescendant().equals("tester"));
        assertTrue(testEvent.getPerson().equals("testering"));
        assertTrue(testEvent.getLatitude() == 12.3);
        assertTrue(testEvent.getLongitude() == 15.6);
        assertTrue(testEvent.getCountry().equals("home"));
        assertTrue(testEvent.getCity().equals("place"));
        assertTrue(testEvent.getEventType().equals("tests"));
        assertTrue(testEvent.getYear() == 2018);

        Event testEvent2 = new Event("123456789", "tester", "testering", 12.3, 15.6, "home", "place", "tests", 2018);
        assertTrue(testEvent.equals(testEvent));
        assertTrue(testEvent.equals(testEvent2));

        testEvent.setEventId("987654321");
        testEvent.setDescendant("descendant");
        testEvent.setPerson("more");
        testEvent.setLatitude(206.5);
        testEvent.setLongitude(16.8);
        testEvent.setCountry("far");
        testEvent.setCity("away");
        testEvent.setEventType("different");
        testEvent.setYear(2019);

        assertFalse(testEvent.getEventId().equals("123456789"));
        assertFalse(testEvent.getDescendant().equals("tester"));
        assertFalse(testEvent.getPerson().equals("testering"));
        assertFalse(testEvent.getLatitude() == 12.3);
        assertFalse(testEvent.getLongitude() == 15.6);
        assertFalse(testEvent.getCountry().equals("home"));
        assertFalse(testEvent.getCity().equals("place"));
        assertFalse(testEvent.getEventType().equals("tests"));
        assertFalse(testEvent.getYear() == 2018);

        assertFalse(testEvent.equals(testEvent2));

        assertTrue(testEvent.getEventId().equals("987654321"));
        assertTrue(testEvent.getDescendant().equals("descendant"));
        assertTrue(testEvent.getPerson().equals("more"));
        assertTrue(testEvent.getLatitude() == 206.5);
        assertTrue(testEvent.getLongitude() == 16.8);
        assertTrue(testEvent.getCountry().equals("far"));
        assertTrue(testEvent.getCity().equals("away"));
        assertTrue(testEvent.getEventType().equals("different"));
        assertTrue(testEvent.getYear() == 2019);
    }

    @Test
    public void authTokenModelTest() {
        AuthToken testAuthToken = new AuthToken("123456789", "test");
        assertTrue(testAuthToken.getAuthToken().equals("123456789"));
        assertTrue(testAuthToken.getUserName().equals("test"));

        AuthToken testAuthToken2 = new AuthToken("123456789", "test");

        assertTrue(testAuthToken.equals(testAuthToken));
        assertTrue(testAuthToken.equals(testAuthToken2));

        testAuthToken.setAuthToken("987654321");
        testAuthToken.setUserName("fun");

        assertFalse(testAuthToken.getAuthToken().equals("123456789"));
        assertFalse(testAuthToken.getUserName().equals("test"));

        assertFalse(testAuthToken.equals(testAuthToken2));

        assertTrue(testAuthToken.getAuthToken().equals("987654321"));
        assertTrue(testAuthToken.getUserName().equals("fun"));
    }

}
