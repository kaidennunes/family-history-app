import org.junit.Test;

import java.sql.SQLException;

import static Json.Json.loadJson;

import Dao.Database;
import Json.Json;
import Requests.*;
import Response.*;
import Services.*;
import Models.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Based on the assumption that the Dao classes work, as tested in the DaoTest class
 * The test connection allows the tests to freely delete and change the test database as needed.
 * If the real database tables were to change, copy the database file over and delete all rows.
 * Created by Kaiden on 2/20/2018.
 */

public class ServicesTest {

    // Create multiple things to add to the database
    private Person person1 = new Person("person1", "username1", "firstName1", "lastName1", 'm', "father1", "mother1", "spouse1");
    private Event event1 = new Event("event1", "username1", "person1", 12.3, 123.5, "country1", "city1", "eventType1", 123);
    private User user1 = new User("username1", "test1", "email1", "firstName1", "lastName1", 'm', "personId1");
    private AuthToken authToken1 = new AuthToken("authtoken1", "username1");

    private Person person2 = new Person("person2", "username2", "firstName2", "lastName2", 'f', "father2", "mother2", "spouse2");
    private Event event2 = new Event("event2", "username2", "person2", 121.3, 1231.5, "country2", "city2", "eventType2", 1231);
    private User user2 = new User("username2", "test2", "email2", "firstName2", "lastName2", 'f', "personId2");
    private AuthToken authToken2 = new AuthToken("authtoken2", "username2");

    private Person person3 = new Person("person3", "username2", "firstName3", "lastName3", 'f', "father3", "mother3", "spouse3");
    private Event event3 = new Event("event3", "username2", "person3", 121.3, 1231.5, "country3", "city3", "eventType3", 1231);
    private User user3 = new User("user3", "test3", "email3", "firstName3", "lastName3", 'f', "personId3");

    private final int DEFAULT_FILL_EVENTS_PER_PERSON = 3;

    @Test
    public void fillServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create an FillRequest to test with
        FillRequest fillRequestValid1 = new FillRequest(user1.getUserName(), 3);
        FillRequest fillRequestInvalid1 = new FillRequest("test", 3);
        FillRequest fillRequestValid2 = new FillRequest(user1.getUserName(), 4);

        // Create an instance of the FillService class to test
        FillService fillServiceValid1 = new FillService(fillRequestValid1);
        FillService fillServiceInvalid1 = new FillService(fillRequestInvalid1);
        FillService fillServiceValid2 = new FillService(fillRequestValid2);

        // Load the fill data from json
        loadJson();

        try {
            // Clear the database for the test
            clearTestDatabase(database);

            // Open the database connection
            database.userDao.openTestConnection();

            // Add a user to the database
            database.userDao.addUser(user1);

            // Close the database connection
            database.userDao.closeConnection(true);

            // Try to fill the database
            FillResponse fillResponseInvalid1 = fillServiceInvalid1.fillTest();

            // This should not work because the user doesn't exist in the database
            assertTrue(fillResponseInvalid1.getHasError());


            // Try to fill the database again
            FillResponse fillResponseValid1 = fillServiceValid1.fillTest();

            // This was valid, so there should be no error
            assertFalse(fillResponseValid1.getHasError());

            // This should be the response of the message
            assertTrue("Successfully added 15 persons and 45 events to the database.".equals(fillResponseValid1.getMessage()));

            // Open the database connection
            database.personDao.openTestConnection();

            // Get the events that should have been filled
            Person[] people1 = database.personDao.getAllPeopleForUsername(user1.getUserName());

            // Make sure the appropriate number of people were generated (3 generations, 15 people, 4 generations, which is default, 31 people)
            assertTrue(people1 != null && people1.length == 15);

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // Get the events that should have been filled
            Event[] events1 = database.eventDao.getAllEventsForUsername(user1.getUserName());

            // Make sure the appropriate number of events were generated
            assertTrue(events1 != null && events1.length == DEFAULT_FILL_EVENTS_PER_PERSON * 15);

            // Close the database connection
            database.eventDao.closeConnection(false);


            // Try to fill the database again, overriding the previous fill
            FillResponse fillResponseValid2 = fillServiceValid2.fillTest();

            // This was valid, so there should be no error
            assertFalse(fillResponseValid2.getHasError());

            // This should be the response of the message
            assertTrue("Successfully added 31 persons and 93 events to the database.".equals(fillResponseValid2.getMessage()));

            // Open the database connection
            database.personDao.openTestConnection();

            // Get the events that should have been filled
            Person[] people2 = database.personDao.getAllPeopleForUsername(user1.getUserName());

            // Make sure the appropriate number of people were generated (3 generations, 15 people, 4 generations, which is default, 31 people)
            assertTrue(people2 != null && people2.length == 31);

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // Get the events that should have been filled
            Event[] events2 = database.eventDao.getAllEventsForUsername(user1.getUserName());

            // Make sure the appropriate number of events were generated
            assertTrue(events2 != null && events2.length == DEFAULT_FILL_EVENTS_PER_PERSON * 31);

            // Close the database connection
            database.eventDao.closeConnection(false);

            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void clearServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create an instance of the ClearService class to test
        ClearService clearService = new ClearService();

        try {
            // Open the database connection
            database.personDao.openTestConnection();

            // Clear everything to clean the database
            database.personDao.deleteAllPeople();

            // Add multiple things to the database to be able to clear
            assertTrue(database.personDao.addPerson(person1));
            assertTrue(database.personDao.addPerson(person2));

            // Make sure these were added
            assertTrue(database.personDao.getPerson(person1.getPersonId()) != null);
            assertTrue(database.personDao.getPerson(person2.getPersonId()) != null);

            // Close the database connection
            database.personDao.closeConnection(true);

            // Now clear the database using the ClearService
            clearService.clearTest();

            // Open the database connection
            database.personDao.openTestConnection();

            // Make sure these were deleted
            assertTrue(database.personDao.getPerson(person1.getPersonId()) == null);
            assertTrue(database.personDao.getPerson(person2.getPersonId()) == null);

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // Clear everything to clean the database
            database.eventDao.deleteAllEvents();

            // Add multiple things to the database to be able to clear
            assertTrue(database.eventDao.addEvent(event1));
            assertTrue(database.eventDao.addEvent(event2));

            // Make sure these were added
            assertTrue(database.eventDao.getEvent(event1.getEventId()) != null);
            assertTrue(database.eventDao.getEvent(event2.getEventId()) != null);

            // Close the database connection
            database.eventDao.closeConnection(true);

            // Now clear the database using the ClearService
            clearService.clearTest();

            // Open the database connection
            database.eventDao.openTestConnection();

            // Make sure these were deleted
            assertTrue(database.eventDao.getEvent(event1.getEventId()) == null);
            assertTrue(database.eventDao.getEvent(event2.getEventId()) == null);

            // Open the database connection
            database.eventDao.closeConnection(false);


            // Open the database connection
            database.userDao.openTestConnection();

            // Clear everything to clean the database
            database.userDao.deleteAllUsers();

            // Add multiple things to the database to be able to clear
            assertTrue(database.userDao.addUser(user1));
            assertTrue(database.userDao.addUser(user2));

            // Make sure these were added
            assertTrue(database.userDao.getUser(user1.getUserName()) != null);
            assertTrue(database.userDao.getUser(user2.getUserName()) != null);

            // Close the database connection
            database.userDao.closeConnection(true);

            // Now clear the database using the ClearService
            clearService.clearTest();

            // Open the database connection
            database.userDao.openTestConnection();

            // Make sure these were deleted
            assertTrue(database.userDao.getUser(user1.getUserName()) == null);
            assertTrue(database.userDao.getUser(user2.getUserName()) == null);

            // Close the database connection
            database.userDao.closeConnection(false);


            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Clear everything to clean the database
            database.authTokenDao.deleteAllAuthTokens();

            // Add multiple things to the database to be able to clear
            assertTrue(database.authTokenDao.addAuthToken(authToken1));
            assertTrue(database.authTokenDao.addAuthToken(authToken2));

            // Make sure these were added
            assertTrue(database.authTokenDao.getAuthToken(authToken1.getAuthToken()) != null);
            assertTrue(database.authTokenDao.getAuthToken(authToken2.getAuthToken()) != null);

            // Close the database connection
            database.authTokenDao.closeConnection(true);

            // Now clear the database using the ClearService
            clearService.clearTest();

            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Make sure these were deleted
            assertTrue(database.authTokenDao.getAuthToken(authToken1.getAuthToken()) == null);
            assertTrue(database.authTokenDao.getAuthToken(authToken2.getAuthToken()) == null);

            // Close the database connection
            database.authTokenDao.closeConnection(false);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void eventServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create instances of EventRequest to test with
        EventRequest eventRequestValid1 = new EventRequest(authToken1.getAuthToken(), event1.getEventId());
        EventRequest eventRequestInvalid1 = new EventRequest("Invalid", event2.getEventId());
        EventRequest eventRequestInvalidUsername1 = new EventRequest(authToken1.getAuthToken(), event3.getEventId());
        EventRequest eventRequestValid2 = new EventRequest(authToken2.getAuthToken());
        EventRequest eventRequestInvalid2 = new EventRequest("Invalid");

        try {
            // Open the database connection
            database.eventDao.openTestConnection();

            // Clear the database
            database.eventDao.deleteAllEvents();

            // Add the events to the database
            database.eventDao.addEvent(event1);
            database.eventDao.addEvent(event2);
            database.eventDao.addEvent(event3);

            // Close the database connection
            database.eventDao.closeConnection(true);

            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Clear the database
            database.authTokenDao.deleteAllAuthTokens();

            // Add the authentication tokens to the database
            database.authTokenDao.addAuthToken(authToken1);
            database.authTokenDao.addAuthToken(authToken2);

            // Close the database connection
            database.authTokenDao.closeConnection(true);

            // Create instances of the EventService class to test
            EventService eventServiceValid1 = new EventService(eventRequestValid1);
            EventService eventServiceInvalid1 = new EventService(eventRequestInvalid1);
            EventService eventServiceInvalidUsername1 = new EventService(eventRequestInvalidUsername1);
            EventService eventServiceValid2 = new EventService(eventRequestValid2);
            EventService eventServiceInvalid2 = new EventService(eventRequestInvalid2);

            // Create instances of EventResponse to hold the results of the EventService
            EventResponse eventResponseValid1 = eventServiceValid1.getEventTest();
            EventResponse eventResponseInvalid1 = eventServiceInvalid1.getEventTest();
            EventResponse eventResponseInvalidUsername1 = eventServiceInvalidUsername1.getEventTest();
            EventResponse eventResponseValid2 = eventServiceValid2.getEventsTest();
            EventResponse eventResponseInvalid2 = eventServiceInvalid2.getEventsTest();

            // This should work because it is valid
            assertTrue(eventResponseValid1.getEvents()[0].equals(event1));

            // This should not work because the authentication token doesn't exist
            assertTrue(eventResponseInvalid1.getHasError());

            // This should not work because the authentication token is valid, but the username isn't correct
            assertTrue(eventResponseInvalidUsername1.getHasError());

            // This should work because two events were added with the username included in the authentication token
            assertTrue(eventResponseValid2.getEvents().length == 2);

            // This should work because it is valid
            assertTrue(eventResponseValid2.getEvents()[0].equals(event2));

            // This should work because it is valid
            assertTrue(eventResponseValid2.getEvents()[1].equals(event3));

            // This should not work because the authentication token doesn't exist
            assertTrue(eventResponseInvalid2.getHasError());

            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void loadServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create the arrays full of data to test
        User[] users1 = new User[3];
        Person[] people1 = new Person[2];
        Event[] events1 = new Event[1];
        User[] users2 = new User[2];
        Person[] people2 = new Person[1];
        Event[] events2 = new Event[3];

        // Load up the user arrays with objects
        users1[0] = user1;
        users1[1] = user2;
        users1[2] = user3;

        users2[0] = user1;
        users2[1] = user2;

        // Load up the people arrays with objects
        people1[0] = person1;
        people1[1] = person2;

        people2[0] = person3;

        // Load up the event arrays with objects
        events1[0] = event1;

        events2[0] = event1;
        events2[1] = event2;
        events2[2] = event3;

        // Create an instances of the LoadRequest to test with
        LoadRequest loadRequest1 = new LoadRequest(users1, people1, events1);
        LoadRequest loadRequest2 = new LoadRequest(users2, people2, events2);

        // Create an instance of the ClearService class to test
        LoadService loadService1 = new LoadService(loadRequest1);
        LoadService loadService2 = new LoadService(loadRequest2);

        try {
            // Clear the database for testing
            clearTestDatabase(database);

            // Load up the database
            LoadResponse loadResponse1 = loadService1.loadTest();

            // Open the database connection
            database.userDao.openTestConnection();

            // There should be no errors
            assertFalse(loadResponse1.getHasError());

            // See if the users were uploaded
            assertTrue(database.userDao.getUser(users1[0].getUserName()).equals(user1));
            assertTrue(database.userDao.getUser(users1[1].getUserName()).equals(user2));
            assertTrue(database.userDao.getUser(users1[2].getUserName()).equals(user3));

            // Close the database connection
            database.userDao.closeConnection(false);


            // Open the database connection
            database.personDao.openTestConnection();

            // See if the people were uploaded
            assertTrue(database.personDao.getPerson(people1[0].getPersonId()).equals(person1));
            assertTrue(database.personDao.getPerson(people1[1].getPersonId()).equals(person2));

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // See if the events were uploaded
            assertTrue(database.eventDao.getEvent(events1[0].getEventId()).equals(event1));

            // Close the database connection
            database.eventDao.closeConnection(false);


            // Reload the database
            LoadResponse loadResponse2 = loadService2.loadTest();

            // There should be no errors
            assertFalse(loadResponse2.getHasError());

            // Open the database connection
            database.userDao.openTestConnection();

            // See if the users were uploaded (and the previous ones deleted)
            assertTrue(database.userDao.getUser(users2[0].getUserName()).equals(user1));
            assertTrue(database.userDao.getUser(users2[1].getUserName()).equals(user2));

            // This one should have been deleted
            assertTrue(database.userDao.getUser(users1[2].getUserName()) == null);

            // Close the database connection
            database.userDao.closeConnection(false);


            // Open the database connection
            database.personDao.openTestConnection();

            // See if the people were uploaded (and the previous ones deleted)
            assertTrue(database.personDao.getPerson(people2[0].getPersonId()).equals(person3));

            // These ones should have been deleted
            assertTrue(database.personDao.getPerson(people1[0].getPersonId()) == null);
            assertTrue(database.personDao.getPerson(people1[1].getPersonId()) == null);

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // See if the events were uploaded
            assertTrue(database.eventDao.getEvent(events2[0].getEventId()).equals(event1));
            assertTrue(database.eventDao.getEvent(events2[1].getEventId()).equals(event2));
            assertTrue(database.eventDao.getEvent(events2[2].getEventId()).equals(event3));

            // Close the database connection
            database.eventDao.closeConnection(false);

            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void loginServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        try {
            // Clear the database for the test
            clearTestDatabase(database);

            // Try to open the database connection
            database.userDao.openTestConnection();

            // Add the users to the database
            database.userDao.addUser(user1);
            database.userDao.addUser(user2);
            database.userDao.addUser(user3);

            // Close the database connection
            database.userDao.closeConnection(true);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        // Create instances of the LoginRequest to test with
        LoginRequest loginRequestValid1 = new LoginRequest(user1.getUserName(), user1.getUserPassword());
        LoginRequest loginRequestInvalid1 = new LoginRequest(user1.getUserName(), user2.getUserPassword());
        LoginRequest loginRequestValid2 = new LoginRequest(user2.getUserName(), user2.getUserPassword());
        LoginRequest loginRequestInvalid2 = new LoginRequest("testUsername", user3.getUserPassword());
        LoginRequest loginRequestInvalid3 = new LoginRequest(user2.getUserName(), "testPassword");
        LoginRequest loginRequestInvalid4 = new LoginRequest(user3.getUserName(), null);
        LoginRequest loginRequestInvalid5 = new LoginRequest(null, user3.getUserPassword());


        // Create an instances of the LoginService class to test
        LoginService loginServiceValid1 = new LoginService(loginRequestValid1);
        LoginService loginServiceInvalid1 = new LoginService(loginRequestInvalid1);
        LoginService loginServiceValid2 = new LoginService(loginRequestValid2);
        LoginService loginServiceInvalid2 = new LoginService(loginRequestInvalid2);
        LoginService loginServiceInvalid3 = new LoginService(loginRequestInvalid3);
        LoginService loginServiceInvalid4 = new LoginService(loginRequestInvalid4);
        LoginService loginServiceInvalid5 = new LoginService(loginRequestInvalid5);

        // Create instances of LoginResponse to hold the results of the LoginService
        LoginResponse loginResponseValid1 = loginServiceValid1.loginTest();
        LoginResponse loginResponseInvalid1 = loginServiceInvalid1.loginTest();
        LoginResponse loginResponseValid2 = loginServiceValid2.loginTest();
        LoginResponse loginResponseInvalid2 = loginServiceInvalid2.loginTest();
        LoginResponse loginResponseInvalid3 = loginServiceInvalid3.loginTest();
        LoginResponse loginResponseInvalid4 = loginServiceInvalid4.loginTest();
        LoginResponse loginResponseInvalid5 = loginServiceInvalid5.loginTest();

        // This should work because it is a valid request
        assertFalse(loginResponseValid1.getHasError());

        // The authentication tokens should be unique
        assertFalse(loginResponseValid1.getAuthToken().equals(loginResponseValid2.getAuthToken()));

        // The person IDs should be unique
        assertFalse(loginResponseValid1.getPersonId().equals(loginResponseValid2.getPersonId()));

        // The username for the authentication token should be the same as the username in the login request
        assertTrue(loginResponseValid1.getUserName().equals(user1.getUserName()));

        // This should not work because the username and password don't match
        assertTrue(loginResponseInvalid1.getHasError());

        // This should work because it is a valid request
        assertFalse(loginResponseValid2.getHasError());

        // The username for the authentication token should be the same as the username in the login request
        assertTrue(loginResponseValid2.getUserName().equals(user2.getUserName()));

        // This should not work because the username isn't in the database
        assertTrue(loginResponseInvalid2.getHasError());

        // This should not work because the password isn't in the database
        assertTrue(loginResponseInvalid3.getHasError());

        // This should not work because the username is null
        assertTrue(loginResponseInvalid4.getHasError());

        // This should not work because the password is null
        assertTrue(loginResponseInvalid5.getHasError());

        try {
            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Get the username with the associated authentication token that should have been created
            String usernameValid1 = database.authTokenDao.getAuthToken(loginResponseValid1.getAuthToken());

            // The username of the created authentication token and the login response should be the same
            assertTrue(loginResponseValid1.getUserName().equals(usernameValid1));

            // Get the username with the associated authentication token that should have been created
            String usernameValid2 = database.authTokenDao.getAuthToken(loginResponseValid2.getAuthToken());

            // The username of the created authentication token and the login response should be the same
            assertTrue(loginResponseValid2.getUserName().equals(usernameValid2));

            // Close the database connection
            database.authTokenDao.closeConnection(false);

            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void personServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create instances of PersonRequest to test with
        PersonRequest personRequestValid1 = new PersonRequest(authToken1.getAuthToken(), person1.getPersonId());
        PersonRequest personRequestInvalid1 = new PersonRequest("Invalid", person2.getPersonId());
        PersonRequest personRequestInvalidUsername1 = new PersonRequest(authToken1.getAuthToken(), person3.getPersonId());
        PersonRequest personRequestValid2 = new PersonRequest(authToken2.getAuthToken());
        PersonRequest personRequestInvalid2 = new PersonRequest("Invalid");

        try {
            // Open the database connection
            database.personDao.openTestConnection();

            // Clear the database
            database.personDao.deleteAllPeople();

            // Add the events to the database
            database.personDao.addPerson(person1);
            database.personDao.addPerson(person2);
            database.personDao.addPerson(person3);

            // Close the database connection
            database.personDao.closeConnection(true);

            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Clear the database
            database.authTokenDao.deleteAllAuthTokens();

            // Add the authentication tokens to the database
            database.authTokenDao.addAuthToken(authToken1);
            database.authTokenDao.addAuthToken(authToken2);

            // Close the database connection
            database.authTokenDao.closeConnection(true);

            // Create instances of the PersonService class to test
            PersonService personServiceValid1 = new PersonService(personRequestValid1);
            PersonService personServiceInvalid1 = new PersonService(personRequestInvalid1);
            PersonService personServiceInvalidUsername1 = new PersonService(personRequestInvalidUsername1);
            PersonService personServiceValid2 = new PersonService(personRequestValid2);
            PersonService personServiceInvalid2 = new PersonService(personRequestInvalid2);

            // Create instances of PersonResponse to hold the results of the EventService
            PersonResponse personResponseValid1 = personServiceValid1.getPersonTest();
            PersonResponse personResponseInvalid1 = personServiceInvalid1.getPersonTest();
            PersonResponse personResponseInvalidUsername1 = personServiceInvalidUsername1.getPersonTest();
            PersonResponse personResponseValid2 = personServiceValid2.getPeopleTest();
            PersonResponse personResponseInvalid2 = personServiceInvalid2.getPeopleTest();

            // This should work because it is valid
            assertTrue(personResponseValid1.getPersons()[0].equals(person1));

            // This should not work because the authentication token doesn't exist
            assertTrue(personResponseInvalid1.getHasError());

            // This should not work because the authentication token is valid, but the username isn't correct
            assertTrue(personResponseInvalidUsername1.getHasError());

            // This should work because two events were added with the username included in the authentication token
            assertTrue(personResponseValid2.getPersons().length == 2);

            // This should work because it is valid
            assertTrue(personResponseValid2.getPersons()[0].equals(person2));

            // This should work because it is valid
            assertTrue(personResponseValid2.getPersons()[1].equals(person3));

            // This should not work because the authentication token doesn't exist
            assertTrue(personResponseInvalid2.getHasError());

            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void registerServiceTest() {
        // Create an instance of the database
        Database database = new Database();

        // Create instances of the RegisterRequest to test with
        RegisterRequest registerRequestValid1 = new RegisterRequest(user1.getUserName(), user1.getUserPassword(), user1.getEmail(), user1.getFirstName(), user1.getLastName(), user1.getGender());
        RegisterRequest registerRequestInvalid1 = new RegisterRequest(user1.getUserName(), user2.getUserPassword(), user1.getEmail(), user1.getFirstName(), user1.getLastName(), user1.getGender());
        RegisterRequest registerRequestValid2 = new RegisterRequest(user2.getUserName(), user1.getUserPassword(), user2.getEmail(), user2.getFirstName(), user2.getLastName(), user2.getGender());
        RegisterRequest registerRequestValid3 = new RegisterRequest(user3.getUserName(), user3.getUserPassword(), user3.getEmail(), user3.getFirstName(), user3.getLastName(), user3.getGender());

        // Create instances of the RegisterService class to test
        RegisterService registerServiceValid1 = new RegisterService(registerRequestValid1);
        RegisterService registerServiceInvalid1 = new RegisterService(registerRequestInvalid1);
        RegisterService registerServiceValid2 = new RegisterService(registerRequestValid2);
        RegisterService registerServiceValid3 = new RegisterService(registerRequestValid3);

        // Load the fill data from json
        loadJson();

        try {
            // Clear the database for the test
            clearTestDatabase(database);

            // Register the users
            RegisterResponse registerResponseValid1 = registerServiceValid1.registerTest();
            RegisterResponse registerResponseInvalid1 = registerServiceInvalid1.registerTest();
            RegisterResponse registerResponseValid2 = registerServiceValid2.registerTest();
            RegisterResponse registerResponseValid3 = registerServiceValid3.registerTest();

            // There should be no errors for these
            assertFalse(registerResponseValid1.getHasError());
            assertFalse(registerResponseValid2.getHasError());
            assertFalse(registerResponseValid3.getHasError());

            // There should be an error for this one (duplicate username)
            assertTrue(registerResponseInvalid1.getHasError());


            // Open the database connection
            database.userDao.openTestConnection();

            // The register requests should have created valid users
            User userValid1 = database.userDao.getUser(registerRequestValid1.getUserName());
            User userValid2 = database.userDao.getUser(registerRequestValid2.getUserName());
            User userValid3 = database.userDao.getUser(registerRequestValid3.getUserName());

            // Set the person ID of the users to be the person ID generated by the register class
            user1.setPersonId(userValid1.getPersonId());
            user2.setPersonId(userValid2.getPersonId());
            user3.setPersonId(userValid3.getPersonId());

            // These should be equal
            assertTrue(userValid1.equals(user1));
            assertTrue(userValid3.equals(user3));

            // This should be equal to the other, except for the user password, which was changed to make sure that users could have the same password
            assertTrue(userValid2 != null && userValid2.getUserPassword().equals(user1.getUserPassword()));

            // Set the password to be the same, so we can easily check if they are actually equal
            userValid2.setUserPassword(user2.getUserPassword());

            // They should now be equal
            assertTrue(userValid2.equals(user2));

            User user = database.userDao.getUser(registerRequestInvalid1.getUserName());

            // Close the database connection
            database.userDao.closeConnection(false);


            // Open the database connection
            database.personDao.openTestConnection();

            // Get the events that should have been filled
            Person[] people1 = database.personDao.getAllPeopleForUsername(user1.getUserName());
            Person[] people2 = database.personDao.getAllPeopleForUsername(user2.getUserName());
            Person[] people3 = database.personDao.getAllPeopleForUsername(user3.getUserName());

            // Make sure the appropriate number of people were generated (3 generations, 15 people, 4 generations, which is default, 31 people)
            assertTrue(people1 != null && people1.length == 31);
            assertTrue(people2 != null && people2.length == 31);
            assertTrue(people3 != null && people3.length == 31);

            // Close the database connection
            database.personDao.closeConnection(false);


            // Open the database connection
            database.eventDao.openTestConnection();

            // Get the events that should have been filled
            Event[] events1 = database.eventDao.getAllEventsForUsername(user1.getUserName());
            Event[] events2 = database.eventDao.getAllEventsForUsername(user2.getUserName());
            Event[] events3 = database.eventDao.getAllEventsForUsername(user3.getUserName());

            // Make sure the appropriate number of events were generated
            assertTrue(events1 != null && events1.length == DEFAULT_FILL_EVENTS_PER_PERSON * 31);
            assertTrue(events2 != null && events2.length == DEFAULT_FILL_EVENTS_PER_PERSON * 31);
            assertTrue(events3 != null && events3.length == DEFAULT_FILL_EVENTS_PER_PERSON * 31);

            // Close the database connection
            database.eventDao.closeConnection(false);


            // Open the database connection
            database.authTokenDao.openTestConnection();

            // Get the usernames with which the authentication tokens the register services should have created are associated with
            String usernameValid1 = database.authTokenDao.getAuthToken(registerResponseValid1.getAuthToken());
            String usernameInvalid1 = database.authTokenDao.getAuthToken(registerResponseInvalid1.getAuthToken());
            String usernameValid2 = database.authTokenDao.getAuthToken(registerResponseValid2.getAuthToken());
            String usernameValid3 = database.authTokenDao.getAuthToken(registerResponseValid3.getAuthToken());

            // These register requests should have created authentication tokens associated with the username
            assertTrue(registerRequestValid1.getUserName().equals(usernameValid1));
            assertTrue(registerRequestValid2.getUserName().equals(usernameValid2));
            assertTrue(registerRequestValid3.getUserName().equals(usernameValid3));

            // These was invalid, so it shouldn't have created an authentication token
            assertTrue(usernameInvalid1 == null);

            // Close the database connection
            database.authTokenDao.closeConnection(false);


            // Clear the database for the next test
            clearTestDatabase(database);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void clearTestDatabase(Database database) throws SQLException {
        database.userDao.openTestConnection();
        database.userDao.deleteAllUsers();
        database.userDao.closeConnection(true);
        database.personDao.openTestConnection();
        database.personDao.deleteAllPeople();
        database.personDao.closeConnection(true);
        database.eventDao.openTestConnection();
        database.eventDao.deleteAllEvents();
        database.eventDao.closeConnection(true);
        database.authTokenDao.openTestConnection();
        database.authTokenDao.deleteAllAuthTokens();
        database.authTokenDao.closeConnection(true);
    }
}
