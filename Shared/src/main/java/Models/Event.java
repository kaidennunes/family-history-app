package Models;

import com.google.gson.annotations.SerializedName;

/**
 * Model class for event data.
 * Created by Kaiden on 2/10/2018.
 */

public class Event implements Comparable<Event> {
    private static String firstEvent = "birth";
    private static String lastEvent = "death";
    /**
     * Unique identifier for this event (non-empty string)
     */
    @SerializedName("eventID")
    private String eventId;
    /**
     * User (Username) to which this person belongs
     */
    private String descendant;
    /**
     * ID of person to which this event belongs
     */
    @SerializedName("personID")
    private String person;
    /**
     * Latitude of event’s location
     */
    private double latitude;
    /**
     * Longitude of event’s location
     */
    private double longitude;
    /**
     * Country in which event occurred
     */
    private String country;
    /**
     * City in which event occurred
     */
    private String city;
    /**
     * Type of event (birth, baptism, christening, marriage, death, etc.)
     */
    private String eventType;
    /**
     * Year in which event occurred
     */
    private int year;

    /**
     * Constructor for event data.
     *
     * @param eventId    Unique identifier for this event
     * @param descendant User (Username) to which this person belongs
     * @param person     ID of person to which this event belongs
     * @param latitude   Latitude of event’s location
     * @param longitude  Longitude of event’s location
     * @param country    Country in which event occurred
     * @param city       City in which event occurred
     * @param eventType  Type of event (birth, baptism, christening, marriage, death, etc.)
     */
    public Event(String eventId, String descendant, String person, double latitude, double longitude, String country, String city, String eventType) {
        this.setEventId(eventId);
        this.setDescendant(descendant);
        this.setPerson(person);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setCountry(country);
        this.setCity(city);
        this.setEventType(eventType);
    }

    /**
     * Constructor for event data.
     *
     * @param eventId    Unique identifier for this event
     * @param descendant User (Username) to which this person belongs
     * @param person     ID of person to which this event belongs
     * @param latitude   Latitude of event’s location
     * @param longitude  Longitude of event’s location
     * @param country    Country in which event occurred
     * @param city       City in which event occurred
     * @param eventType  Type of event (birth, baptism, christening, marriage, death, etc.)
     * @param year       Year in which event occurred
     */
    public Event(String eventId, String descendant, String person, double latitude, double longitude, String country, String city, String eventType, int year) {
        this.setEventId(eventId);
        this.setDescendant(descendant);
        this.setPerson(person);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
        this.setCountry(country);
        this.setCity(city);
        this.setEventType(eventType);
        this.setYear(year);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o.getClass() != this.getClass()) {
            return false;
        }

        Event object = (Event) o;

        if (!object.getEventId().equals(this.getEventId())) {
            return false;
        }

        if (!object.getDescendant().equals(this.getDescendant())) {
            return false;
        }

        if (!object.getPerson().equals(this.getPerson())) {
            return false;
        }

        if (object.getLatitude() != this.getLatitude()) {
            return false;
        }

        if (object.getLongitude() != this.getLongitude()) {
            return false;
        }

        if (!object.getCountry().equals(this.getCountry())) {
            return false;
        }

        if (!object.getCity().equals(this.getCity())) {
            return false;
        }

        if (!object.getEventType().equals(this.getEventType())) {
            return false;
        }

        if (object.getYear() != this.getYear()) {
            return false;
        }

        return true;
    }

    @Override
    public int compareTo(Event event) {
        // Make sure the first and last events are sorted correctly (but if the event types are the same, which should never happen, move on, they need to be compared by date)
        if ((event.getEventType().toLowerCase().equals(firstEvent) && !eventType.toLowerCase().equals(firstEvent)) || (eventType.toLowerCase().equals(lastEvent) && !event.getEventType().toLowerCase().equals(lastEvent))) {
            return 1;
        } else if ((event.getEventType().toLowerCase().equals(lastEvent) && !eventType.toLowerCase().equals(lastEvent)) || (eventType.toLowerCase().equals(firstEvent) && !event.getEventType().toLowerCase().equals(firstEvent))) {
            return -1;
        }

        // See how the events compare by year
        int yearComparison = year > event.getYear() ? +1 : year < event.getYear() ? -1 : 0;

        // If they are in the same year, compare by description (event type). Otherwise, compare by the year
        if (yearComparison == 0) {
            return eventType.compareTo(event.getEventType());
        } else {
            return yearComparison;
        }
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getDescendant() {
        return descendant;
    }

    public void setDescendant(String descendant) {
        this.descendant = descendant;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
