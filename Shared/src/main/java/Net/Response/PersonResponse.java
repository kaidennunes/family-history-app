package Net.Response;

import com.google.gson.annotations.SerializedName;

import Models.Person;

/**
 * Encapsulates the response information for returning a set of person data in the database.
 * Created by Kaiden on 2/10/2018.
 */

public class PersonResponse extends Response {
    /**
     * Array of person objects in the database for the given user.
     */
    @SerializedName("data")
    private Person[] persons;

    public PersonResponse() {}

    /**
     * Constructor for the response class for returning a set of person data in the database successfully.
     *
     * @param persons Array of person objects that were found.
     */
    public PersonResponse(Person[] persons) {
        this.setPersons(persons);
    }

    /**
     * Constructor for the response class for returning a set of person data in the database, with error(s).
     *
     * @param errorMessage Non-empty string describing the error.
     */
    public PersonResponse(String errorMessage) {
        super(errorMessage);
    }

    public Person[] getPersons() {
        return persons;
    }

    public void setPersons(Person[] persons) {
        this.persons = persons;
    }
}
