package Net.Response;

import com.google.gson.annotations.SerializedName;

/**
 * Base class for all responses that are sent to the handlers.
 * Created by Kaiden on 2/13/2018.
 */

public abstract class Response {
    /**
     * Default constructor for the response class.
     */
    public Response() {
        this.setHasError(false);
    }

    /**
     * Constructor for the response class for registering with error(s).
     * @param errorMessage Non-empty string describing the error.
     */
    public Response(String errorMessage) {
        this.errorMessage = errorMessage;
        this.setHasError(true);
    }
    /**
     * Boolean value set to true if there is an error.
     */
    private Boolean hasError;
    /**
     * Non-empty string describing the error.
     */
    @SerializedName("message")
    private String errorMessage;
    /**
     * Gets errorMessage
     * @return The error message of the response.
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * Sets errorMessage
     * @param errorMessage The error message to set to the response.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Boolean value set to true if there is an error.
     * @return Boolean value of whether the response has an error.
     */
    public Boolean getHasError() {
        return hasError;
    }
    /**
     * Boolean value set to true if there is an error.
     * @param hasError Boolean value of whether the response has an error.
     */
    public void setHasError(Boolean hasError) {
        if (hasError == null) hasError = false;
        this.hasError = hasError;
    }
}
