package com.example.kaiden.familyhistoryserver.Async.Interfaces;

import android.support.v4.app.Fragment;

/**
 * Interface that allows the async tasks to call methods in their callers upon task complete.
 * Created by Kaiden on 3/22/2018.
 */

public interface IAsync {
    void displayToast(String message);
    void displayToast(int stringId);
    void onCompleteAsyncTask();
}
