package com.example.kaiden.familyhistoryserver.Async.Tasks;

import android.os.AsyncTask;

import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;
import com.example.kaiden.familyhistoryserver.Async.Interfaces.IAsync;

import java.lang.ref.WeakReference;

import Net.Requests.LoginRequest;
import Net.Response.LoginResponse;

/**
 * Created by Kaiden on 3/22/2018.
 */

public class FamilyHistoryLogin extends AsyncTask<LoginRequest, String, LoginResponse> {
    private WeakReference<IAsync> app;

    public FamilyHistoryLogin(IAsync IAsync) {
        app = new WeakReference<>(IAsync);
    }

    @Override
    protected LoginResponse doInBackground(LoginRequest... loginRequests) {
        // Set the default response for the register (in case something horrible happens)
        LoginResponse loginResponse = new LoginResponse("There was an unknown error.");

        // Get the current instance of the server proxy
        ServerProxy serverProxy = ServerProxy.getServerProxy();

        // Try to register the users
        for (int i = 0; i < loginRequests.length; i++) {
            loginResponse = serverProxy.login(loginRequests[i]);
        }

        return loginResponse;
    }

    @Override
    protected void onPostExecute(LoginResponse loginResponse) {
        // If the user wasn't successfully logged in, display an error to the user and stop trying for this request
        if (loginResponse.getHasError()) {
            app.get().displayToast(loginResponse.getErrorMessage());
            return;
        }

        // If the user was successfully logged in, get and put his family history data into the model
        ServerProxy serverProxy = ServerProxy.getServerProxy();
        new FamilyHistorySynchronizer(app.get()).execute(serverProxy);
    }
}
