package com.example.kaiden.familyhistoryserver.Async.Tasks;

import android.os.AsyncTask;

import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;
import com.example.kaiden.familyhistoryserver.Async.Interfaces.IAsync;

import java.lang.ref.WeakReference;

import Net.Requests.RegisterRequest;
import Net.Response.RegisterResponse;

/**
 * Created by Kaiden on 3/22/2018.
 */

public class FamilyHistoryRegister extends AsyncTask<RegisterRequest, String, RegisterResponse> {
    private WeakReference<IAsync> app;

    public FamilyHistoryRegister(IAsync IAsync) {
        app = new WeakReference<>(IAsync);
    }

    @Override
    protected RegisterResponse doInBackground(RegisterRequest... registerRequests) {
        // Set the default response for the register (in case something horrible happens)
        RegisterResponse registerResponse = new RegisterResponse("There was an unknown error.");

        // Get the current instance of the server proxy
        ServerProxy serverProxy = ServerProxy.getServerProxy();

        // Try to register the users
        for (int i = 0; i < registerRequests.length; i++) {
            registerResponse = serverProxy.register(registerRequests[i]);
        }

        return registerResponse;
    }

    @Override
    protected void onPostExecute(RegisterResponse registerResponse) {
        // If the user wasn't successfully registered, display an error to the user and stop trying for this request
        if (registerResponse.getHasError()) {
            app.get().displayToast(registerResponse.getErrorMessage());
            return;
        }

        // If the user was successfully registered, get and put his family history data into the model
        ServerProxy serverProxy = ServerProxy.getServerProxy();
        new FamilyHistorySynchronizer(app.get()).execute(serverProxy);

    }
}
