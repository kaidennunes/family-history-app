package com.example.kaiden.familyhistoryserver.Async.Tasks;

import android.os.AsyncTask;

import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.Async.Interfaces.IAsync;

import java.lang.ref.WeakReference;

/**
 * Created by Kaiden on 3/22/2018.
 */

public class FamilyHistorySynchronizer extends AsyncTask<ServerProxy, String, Boolean> {
    private WeakReference<IAsync> app;

    public FamilyHistorySynchronizer(IAsync IAsync) {
        app = new WeakReference<>(IAsync);
    }

    @Override
    protected Boolean doInBackground(ServerProxy... serverProxies) {
        Boolean familyHistorySynchronized = true;
        for (int i = 0; i < serverProxies.length; i++) {
            if (!serverProxies[i].synchronizeFamilyHistoryData())
                familyHistorySynchronized = false;
            if (isCancelled()) familyHistorySynchronized = false;
        }
        return familyHistorySynchronized;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        // If the model data couldn't be synchronized with the server data, tell the user that, otherwise, move to the map fragment
        if (!success) {
            app.get().displayToast(R.string.synchronization_error_message);
        } else {
            // Execute the caller's method to be executed when the async task is finished
            app.get().onCompleteAsyncTask();
        }
    }
}
