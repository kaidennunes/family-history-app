package com.example.kaiden.familyhistoryserver.Converters;

import android.content.Context;
import android.graphics.Color;

import com.example.kaiden.familyhistoryserver.R;

/**
 * Class that converts colors from integers to strings, and strings to integers.
 * Created by Kaiden on 3/24/2018.
 */

public class ColorsConverter {
    /**
     * Converts a string color to its integer value.
     *
     * @param context The context of the request.
     * @param color   The string color to be converted.
     * @return The integer value of that color.
     */
    public static int convertToColorInt(Context context, String color) {
        int convertedColor;
        if (color == null) {
            return 0;
        }
        if (color.equals(context.getResources().getString(R.string.red_color))) {
            convertedColor = Color.RED;
            System.out.println("Red found");
        } else if (color.equals(context.getResources().getString(R.string.blue_color))) {
            convertedColor = Color.BLUE;
        } else if (color.equals(context.getResources().getString(R.string.yellow_color))) {
            convertedColor = Color.YELLOW;
        } else {
            convertedColor = Color.GREEN;
        }
        return convertedColor;
    }

    /**
     * Converts a integer color to its string value.
     *
     * @param context The context of the request.
     * @param color   The integer color to be converted.
     * @return The string value of that color.
     */
    public static String convertToColorString(Context context, int color) {
        String convertedColor;
        if (Color.RED == color) {
            convertedColor = context.getResources().getString(R.string.red_color);
        } else if (Color.BLUE == color) {
            convertedColor = context.getResources().getString(R.string.blue_color);
        } else if (Color.YELLOW == color) {
            convertedColor = context.getResources().getString(R.string.yellow_color);
        } else {
            convertedColor = context.getResources().getString(R.string.green_color);
        }
        return convertedColor;
    }
}
