package com.example.kaiden.familyhistoryserver.Converters;

import android.content.Context;

import com.example.kaiden.familyhistoryserver.R;
import com.google.android.gms.maps.GoogleMap;

/**
 * Class that converts map types from integers to strings, and strings to integers.
 * Created by Kaiden on 3/23/2018.
 */

public class MapConverter {
    /**
     * Converts a map type to its integer value.
     *
     * @param context The context of the request.
     * @param mapType The string map type to be converted.
     * @return The integer value of that map type.
     */
    public static int convertToMapTypeInt(Context context, String mapType) {
        int convertedMapType;
        if (mapType == null) {
            convertedMapType = GoogleMap.MAP_TYPE_NORMAL;
        } else if (mapType == context.getResources().getString(R.string.map_type_hybrid)) {
            convertedMapType = GoogleMap.MAP_TYPE_HYBRID;
        } else if (mapType == context.getResources().getString(R.string.map_type_satellite)) {
            convertedMapType = GoogleMap.MAP_TYPE_SATELLITE;
        } else if (mapType == context.getResources().getString(R.string.map_type_terrain)) {
            convertedMapType = GoogleMap.MAP_TYPE_TERRAIN;
        } else {
            convertedMapType = GoogleMap.MAP_TYPE_NORMAL;
        }
        return convertedMapType;
    }

    /**
     * Converts a map type to its string value.
     *
     * @param context The context of the request.
     * @param mapType The integer map type to be converted.
     * @return The string value of that map type.
     */
    public static String convertToMapTypeString(Context context, int mapType) {
        String convertedMapType;
        if (mapType == GoogleMap.MAP_TYPE_NORMAL) {
            convertedMapType = context.getResources().getString(R.string.map_type_normal);
        } else if (mapType == GoogleMap.MAP_TYPE_HYBRID) {
            convertedMapType = context.getResources().getString(R.string.map_type_hybrid);
        } else if (mapType == GoogleMap.MAP_TYPE_SATELLITE) {
            convertedMapType = context.getResources().getString(R.string.map_type_satellite);
        } else {
            convertedMapType = context.getResources().getString(R.string.map_type_terrain);
        }
        return convertedMapType;
    }
}
