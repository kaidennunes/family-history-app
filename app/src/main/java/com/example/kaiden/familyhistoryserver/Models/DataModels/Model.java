package com.example.kaiden.familyhistoryserver.Models.DataModels;

import com.example.kaiden.familyhistoryserver.Models.FilterModels.Filters;
import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResult;
import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResultEvent;
import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResultPerson;
import com.example.kaiden.familyhistoryserver.Models.SettingsModels.Settings;
import com.example.kaiden.familyhistoryserver.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import Models.Event;
import Models.Person;

/**
 * This model class holds all the information that needs to be used or displayed to the user
 * Created by Kaiden on 3/14/2018.
 */

public class Model {
    private static Model _instance = new Model();
    private final float MAX_COLOR_VALUE = 359f;

    private Person user;
    private Map<String, Person> people;
    private Map<String, List<Event>> events;
    private Map<String, List<Person>> childrenPeople;
    private List<String> fatherSide;
    private List<String> motherSide;
    private Set<String> eventTypes;
    private Settings settings;
    private Filters filters;

    public static Model getModel() {
        return _instance;
    }

    /**
     * Loads the events, people and user into the model, as well as calculates the relationships between people.
     *
     * @param user          The user as a Person object.
     * @param peopleEvents  An array of all the events in the user's family history tree.
     * @param peopleHistory An array of all the people in the user's family history tree.
     */
    public void loadData(Person user, Event[] peopleEvents, Person[] peopleHistory) {
        // Reset the model's data
        resetModelData();

        // Set the user to be the new user
        setUser(user);

        // Add the people and events to the appropriate maps
        setEvents(peopleEvents);
        setPeople(peopleHistory);
        setChildren();
        calculateFamilyTreeSides();

        // Add any extra event type filters and keep the old ones
        filters.setAllEventTypeFilters(eventTypes);
    }

    /**
     * Converts the array of events to a map of events with the event ID as the key.
     *
     * @param peopleEvents Array of events to convert into a map.
     */
    private void setEvents(Event[] peopleEvents) {
        if (peopleEvents == null) return;
        // Add the events to the map of events
        for (int i = 0; i < peopleEvents.length; i++) {
            // Get any existing events for this person to whom the event belongs
            List<Event> personEvents = events.get(peopleEvents[i].getPerson());

            // If there are no events yet for this person, create a empty list of events
            if (personEvents == null) {
                personEvents = new ArrayList<Event>();
            }

            // Add the new event to the list of events for this person
            personEvents.add(peopleEvents[i]);

            // Update the map with the list of events for that person
            events.put(peopleEvents[i].getPerson(), personEvents);
        }

        // Store all the possible event types
        eventTypes = getAllEventTypes();
    }

    /**
     * Converts the array of people to a map of people with the person ID as the key.
     *
     * @param peopleHistory Array of people to convert into a map.
     */
    private void setPeople(Person[] peopleHistory) {
        if (peopleHistory == null) return;

        // Add the people to the map of people
        for (int i = 0; i < peopleHistory.length; i++) {
            people.put(peopleHistory[i].getPersonId(), peopleHistory[i]);
        }
    }

    /**
     * Calculates which people belong to the father's side and which belongs to the mother's side.
     */
    private void calculateFamilyTreeSides() {
        // Recursively add all people from the mother's side to the list of mother side people
        addMotherSide(people.get(user.getMother()));

        // Recursively add all people from the father's side to the list of father side people
        addFatherSide(people.get(user.getFather()));
    }

    /**
     * Recursive helper method that adds a person and all of the person's ancestors to the mother's side.
     *
     * @param person Person to be added to the mother's side, along with their ancestors.
     */
    private void addMotherSide(Person person) {
        // If the person doesn't exist, don't add them
        if (person == null) {
            return;
        }

        // Add the person to the mother side
        addToMotherSide(person.getPersonId());

        // Recursively add all people from the mother's side to the list of overall mother side people
        addMotherSide(people.get(person.getMother()));

        // Recursively add all people from the father's side to the list of overall mother side people
        addMotherSide(people.get(person.getFather()));
    }

    /**
     * Recursive helper method that adds a person and all of the person's ancestors to the father's side.
     *
     * @param person Person to be added to the father's side, along with their ancestors.
     */
    private void addFatherSide(Person person) {
        // If the person doesn't exist, don't add them
        if (person == null) {
            return;
        }

        // Add the person to the father side
        addToFatherSide(person.getPersonId());

        // Recursively add all people from the mother's side to the list of overall father side people
        addFatherSide(people.get(person.getMother()));

        // Recursively add all people from the father's side to the list of overall father side people
        addFatherSide(people.get(person.getFather()));
    }

    /**
     * Helper method that adds a person to the list of people on the mother's side.
     *
     * @param personId The person to be added to the mother's side.
     */
    private void addToMotherSide(String personId) {
        if (personId != null) {
            motherSide.add(personId);
        }
    }

    /**
     * Helper method that adds a person to the list of people on the father's side.
     *
     * @param personId The person to be added to the father's side.
     */
    private void addToFatherSide(String personId) {
        if (personId != null) {
            fatherSide.add(personId);
        }
    }

    /**
     * Calculates the relationship of parent to children for all people in the map of people.
     */
    private void setChildren() {
        for (Map.Entry<String, Person> personEntry : people.entrySet()) {
            // Get the person in the map entry
            Person person = personEntry.getValue();

            // If he has a valid father, add the child to the father's list of children
            if (person.getFather() != null) {
                List<Person> childrenOfFather = childrenPeople.get(person.getFather());

                // If this person has no children added yet, create an empty list of children
                if (childrenOfFather == null) {
                    childrenOfFather = new ArrayList<>();
                }

                // Add the person to the list of children of the father
                childrenOfFather.add(person);

                // Add the list of children to the map of children
                childrenPeople.put(person.getFather(), childrenOfFather);
            }

            // If he has a valid mother, add the child to the mother's list of children
            if (person.getMother() != null) {
                List<Person> childrenOfMother = childrenPeople.get(person.getMother());

                // If this person has no children added yet, create an empty list of children
                if (childrenOfMother == null) {
                    childrenOfMother = new ArrayList<>();
                }

                // Add the person to the list of children of the mother
                childrenOfMother.add(person);

                // Add the list of children to the map of children
                childrenPeople.put(person.getMother(), childrenOfMother);
            }
        }
    }

    /**
     * Gets all the event types that are found in the map of events.
     *
     * @return Returns a set (all unique values) of every event type found in the model.
     */
    private Set<String> getAllEventTypes() {
        // Initialize the list of event types
        Set<String> eventTypes = new HashSet<>();

        // For every event in the model, add it's event type to the set of event types
        for (List<Event> listOfEvents : events.values()) {
            for (Event event : listOfEvents) {
                eventTypes.add(event.getEventType());
            }
        }
        return eventTypes;
    }

    /**
     * Resets all of the model's data, including settings and filters.
     */
    public void resetAllModelData() {
        resetModelData();
        settings = new Settings();
        filters = new Filters(eventTypes);
    }

    /**
     * Resets the model's data, not including settings and filters.
     */
    private void resetModelData() {
        setUser(null);
        people = new HashMap<>();
        events = new HashMap<>();
        fatherSide = new ArrayList<>();
        motherSide = new ArrayList<>();
        childrenPeople = new HashMap<>();
        eventTypes = new HashSet<>();
    }

    private Model() {
        resetAllModelData();
    }

    /**
     * Determines whether an event should be shown based on the current filters.
     *
     * @param event The event that may or may not be shown.
     * @return Returns true if the event should be shown, false otherwise.
     */
    private boolean showEvent(Event event) {
        String personId = event.getPerson();

        // If the father' side should not show and this is the father's side, then this event should not be shown
        if (!filters.showFathersSide() && fatherSide.contains(personId)) {
            return false;
        }

        // If the mothers' side should not show and this is the mother's side, then this event should not be shown
        if (!filters.showMothersSide() && motherSide.contains(personId)) {
            return false;
        }

        // Get the gender of the person associated with the event
        char genderOfPerson = Character.toLowerCase(people.get(personId).getGender());

        // If the male events should not show and this is a male event, then this event should not be shown
        if (!filters.showMaleEvents() && genderOfPerson == 'm') {
            return false;
        }

        // If the female events should not show and this is a female event, then this event should not be shown
        if (!filters.showFemaleEvents() && genderOfPerson == 'f') {
            return false;
        }

        // If the event type of this event should not be shown, then this event should not be shown
        if (!filters.showEventType(event.getEventType())) {
            return false;
        }

        return true;
    }

    /**
     * From the gender, returns an int that represents the appropriate drawable for that gender.
     *
     * @param gender Char that represents the gender.
     * @return Returns an int that represents the appropriate drawable for that gender.
     */
    private int getDrawableFromGender(char gender) {
        if (Character.toLowerCase(gender) == 'm') {
            return R.drawable.ic_men;
        } else {
            return R.drawable.ic_female;
        }
    }

    /**
     * Searches through the model data to return back all data that matches the given query.
     *
     * @param query The search query as a String.
     * @return Returns a list of SearchResult objects that match the search query.
     */
    public List<SearchResult> searchModelData(String query) {
        // Initialize the search result list
        List<SearchResult> searchResults = new LinkedList<>();

        // If there is no query, then return the empty list
        if (query == null || query.length() == 0) {
            return searchResults;
        }

        // Go through all the people and see if any match the query
        for (Map.Entry<String, Person> entry : people.entrySet()) {
            // Get the person
            Person person = entry.getValue();

            // Get the person's full name
            String fullName = person.getFirstName() + " " + person.getLastName();

            // If someone matches, encapsulate the data in a search result and add the event to the list of search results
            if (fullName.toLowerCase().contains(query.toLowerCase())) {
                searchResults.add(new SearchResultPerson(fullName, getDrawableFromGender(person.getGender()), person.getPersonId()));
            }
        }

        // Go through all the events and see if any match the query
        for (Event event : getAllFilteredEvents()) {
            // If the event's country, city, description (event type), or year matches the query, encapsulate the data in a search result
            if (event.getCountry().toLowerCase().contains(query.toLowerCase())
                    || event.getCountry().toLowerCase().contains(query.toLowerCase())
                    || event.getCity().toLowerCase().contains(query.toLowerCase())
                    || event.getEventType().toLowerCase().contains(query.toLowerCase())
                    || Integer.toString(event.getYear()).toLowerCase().contains(query.toLowerCase())) {
                // Get the person with whom the event is associated with
                Person person = people.get(event.getPerson());

                // Get the event data to be displayed
                StringBuilder eventInfo = new StringBuilder();
                eventInfo.append(event.getEventType());
                eventInfo.append(": ");
                eventInfo.append(event.getCity());
                eventInfo.append(", ");
                eventInfo.append(event.getCountry());
                eventInfo.append(" (");
                eventInfo.append(event.getYear());
                eventInfo.append(")\n");
                eventInfo.append(person.getFirstName());
                eventInfo.append(" ");
                eventInfo.append(person.getLastName());

                // Add the event to the list of search results
                searchResults.add(new SearchResultEvent(eventInfo.toString(), R.drawable.ic_room_black_24dp, event.getEventId()));
            }
        }
        return searchResults;
    }

    public Map<String, Boolean> getEventTypeFilters() {
        return filters.getEventTypeFilters();
    }

    /**
     * Sets whether an event type should be shown or not.
     *
     * @param eventType The event type which should be shown or not.
     * @param showEvent True if the event type should be shown, false otherwise.
     */
    public void setEventTypeFilter(String eventType, Boolean showEvent) {
        filters.setEventTypeFilter(eventType, showEvent);
    }

    /**
     * Sets the filter on the father's side.
     *
     * @param showFathersSide True if the father's side events should be shown, false otherwise
     */
    public void setFathersSideFilter(boolean showFathersSide) {
        filters.setFathersSide(showFathersSide);
    }

    /**
     * Sets the filter on the mother's side.
     *
     * @param showMothersSide True if the mother's side events should be shown, false otherwise
     */
    public void setMothersSideFilter(boolean showMothersSide) {
        filters.setMothersSide(showMothersSide);
    }

    /**
     * Sets the filter on the male events.
     *
     * @param showMaleEvents True if the male events events should be shown, false otherwise
     */
    public void setMaleEventsFilter(boolean showMaleEvents) {
        filters.setMaleFilter(showMaleEvents);
    }

    /**
     * Sets the filter on the female events.
     *
     * @param showFemaleEvents True if the female events events should be shown, false otherwise
     */
    public void setFemaleEventsFilter(boolean showFemaleEvents) {
        filters.setFemaleFilter(showFemaleEvents);
    }

    /**
     * Determines whether the father's side events should be shown or not.
     *
     * @return True if the father side events should be show, false otherwise.
     */
    public boolean showFathersSide() {
        return filters.showFathersSide();
    }

    /**
     * Determines whether the mother's side events should be shown or not.
     *
     * @return True if the mother side events should be show, false otherwise.
     */
    public boolean showMothersSide() {
        return filters.showMothersSide();
    }

    /**
     * Determines whether the male events should be shown or not.
     *
     * @return True if the male events should be show, false otherwise.
     */
    public boolean showMaleEvents() {
        return filters.showMaleEvents();
    }

    /**
     * Determines whether the female events should be shown or not.
     *
     * @return True if the female events should be show, false otherwise.
     */
    public boolean showFemaleEvents() {
        return filters.showFemaleEvents();
    }

    /**
     * Sets the color for the life story lines.
     *
     * @param lifeStoryLinesColor The int value that represents a color.
     */
    public void setLifeStoryLinesColor(int lifeStoryLinesColor) {
        settings.setLifeStoryLinesColor(lifeStoryLinesColor);
    }

    /**
     * Sets whether the life story lines should be drawn or not.
     *
     * @param lifeStoryLines True if the life story lines should be shown, false otherwise
     */
    public void setLifeStoryLines(boolean lifeStoryLines) {
        settings.setLifeStoryLinesOn(lifeStoryLines);
    }

    /**
     * Sets the color for the family tree lines.
     *
     * @param familyTreeLinesColor The int value that represents a color.
     */
    public void setFamilyTreeLinesColor(int familyTreeLinesColor) {
        settings.setFamilyTreeLinesColor(familyTreeLinesColor);
    }

    /**
     * Sets whether the family tree lines should be drawn or not.
     *
     * @param familyTreeLines True if the family tree lines should be shown, false otherwise
     */
    public void setFamilyTreeLines(boolean familyTreeLines) {
        settings.setFamilyTreeLinesOn(familyTreeLines);
    }

    /**
     * Sets the color for the spouse lines.
     *
     * @param spouseLinesColor The int value that represents a color.
     */
    public void setSpouseLinesColor(int spouseLinesColor) {
        settings.setSpouseLinesColor(spouseLinesColor);
    }

    /**
     * Sets whether the spouse lines should be drawn or not.
     *
     * @param spouseLines True if the spouse lines should be shown, false otherwise
     */
    public void setSpouseLines(boolean spouseLines) {
        settings.setSpouseLinesOn(spouseLines);
    }

    /**
     * Sets the map type for the google map.
     *
     * @param mapType The int value that represents the type of map to be displayed.
     */
    public void setMapType(int mapType) {
        settings.setMapType(mapType);
    }

    /**
     * Gets the first event from the list of filtered events of the spouse of the person associated with the event in the parameter.
     *
     * @param event The event of a person.
     * @return Returns null if the person has no spouse or if the spouse has no unfiltered events. Otherwise, returns the first ordered event from the spouse's list of events.
     */
    public Event getBestSpouseEvent(Event event) {
        Event bestSpouseEvent = null;
        // Find the person associated with the event
        Person person = people.get(event.getPerson());
        if (person.getSpouse() != null && !person.getSpouse().equals("")) {
            // Get the ordered and filtered list of events for the spouse
            List<Event> spouseEvents = getOrderedFilteredPersonEvents(person.getSpouse());

            // If the list is not null, then the first event is the one that is the best spouse event
            if (spouseEvents != null) {
                return spouseEvents.get(0);
            }
        }

        return bestSpouseEvent;
    }

    public int getFamilyTreeLineColor() {
        return settings.getFamilyTreeLinesColor();
    }

    public int getLifeStoryLineColor() {
        return settings.getLifeStoryLinesColor();
    }

    public int getSpouseLineColor() {
        return settings.getSpouseLinesColor();
    }

    public int getGoogleMapDisplayType() {
        return settings.getMapType();
    }

    public boolean showSpouseLines() {
        return settings.isSpouseLinesOn();
    }

    public boolean showFamilyTreeLines() {
        return settings.isFamilyTreeLinesOn();
    }

    public boolean showLifeStoryLines() {
        return settings.isLifeStoryLinesOn();
    }

    public List<Event> getAllFilteredEvents() {
        List<Event> filteredEvents = new ArrayList<>();
        for (Map.Entry<String, List<Event>> personEntry : events.entrySet()) {
            for (Event event : personEntry.getValue()) {
                if (showEvent(event)) {
                    filteredEvents.add(event);
                }
            }
        }
        return filteredEvents;
    }

    public float getEventColor(Event event) {
        // Get the event's type
        String eventType = event.getEventType();

        // Go through the all of the possible event types to find the one that matches the event
        int position = 1;
        for (String currentEventType : eventTypes) {
            // If the event matches, then return a float (which will be used as a color)
            if (eventType.toLowerCase().equals(currentEventType.toLowerCase())) {
                float hue = MAX_COLOR_VALUE / position;
                return hue;
            }
            position++;
        }

        // If somehow no color has been returned yet, return a default color
        return BitmapDescriptorFactory.HUE_AZURE;
    }

    public List<Event> getOrderedFilteredPersonEvents(String personId) {
        // Get the list of all of the events for the person
        List<Event> orderedFilteredPersonEvents = new LinkedList<>(events.get(personId));

        // Filter the events based on the current filter settings
        for (Iterator<Event> iter = orderedFilteredPersonEvents.listIterator(); iter.hasNext(); ) {
            Event event = iter.next();
            if (!showEvent(event)) {
                iter.remove();
            }
        }

        // Sort the list
        Collections.sort(orderedFilteredPersonEvents);

        // Return the sorted and filtered list
        return orderedFilteredPersonEvents;
    }

    /**
     * Returns the children of a person.
     *
     * @param personId The ID of the person whose children are to be found
     * @return A list of Person objects, which are the children of the person. Returns null if there are no children.
     */
    public List<Person> getChildren(String personId) {
        return childrenPeople.get(personId);
    }

    public Person getSpouse(String personId) {
        return people.get(people.get(personId).getSpouse());
    }

    public Person getMother(String personId) {
        return people.get(people.get(personId).getFather());
    }

    public Person getFather(String personId) {
        return people.get(people.get(personId).getMother());
    }

    /**
     * Finds an event by event ID.
     *
     * @param eventId The ID of the event to be found.
     * @return Returns null if no event has the given ID, otherwise, returns the found event.
     */
    public Event findEvent(String eventId) {
        for (Map.Entry<String, List<Event>> personEntry : Model.getModel().getEvents().entrySet()) {
            for (Event personEvent : personEntry.getValue()) {
                if (personEvent.getEventId().equals(eventId)) {
                    return personEvent;
                }
            }
        }
        return null;
    }

    public Person findPerson(String personId) {
        return people.get(personId);
    }

    public Person getUser() {
        return user;
    }

    public void setUser(Person user) {
        this.user = user;
    }

    public Map<String, Person> getPeople() {
        return people;
    }

    public Map<String, List<Event>> getEvents() {
        return events;
    }
}
