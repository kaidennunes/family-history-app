package com.example.kaiden.familyhistoryserver.Models.FilterModels;

/**
 * Class that encapsulates the event type and whether to filter that event type.
 * Created by Kaiden on 4/12/2018.
 */

public class EventTypeFilter implements Comparable<EventTypeFilter> {
    private String eventType;
    private boolean filterEvent;

    public EventTypeFilter(String eventType, boolean filterEvent) {
        this.eventType = eventType;
        this.filterEvent = filterEvent;
    }

    public String getEventType() {
        return eventType;
    }

    public boolean isFilterEvent() {
        return filterEvent;
    }

    @Override
    public int compareTo(EventTypeFilter eventTypeFilter) {
        return this.getEventType().compareTo(eventTypeFilter.getEventType());
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (object.getClass() != this.getClass()) {
            return false;
        }

        EventTypeFilter obj = (EventTypeFilter) object;
        if (!this.getEventType().equals(obj.getEventType()) || this.isFilterEvent() != obj.isFilterEvent()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return eventType.hashCode() * ((Boolean) filterEvent).hashCode();
    }

}
