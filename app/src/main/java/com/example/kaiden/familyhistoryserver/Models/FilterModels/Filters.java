package com.example.kaiden.familyhistoryserver.Models.FilterModels;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Class that holds the filter data for the events.
 * Casing of event type is irrelevant.
 * Created by Kaiden on 3/14/2018.
 */

public class Filters {
    private Map<String, Boolean> eventTypeFilters;
    private boolean maleFilter;
    private boolean femaleFilter;
    private boolean fathersSide;
    private boolean mothersSide;

    public Filters(Set<String> eventTypes) {
        // Initialize the map of event type filters
        eventTypeFilters = new HashMap<>();

        // Set the default values for all of the event type filters
        setAllEventTypeFilters(eventTypes);

        // Set the default values for all of the other filters
        setMaleFilter(true);
        setFemaleFilter(true);
        setFathersSide(true);
        setMothersSide(true);
    }

    /**
     * This method adds all of the event types to the map of event type filters. Any existing filter (duplicate) is left with its original value.
     * <p>
     * Any filters currently in the map that do not exist in the set of event types are deleted.
     * </p>
     *
     * @param eventTypes A set of all of the event types to filter for.
     */
    public void setAllEventTypeFilters(Set<String> eventTypes) {
        // Initialize the new map of event type filters
        Map<String, Boolean> newEventTypeFilters = new HashMap<>();

        // Go through every event type in the set
        for (String eventType : eventTypes) {
            // Make the event type lower case, so casing doesn't matter
            String eventTypeLowerCase = eventType.toLowerCase();

            // Get the current filter value of the event type
            Boolean currentFilterValue = eventTypeFilters.get(eventTypeLowerCase);

            // If there is no current value, add a default true value. Otherwise, add the current filter value to the new map of event type filters
            if (currentFilterValue == null) {
                newEventTypeFilters.put(eventTypeLowerCase, true);
            } else {
                newEventTypeFilters.put(eventTypeLowerCase, currentFilterValue);
            }
        }

        // Replace the old map of event type filters with the new map
        eventTypeFilters = newEventTypeFilters;
    }

    /**
     * Determines whether an event type should be shown or not.
     *
     * @param eventType The event type.
     * @return True if the event type should be shown, false otherwise.
     */
    public Boolean showEventType(String eventType) {
        Boolean showEvent = eventTypeFilters.get(eventType.toLowerCase());
        if (showEvent == null) {
            return false;
        }
        return showEvent;
    }

    public Map<String, Boolean> getEventTypeFilters() {
        return eventTypeFilters;
    }

    public void setEventTypeFilter(String eventType, Boolean showEvent) {
        eventTypeFilters.put(eventType.toLowerCase(), showEvent);
    }

    public boolean showMaleEvents() {
        return maleFilter;
    }

    public void setMaleFilter(boolean maleFilter) {
        this.maleFilter = maleFilter;
    }

    public boolean showFemaleEvents() {
        return femaleFilter;
    }

    public void setFemaleFilter(boolean femaleFilter) {
        this.femaleFilter = femaleFilter;
    }

    public boolean showFathersSide() {
        return fathersSide;
    }

    public void setFathersSide(boolean fathersSide) {
        this.fathersSide = fathersSide;
    }

    public boolean showMothersSide() {
        return mothersSide;
    }

    public void setMothersSide(boolean mothersSide) {
        this.mothersSide = mothersSide;
    }
}
