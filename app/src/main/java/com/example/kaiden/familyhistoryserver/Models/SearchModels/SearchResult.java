package com.example.kaiden.familyhistoryserver.Models.SearchModels;

import android.content.Context;

/**
 * Class that encapsulates the display data for the search results, as well as acts as an interface so that search results can launch different activities.
 * Created by Kaiden on 4/12/2018.
 */

public abstract class SearchResult {
    private String displayText;
    private int drawable;
    private String searchId;

    public SearchResult(String displayText, int drawable, String searchId) {
        this.displayText = displayText;
        this.drawable = drawable;
        this.searchId = searchId;
    }

    public abstract void launchActivity(Context context);

    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }
}
