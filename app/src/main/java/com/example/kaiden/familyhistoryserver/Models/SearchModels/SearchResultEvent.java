package com.example.kaiden.familyhistoryserver.Models.SearchModels;

import android.content.Context;
import android.content.Intent;

import com.example.kaiden.familyhistoryserver.UI.Activities.EventActivity;

/**
 * Implementation of the SearchResult semi-abstract class, using events.
 * Created by Kaiden on 4/12/2018.
 */

public class SearchResultEvent extends SearchResult {
    public SearchResultEvent(String displayText, int drawable, String searchId) {
        super(displayText, drawable, searchId);
    }

    @Override
    public void launchActivity(Context context) {
        Intent intent = EventActivity.newIntent(context, this.getSearchId());
        context.startActivity(intent);
    }
}
