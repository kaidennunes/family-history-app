package com.example.kaiden.familyhistoryserver.Models.SearchModels;

import android.content.Context;
import android.content.Intent;

import com.example.kaiden.familyhistoryserver.UI.Activities.PersonActivity;

/**
 * Implementation of the SearchResult semi-abstract class, using person objects.
 * Created by Kaiden on 4/12/2018.
 */

public class SearchResultPerson extends SearchResult {
    public SearchResultPerson(String displayText, int drawable, String searchId) {
        super(displayText, drawable, searchId);
    }

    @Override
    public void launchActivity(Context context) {
        Intent intent = PersonActivity.newIntent(context, this.getSearchId());
        context.startActivity(intent);
    }
}
