package com.example.kaiden.familyhistoryserver.Models.SettingsModels;


import android.graphics.Color;

import com.google.android.gms.maps.GoogleMap;

/**
 * Class that holds the settings for the application.
 * Created by Kaiden on 3/14/2018.
 */

public class Settings {
    private int mapType;
    private boolean lifeStoryLinesOn;
    private boolean familyTreeLinesOn;
    private boolean spouseLinesOn;

    private int lifeStoryLinesColor;
    private int familyTreeLinesColor;
    private int spouseLinesColor;

    public Settings() {
        // Set the default settings for the application
        setMapType(GoogleMap.MAP_TYPE_NORMAL);
        setLifeStoryLinesOn(true);
        setFamilyTreeLinesOn(true);
        setSpouseLinesOn(true);

        setLifeStoryLinesColor(Color.BLUE);
        setFamilyTreeLinesColor(Color.GREEN);
        setSpouseLinesColor(Color.YELLOW);

    }

    public int getMapType() {
        return mapType;
    }

    public void setMapType(int mapType) {
        this.mapType = mapType;
    }

    public boolean isLifeStoryLinesOn() {
        return lifeStoryLinesOn;
    }

    public void setLifeStoryLinesOn(boolean lifeStoryLinesOn) {
        this.lifeStoryLinesOn = lifeStoryLinesOn;
    }

    public boolean isFamilyTreeLinesOn() {
        return familyTreeLinesOn;
    }

    public void setFamilyTreeLinesOn(boolean familyTreeLinesOn) {
        this.familyTreeLinesOn = familyTreeLinesOn;
    }

    public boolean isSpouseLinesOn() {
        return spouseLinesOn;
    }

    public void setSpouseLinesOn(boolean spouseLinesOn) {
        this.spouseLinesOn = spouseLinesOn;
    }

    public int getLifeStoryLinesColor() {
        return lifeStoryLinesColor;
    }

    public void setLifeStoryLinesColor(int lifeStoryLinesColor) {
        this.lifeStoryLinesColor = lifeStoryLinesColor;
    }

    public int getFamilyTreeLinesColor() {
        return familyTreeLinesColor;
    }

    public void setFamilyTreeLinesColor(int familyTreeLinesColor) {
        this.familyTreeLinesColor = familyTreeLinesColor;
    }

    public int getSpouseLinesColor() {
        return spouseLinesColor;
    }

    public void setSpouseLinesColor(int spouseLinesColor) {
        this.spouseLinesColor = spouseLinesColor;
    }
}
