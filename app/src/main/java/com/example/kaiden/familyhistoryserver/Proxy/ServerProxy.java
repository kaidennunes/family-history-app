package com.example.kaiden.familyhistoryserver.Proxy;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;

import Models.Event;
import Models.Person;
import Net.Requests.*;
import Net.Response.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static Json.JsonConverter.*;

/**
 * This class interacts with the server to login and register users, as well as to synchronize the model with their family history information.
 * Created by Kaiden on 3/14/2018.
 */

public class ServerProxy {
    private static ServerProxy _instance = new ServerProxy();

    private String serverHost;
    private String serverPort;
    private String authToken;

    public static ServerProxy getServerProxy() {
        return _instance;
    }

    /**
     * Sets the server host and server port that the proxy will attempt to connect to.
     *
     * @param serverHost The server host.
     * @param serverPort The server port.
     */
    public static void setServerProxy(String serverHost, String serverPort) {
        _instance.serverHost = serverHost;
        _instance.serverPort = serverPort;
        _instance.authToken = null;
    }

    private ServerProxy() {
        serverHost = null;
        serverPort = null;
        authToken = null;
    }

    /**
     * Attempts to login a user. Sets the authentication token if successful.
     *
     * @param loginRequest The encapsulated login request data.
     * @return The encapsulated login response data.
     */
    public LoginResponse login(LoginRequest loginRequest) {
        LoginResponse loginResponse = new LoginResponse("There was an unknown error.");
        try {
            // Create a URL indicating where the server is running, and which operation we want
            URL url = new URL("http://" + serverHost + ":" + serverPort + "/user/login");

            // Start constructing our HTTP request
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            http.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            http.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            http.addRequestProperty("Accept", "application/json");

            // Add an auth token to the request in the HTTP "Authorization" header
            http.addRequestProperty("Authorization", loginRequest.getUserPassword());

            // Connect to the server and send the HTTP request
            http.connect();

            // Turn the LoginRequest into a JSON string we will send in the HTTP request body
            String jsonReqData = convertObjectToJson(loginRequest);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = http.getOutputStream();

            // Write the JSON data to the request body
            writeString(jsonReqData, reqBody);

            // Close the request body output stream, indicating that the request is complete
            reqBody.close();

            // Get the input stream containing the HTTP response body
            InputStream respBody;
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                respBody = http.getInputStream();
            } else {
                respBody = http.getErrorStream();
            }

            // Extract JSON data from the HTTP response body
            String respData = readString(respBody);

            // Turn the JSON data into a LoginResponse object
            loginResponse = (LoginResponse) convertJsonToObject(respData, loginResponse);

            // Check to make sure that the HTTP response from the server contains a 200 status code, which means "success".  Treat anything else as a failure.
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // Set the authToken
                authToken = loginResponse.getAuthToken();
            } else {
                loginResponse.setHasError(true);
                System.out.println("ERROR: " + http.getResponseMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return loginResponse;
    }

    /**
     * Attempts to register a user. Sets the authentication token if successful.
     *
     * @param registerRequest The encapsulated register request data.
     * @return The encapsulated register response data.
     */
    public RegisterResponse register(RegisterRequest registerRequest) {
        RegisterResponse registerResponse = new RegisterResponse("There was an unknown error");
        try {
            // Create a URL indicating where the server is running, and which operation we want
            URL url = new URL("http://" + serverHost + ":" + serverPort + "/user/register");

            // Start constructing our HTTP request
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            // Specify that we are sending an HTTP POST request
            http.setRequestMethod("POST");

            // Indicate that this request will contain an HTTP request body
            http.setDoOutput(true);

            // Specify that we would like to receive the server's response in JSON
            http.addRequestProperty("Accept", "application/json");

            // Connect to the server and send the HTTP request
            http.connect();

            // Turn the RegisterRequest into a JSON string we will send in the HTTP request body
            String jsonReqData = convertObjectToJson(registerRequest);

            // Get the output stream containing the HTTP request body
            OutputStream reqBody = http.getOutputStream();

            // Write the JSON data to the request body
            writeString(jsonReqData, reqBody);

            // Close the request body output stream, indicating that the request is complete
            reqBody.close();

            // Get the input stream containing the HTTP response body
            InputStream respBody;
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                respBody = http.getInputStream();
            } else {
                respBody = http.getErrorStream();
            }

            // Extract JSON data from the HTTP response body
            String respData = readString(respBody);

            // Turn the JSON data into a RegisterResponse object
            registerResponse = (RegisterResponse) convertJsonToObject(respData, registerResponse);

            // Check to make sure that the HTTP response from the server contains a 200 status code, which means "success".  Treat anything else as a failure.
            if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
                // Set the authToken
                authToken = registerResponse.getAuthToken();
            } else {
                registerResponse.setHasError(true);
                System.out.println("ERROR: " + http.getResponseMessage());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return registerResponse;
    }

    /**
     * Synchronizes the family history data in the model with the data in the server.
     *
     * @return Returns true if the synchronization was successful, false otherwise.
     */
    public boolean synchronizeFamilyHistoryData() {
        try {
            // Retrieve the family history data from the server
            PersonResponse personResponse = getPeople();
            EventResponse eventResponse = getEvents();

            // If any of the responses is null for any reason, there was an error
            if (personResponse == null || eventResponse == null) {
                return false;
            }

            // Put the data into arrays
            Person[] people = personResponse.getPersons();
            Event[] events = eventResponse.getEvents();

            // Get the model
            Model model = Model.getModel();

            // Load the data into the model (the first person is always the user)
            model.loadData(people[0], events, people);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Makes a request to the server to get the people data for the user, based on the authentication token.
     *
     * @return The encapsulated data holding all the people data for the user.
     * @throws Exception Throws an exception if the request fails for any reason.
     */
    private PersonResponse getPeople() throws Exception {
        // Create a URL indicating where the server is running, and which web API operation we want to call
        URL url = new URL("http://" + serverHost + ":" + serverPort + "/person");

        // Start constructing our HTTP request
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        // Specify that we are sending an HTTP GET request
        http.setRequestMethod("GET");

        // Indicate that this request will not contain an HTTP request body
        http.setDoOutput(false);

        // Add an auth token to the request in the HTTP "Authorization" header
        http.addRequestProperty("Authorization", authToken);

        // Specify that we would like to receive the server's response in JSON
        http.addRequestProperty("Accept", "application/json");

        // Connect to the server and send the HTTP request
        http.connect();

        // Check to make sure that the HTTP response from the server contains a 200 status code, which means "success".  Treat anything else as a failure.
        if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
            // Get the input stream containing the HTTP response body
            InputStream respBody = http.getInputStream();

            // Extract JSON data from the HTTP response body
            String respData = readString(respBody);

            // Create an empty PersonResponse to let the json converter use it
            PersonResponse personResponse = new PersonResponse("");

            // Turn the JSON data into a PersonResponse object
            personResponse = (PersonResponse) convertJsonToObject(respData, personResponse);

            // Set the people from the server response
            return personResponse;
        } else {
            // The HTTP response status code indicates an error occurred, so print out the message from the HTTP response
            System.out.println("ERROR: " + http.getResponseMessage());
        }

        return null;
    }

    /**
     * Makes a request to the server to get the event data for the user, based on the authentication token.
     *
     * @return The encapsulated data holding all the event data for the user.
     * @throws Exception Throws an exception if the request fails for any reason.
     */
    private EventResponse getEvents() throws Exception {
        // Create a URL indicating where the server is running, and which web API operation we want to call
        URL url = new URL("http://" + serverHost + ":" + serverPort + "/event");

        // Start constructing our HTTP request
        HttpURLConnection http = (HttpURLConnection) url.openConnection();

        // Specify that we are sending an HTTP GET request
        http.setRequestMethod("GET");

        // Indicate that this request will not contain an HTTP request body
        http.setDoOutput(false);

        // Add an auth token to the request in the HTTP "Authorization" header
        http.addRequestProperty("Authorization", authToken);

        // Specify that we would like to receive the server's response in JSON
        http.addRequestProperty("Accept", "application/json");

        // Connect to the server and send the HTTP request
        http.connect();

        // Check to make sure that the HTTP response from the server contains a 200 status code, which means "success".  Treat anything else as a failure.
        if (http.getResponseCode() == HttpURLConnection.HTTP_OK) {
            // Get the input stream containing the HTTP response body
            InputStream respBody = http.getInputStream();

            // Extract JSON data from the HTTP response body
            String respData = readString(respBody);

            // Create an empty EventResponse to let the json converter use it
            EventResponse eventResponse = new EventResponse("");

            // Turn the JSON data into a EventResponse object
            eventResponse = (EventResponse) convertJsonToObject(respData, eventResponse);

            // Set the events from the server response
            return eventResponse;
        } else {
            // The HTTP response status code indicates an error occurred, so print out the message from the HTTP response
            System.out.println("ERROR: " + http.getResponseMessage());
        }
        return null;
    }

    /**
     * Reads an input stream to get the String value.
     *
     * @param is The input stream
     * @return The String holding the information in the input stream.
     * @throws IOException Throws an exception if there is a problem reading the input stream.
     */
    private String readString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader sr = new InputStreamReader(is);
        char[] buf = new char[1024];
        int len;
        while ((len = sr.read(buf)) > 0) {
            sb.append(buf, 0, len);
        }
        return sb.toString();
    }

    /**
     * Writes a String to an output stream.
     *
     * @param str The String to be written to the output stream.
     * @param os  The output stream to be written to.
     * @throws IOException Throws an exception if there is a problem writing to the output stream.
     */
    private void writeString(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }

}
