package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Fragments.MapFragment;

public class EventActivity extends AppCompatActivity {
    private static final String EVENT_KEY = "com.example.kaiden.familyhistoryserver.EventKey";
    private static final String OPTIONS_MENU_KEY = "com.example.kaiden.familyhistoryserver.OptionsMenuKey";
    private final boolean hasOptionsMenu = false;
    private String mEventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        // Display the up button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get the ID of the event to display
        mEventId = getIntent().getStringExtra(EVENT_KEY);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.event_fragment_container);

        if (fragment == null) {
            fragment = new MapFragment();
            // Supply event ID and whether to put an options menu as an argument.
            Bundle args = new Bundle();
            args.putString(EVENT_KEY, mEventId);
            args.putBoolean(OPTIONS_MENU_KEY, hasOptionsMenu);
            fragment.setArguments(args);
            fm.beginTransaction().add(R.id.event_fragment_container, fragment).commit();
        }
    }

    /**
     * Encapsulates the implementation of the event ID extra.
     *
     * @param packageContext The context of the activity.
     * @param eventId        The ID of the event to be displayed.
     * @return An intent to create an instance of the activity, with the event ID included as an extra.
     */
    public static Intent newIntent(Context packageContext, String eventId) {
        Intent intent = new Intent(packageContext, EventActivity.class);
        intent.putExtra(EVENT_KEY, eventId);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
