package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Adapters.EventFilterAdapter;

public class FilterActivity extends AppCompatActivity {
    private Switch mFathersSideFilter;
    private Switch mMothersSideFilter;
    private Switch mMaleEventsFilter;
    private Switch mFemaleSideFilter;

    private RecyclerView mEventFilterRecyclerView;
    private EventFilterAdapter mEventFilterAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        // Display the up button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get the model singleton
        Model model = Model.getModel();

        // Get the widgets for the filter activity and set the to their current value, as well as update the model when the user changes the value
        mFathersSideFilter = this.findViewById(R.id.fathers_side_filter);
        mFathersSideFilter.setChecked(model.showFathersSide());
        mFathersSideFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setFathersSideFilter(isChecked);
            }
        });

        mMothersSideFilter = this.findViewById(R.id.mothers_side_filter);
        mMothersSideFilter.setChecked(model.showMothersSide());
        mMothersSideFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setMothersSideFilter(isChecked);
            }
        });

        mMaleEventsFilter = this.findViewById(R.id.male_events_filter);
        mMaleEventsFilter.setChecked(model.showMaleEvents());
        mMaleEventsFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setMaleEventsFilter(isChecked);
            }
        });

        mFemaleSideFilter = this.findViewById(R.id.female_events_filter);
        mFemaleSideFilter.setChecked(model.showFemaleEvents());
        mFemaleSideFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setFemaleEventsFilter(isChecked);
            }
        });

        // Set up the recycler view with the possible event types
        mEventFilterRecyclerView = this.findViewById(R.id.event_type_filters);
        mEventFilterRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mEventFilterAdapter = new EventFilterAdapter(this, model.getEventTypeFilters());
        mEventFilterRecyclerView.setAdapter(mEventFilterAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
