package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Fragments.LoginFragment;
import com.example.kaiden.familyhistoryserver.UI.Fragments.MapFragment;

public class MainActivity extends AppCompatActivity {
    private static final String OPTIONS_MENU_KEY = "com.example.kaiden.familyhistoryserver.OptionsMenuKey";
    private static final String RESET_MAP_KEY = "com.example.kaiden.familyhistoryserver.ResetMapKey";
    private static final String RESET_EVENT_DISPLAY_DATA_KEY = "com.example.kaiden.familyhistoryserver.ResetDisplayDataKey";
    private final boolean hasOptionsMenu = true;
    private boolean mModelUpdated;

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Allow multiple threads
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Initialize the variable to show that the model hasn't been updated
        mModelUpdated = false;


        // Put in the login fragment if it isn't there already
        FragmentManager fm = getSupportFragmentManager();

        fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new LoginFragment();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }

    /**
     * Create new map fragment and replace the current (login) fragment with it. Adds the search, filter, and settings icons/links to the activity.
     */
    public void replaceFragment() {
        // Replace the current fragment with the map fragment
        fragment = new MapFragment();
        // Supply event ID as an argument.
        Bundle args = new Bundle();
        args.putBoolean(OPTIONS_MENU_KEY, hasOptionsMenu);
        fragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    /**
     * Allows callers to decide whether the map and event display data should be reset.
     * @param packageContext
     * @param resetMap True if the map should be reset, false otherwise.
     * @param resetEventDisplayData True if the event display data should be reset, false otherwise
     * @return An intent with the extras encapsulated in it.
     */
    public static Intent newIntent(Context packageContext, boolean resetMap, boolean resetEventDisplayData) {
        Intent intent = new Intent(packageContext, MainActivity.class);
        intent.putExtra(RESET_MAP_KEY, resetMap);
        intent.putExtra(RESET_EVENT_DISPLAY_DATA_KEY, resetEventDisplayData);
        return intent;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // Find if the map should be reset
        boolean resetMap = intent.getBooleanExtra(RESET_MAP_KEY, false);

        // Find if the event display data should be reset
        boolean resetEventDisplayData = intent.getBooleanExtra(RESET_EVENT_DISPLAY_DATA_KEY, false);

        // Reset the event data if necessary
        if (resetEventDisplayData) {
            ((MapFragment) fragment).resetEventData();
        }

        // Reset the map if necessary
        if (resetMap) {
            ((MapFragment) fragment).redrawMap();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search_action:
                startActivity(new Intent(this, SearchActivity.class));
                return true;

            case R.id.filters_action:
                mModelUpdated = true;
                startActivity(new Intent(this, FilterActivity.class));
                return true;

            case R.id.settings_action:
                mModelUpdated = true;
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isModelUpdated() {
        return mModelUpdated;
    }

    public void mapUpdated() {
        mModelUpdated = false;
    }

}
