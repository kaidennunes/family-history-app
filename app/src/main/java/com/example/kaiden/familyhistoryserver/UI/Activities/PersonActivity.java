package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import Models.Event;
import Models.Person;

public class PersonActivity extends AppCompatActivity {
    private static final String PERSON_KEY = "com.example.kaiden.familyhistoryserver.PersonKey";
    private String mPersonId;

    private TextView mFirstName;
    private TextView mLastName;
    private TextView mGenderName;

    private ExpandableListView mLifeEventsFamily;
    private ExpandableListAdapter mListAdapter;

    private List<String> mListDataHeader;
    private HashMap<String, List<String>> mListHash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        // Display the up button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get the widgets (views)
        mFirstName = findViewById(R.id.person_first_name);
        mLastName = findViewById(R.id.person_last_name);
        mGenderName = findViewById(R.id.person_gender);
        mLifeEventsFamily = findViewById(R.id.person_life_event_family_information);


        // Get the ID of the person to display
        mPersonId = getIntent().getStringExtra(PERSON_KEY);

        // Set the contents of the widgets with the person's information
        fillWidgetContent();
    }

    /**
     * Set the contents of the widgets with the person's information
     */
    private void fillWidgetContent() {
        // Get the data model
        Model model = Model.getModel();

        // Find the person to be displayed
        Person person = model.findPerson(mPersonId);

        // Set the name and gender of the person
        mFirstName.setText(person.getFirstName());
        mLastName.setText(person.getLastName());
        mGenderName.setText(getFullGenderText(person.getGender()));

        // Initialize the header information for the expandable list view
        mListDataHeader = new ArrayList<>();
        mListHash = new HashMap<>();
        mListDataHeader.add(getString(R.string.person_life_events));
        mListDataHeader.add(getString(R.string.person_family));


        // Get all of the events associated with the person (events are filtered)
        List<Event> events = model.getOrderedFilteredPersonEvents(mPersonId);

        // Initialize the list of life event information
        List<String> lifeEvents = new LinkedList<>();

        // Set the life event information of the person
        for (Event event : events) {
            lifeEvents.add(event.getEventId());
        }

        // Get the spouse, parents, and children of the person
        Person spouse = model.getSpouse(mPersonId);
        Person father = model.getFather(mPersonId);
        Person mother = model.getMother(mPersonId);
        List<Person> children = model.getChildren(mPersonId);

        // Initialize the list of family information
        List<String> familyInfo = new ArrayList<>();

        // Set the family information of the person
        if (spouse != null) {
            familyInfo.add(spouse.getPersonId());
        }

        if (father != null) {
            familyInfo.add(father.getPersonId());
        }

        if (mother != null) {
            familyInfo.add(mother.getPersonId());
        }

        if (children != null) {
            for (Person child : children) {
                familyInfo.add(child.getPersonId());
            }
        }

        // Put the headers into the list
        mListHash.put(mListDataHeader.get(0), lifeEvents);
        mListHash.put(mListDataHeader.get(1), familyInfo);

        // Set the expandable list view data with the data gathered from the person to be displayed
        mListAdapter = new com.example.kaiden.familyhistoryserver.UI.Widgets.ExpandableListAdapter(this, mListDataHeader, mListHash, person);
        mLifeEventsFamily.setAdapter(mListAdapter);
    }

    /**
     * Gets the full gender text of the given gender char abbreviation (ex. Male).
     *
     * @param genderChar The char that denotes the gender.
     * @return The full gender text (ex. Male).
     */
    private String getFullGenderText(char genderChar) {
        String gender;
        if (Character.toLowerCase(genderChar) == 'm') {
            gender = getString(R.string.gender_male);
        } else {
            gender = getString(R.string.gender_female);
        }
        return gender;
    }

    /**
     * Encapsulates the implementation of the person ID extra.
     *
     * @param packageContext The context of the activity.
     * @param personId       The ID of the person to be displayed.
     * @return An intent to create an instance of the activity, with the person ID included as an extra.
     */
    public static Intent newIntent(Context packageContext, String personId) {
        Intent intent = new Intent(packageContext, PersonActivity.class);
        intent.putExtra(PERSON_KEY, personId);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
