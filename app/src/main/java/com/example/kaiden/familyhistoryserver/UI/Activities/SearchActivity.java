package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Adapters.SearchResultAdapter;

public class SearchActivity extends AppCompatActivity {
    private SearchView searchBar;
    private RecyclerView searchResults;
    private SearchResultAdapter searchResultsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Get the widgets
        searchBar = findViewById(R.id.search_bar);
        searchResults = findViewById(R.id.search_results);

        // Set up the recycler view layout
        searchResults.setLayoutManager(new LinearLayoutManager(this));
        searchResultsAdapter = new SearchResultAdapter(this, Model.getModel().searchModelData(null));
        searchResults.setAdapter(searchResultsAdapter);


        // Set up the search bar to show the results on text change
        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                updateSearchResults(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    /**
     * Sets the items in the recycler view based on the search query.
     * @param query The query which the model uses to find events and people that matches the query.
     */
    private void updateSearchResults(String query) {
        searchResultsAdapter.updateSearchResults(Model.getModel().searchModelData(query));
        searchResultsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
