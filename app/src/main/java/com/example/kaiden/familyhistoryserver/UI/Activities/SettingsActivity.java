package com.example.kaiden.familyhistoryserver.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.example.kaiden.familyhistoryserver.Converters.ColorsConverter;
import com.example.kaiden.familyhistoryserver.Converters.MapConverter;

import com.example.kaiden.familyhistoryserver.Async.Interfaces.IAsync;
import com.example.kaiden.familyhistoryserver.Async.Tasks.FamilyHistorySynchronizer;
import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;
import com.example.kaiden.familyhistoryserver.R;

public class SettingsActivity extends AppCompatActivity implements IAsync {

    private Spinner mLifeStoryColor;
    private Switch mLifeStoryLines;
    private Spinner mFamilyTreeColor;
    private Switch mFamilyTreeLines;
    private Spinner mSpouseColor;
    private Switch mSpouseLines;

    private Spinner mMapType;

    private View mSyncData;
    private View mLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Model model = Model.getModel();

        // Initialize all of the setting variables
        mLifeStoryColor = this.findViewById(R.id.life_story_color);
        setSpinnerDefault(mLifeStoryColor, ColorsConverter.convertToColorString(this, model.getLifeStoryLineColor()), R.array.spinner_color_array);
        mLifeStoryColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Model.getModel().setLifeStoryLinesColor(ColorsConverter.convertToColorInt(getBaseContext(), selectedItem));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mLifeStoryLines = this.findViewById(R.id.life_story_lines);
        mLifeStoryLines.setChecked(model.showLifeStoryLines());
        mLifeStoryLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setLifeStoryLines(isChecked);
            }
        });

        mFamilyTreeColor = this.findViewById(R.id.family_tree_color);
        setSpinnerDefault(mFamilyTreeColor, ColorsConverter.convertToColorString(this, model.getFamilyTreeLineColor()), R.array.spinner_color_array);
        mFamilyTreeColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Model.getModel().setFamilyTreeLinesColor(ColorsConverter.convertToColorInt(getBaseContext(), selectedItem));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mFamilyTreeLines = this.findViewById(R.id.family_tree_lines);
        mFamilyTreeLines.setChecked(model.showFamilyTreeLines());
        mFamilyTreeLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setFamilyTreeLines(isChecked);
            }
        });

        mSpouseColor = this.findViewById(R.id.spouse_color);
        setSpinnerDefault(mSpouseColor, ColorsConverter.convertToColorString(this, model.getSpouseLineColor()), R.array.spinner_color_array);
        mSpouseColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Model.getModel().setSpouseLinesColor(ColorsConverter.convertToColorInt(getBaseContext(), selectedItem));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSpouseLines = this.findViewById(R.id.spouse_lines);
        mSpouseLines.setChecked(model.showSpouseLines());
        mSpouseLines.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Model.getModel().setSpouseLines(isChecked);
            }
        });


        mMapType = this.findViewById(R.id.map_type);
        setSpinnerDefault(mMapType, MapConverter.convertToMapTypeString(this, model.getGoogleMapDisplayType()), R.array.map_type_array);
        mMapType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                Model.getModel().setMapType(MapConverter.convertToMapTypeInt(getBaseContext(), selectedItem));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSyncData = this.findViewById(R.id.sync_data_constraint);
        mSyncData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sync the model data with the server data
                syncData();
            }
        });
        mLogout = this.findViewById(R.id.logout_constraint);
        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log the user out and send him back to the login screen
                logout();
            }
        });

    }

    private void setSpinnerDefault(Spinner spinner, String spinnerDefault, int spinnerArray) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, spinnerArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        if (spinnerDefault != null) {
            int spinnerPosition = adapter.getPosition(spinnerDefault);
            spinner.setSelection(spinnerPosition);
        }
    }

    /**
     * Logging out destroys the user’s session and returns them to the Main Activity. When it
     * is re-displayed, the Main Activity presents the LoginFragment Fragment so the user can re-login if they
     * desire.
     */
    private void logout() {
        Model.getModel().resetAllModelData();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Asynchronously re-syncs with the Family Map Server. This destroys any locally cached family data and
     * re-loads all required data from the Family Map Server. This is done without changing any of the
     * application settings.
     * If the re-sync fails, an error message (i.e., Android “toast”) is displayed, and the Settings
     * Activity remains open, thus allowing the user to try again.
     * <p>
     * If the re-sync succeeds, the user is returned to the Main Activity. When it is redisplayed,
     * the Main Activity presents the Map Fragment populated with the new data and no
     * event selected.
     */
    private void syncData() {
        // Try to synchronize the user data with the server data
        new FamilyHistorySynchronizer(this).execute(ServerProxy.getServerProxy());
    }

    @Override
    public void displayToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayToast(int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleteAsyncTask() {
        Intent intent = MainActivity.newIntent(this, true, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
