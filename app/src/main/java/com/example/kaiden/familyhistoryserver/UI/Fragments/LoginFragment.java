package com.example.kaiden.familyhistoryserver.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import Models.Person;
import Net.Requests.LoginRequest;
import Net.Requests.RegisterRequest;

import com.example.kaiden.familyhistoryserver.Async.Interfaces.IAsync;
import com.example.kaiden.familyhistoryserver.Async.Tasks.FamilyHistoryLogin;
import com.example.kaiden.familyhistoryserver.Async.Tasks.FamilyHistoryRegister;
import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Activities.MainActivity;

public class LoginFragment extends Fragment implements IAsync {

    private EditText mServerHost;
    private EditText mServerPort;
    private EditText mUserName;
    private EditText mUserPassword;
    private EditText mFirstName;
    private EditText mLastName;
    private EditText mEmail;

    private RadioGroup mGenders;

    private Button mSignIn;
    private Button mRegister;

    public LoginFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the view
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        // Get the widgets (views)
        mServerHost = v.findViewById(R.id.server_host);
        mServerPort = v.findViewById(R.id.server_port);
        mUserName = v.findViewById(R.id.user_name);
        mUserPassword = v.findViewById(R.id.user_password);
        mFirstName = v.findViewById(R.id.user_first_name);
        mLastName = v.findViewById(R.id.user_last_name);
        mEmail = v.findViewById(R.id.user_email);

        mGenders = v.findViewById(R.id.gender_radio_group);

        // Get and set event listeners for the sign-in and register buttons
        mSignIn = v.findViewById(R.id.sign_in_button);
        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sign in the user
                signInUser();
            }
        });

        mRegister = v.findViewById(R.id.register_button);
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Register the user
                registerUser();
            }
        });

        // Return the inflated view
        return v;
    }

    private void signInUser() {
        // Get the values needed to sign-in
        String serverHost = mServerHost.getText().toString();
        String serverPort = mServerPort.getText().toString();
        String userName = mUserName.getText().toString();
        String userPassword = mUserPassword.getText().toString();

        // Check to make sure all the required fields have data
        if (empty(serverHost) || empty(serverPort) || empty(userName) || empty(userPassword)) {
            Toast.makeText(getActivity(), R.string.fill_in_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        // Set the server port and host of the server proxy instance to make the register request to the server
        ServerProxy.setServerProxy(serverHost, serverPort);

        // Create a LoginRequest
        LoginRequest loginRequest = new LoginRequest(userName, userPassword);

        // Try to log in the user
        new FamilyHistoryLogin(this).execute(loginRequest);
    }

    private void registerUser() {
        // Get the values needed to register a user
        String serverHost = mServerHost.getText().toString();
        String serverPort = mServerPort.getText().toString();
        String userName = mUserName.getText().toString();
        String userPassword = mUserPassword.getText().toString();
        String userFirstName = mFirstName.getText().toString();
        String userLastName = mLastName.getText().toString();
        String userEmail = mEmail.getText().toString();
        char gender;

        // Find which radio button was selected, if any
        int radioButtonID = mGenders.getCheckedRadioButtonId();

        // Check to make sure all the required fields have data
        if (empty(serverHost) || empty(serverPort) || empty(userName) || empty(userPassword) || empty(userPassword) || empty(userFirstName) || empty(userLastName) || empty(userEmail) || radioButtonID == -1) {
            Toast.makeText(getActivity(), R.string.fill_in_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        // Find the selected gender of the user to register
        if (radioButtonID == R.id.male_radio_button) {
            gender = 'm';
        } else {
            gender = 'f';
        }

        // Set the server port and host of the server proxy instance to make the register request to the server
        ServerProxy.setServerProxy(serverHost, serverPort);

        // Create a RegisterRequest
        RegisterRequest registerRequest = new RegisterRequest(userName, userPassword, userEmail, userFirstName, userLastName, gender);

        // Try to register the user
        new FamilyHistoryRegister(this).execute(registerRequest);
    }

    @Override
    public void displayToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayToast(int stringId) {
        Toast.makeText(getActivity(), stringId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleteAsyncTask() {
        // Get the user from the model
        Person user = Model.getModel().getUser();

        // Get the user's full name
        String fullUserName = user.getFirstName() + " " + user.getLastName();

        // Make a toast containing the user's first and last name
        displayToast(fullUserName);

        ((MainActivity) getActivity()).replaceFragment();
    }

    private static boolean empty(final String s) {
        return s == null || s.trim().isEmpty();
    }
}
