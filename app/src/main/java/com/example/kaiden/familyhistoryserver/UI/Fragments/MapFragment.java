package com.example.kaiden.familyhistoryserver.UI.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Activities.MainActivity;
import com.example.kaiden.familyhistoryserver.UI.Activities.PersonActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import Models.Event;
import Models.Person;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String EVENT_KEY = "com.example.kaiden.familyhistoryserver.EventKey";
    private static final String OPTIONS_MENU_KEY = "com.example.kaiden.familyhistoryserver.OptionsMenuKey";
    private static final float BASE_FAMILY_TREE_THICKNESS = 10f;
    private static final float FAMILY_TREE_LINES_WIDTH_DECREASE_PER_GENERATION = 2f;
    private GoogleMap mMap;
    private TextView mEventDetails;
    private String mClickedPersonId;
    private String mEventId;

    private List<Polyline> relationshipLines;

    public MapFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        // Initialize a boolean variable to decide whether to show the options menu in this fragment
        boolean hasOptionsMenu = false;

        // Get any possible arguments for the map (an event ID to center on, for example)
        Bundle args = getArguments();
        if (args != null) {
            mEventId = args.getString(EVENT_KEY);
            hasOptionsMenu = args.getBoolean(OPTIONS_MENU_KEY);
        }

        // Either add or don't add the options menu, depending on the arguments
        setHasOptionsMenu(hasOptionsMenu);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Get the text view to display any event data and set an event listener to initiate the person activity when event data is clicked
        mEventDetails = v.findViewById(R.id.map_event_details);
        mEventDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = PersonActivity.newIntent(getActivity(), mClickedPersonId);
                startActivity(intent);
            }
        });
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void resetEventData() {
        mEventDetails.setText("");
        mEventDetails.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_android, 0, 0, 0);
    }

    /**
     * Resets and redraws everything on the google map.
     */
    public void redrawMap() {
        // Clear out the map
        mMap.clear();

        // Redraw the map
        setMap();
    }

    /**
     * Fills the google map with markers from the user's family history.
     */
    private void setMap() {
        // Get the model
        Model model = Model.getModel();

        // Set the display type of the map
        mMap.setMapType(model.getGoogleMapDisplayType());

        // For all the filtered events, add a marker for them, with the appropriate color. The markers themselves store the ID of each event
        for (Event event : model.getAllFilteredEvents()) {
            LatLng newMarker = new LatLng(event.getLatitude(), event.getLongitude());
            mMap.addMarker(new MarkerOptions().position(newMarker).title(event.getEventId())).setIcon(BitmapDescriptorFactory.defaultMarker(model.getEventColor(event)));
        }

        // Set the list of marker lines to an empty list
        relationshipLines = new ArrayList<>();

        // Get the ID of the active event
        String activeEventId = (String) mEventDetails.getTag();

        // If there is an active event, redraw the marker lines
        if (activeEventId != null) {
            drawMarkerLines(model.findEvent(activeEventId));
        }
    }

    private void drawMarkerLines(Event event) {
        // Remove all marker lines
        for (Polyline line : relationshipLines) {
            line.remove();
        }

        // Set the list of marker lines to an empty list
        relationshipLines = new ArrayList<>();

        // Get the model
        Model model = Model.getModel();

        // Draw the appropriate relationship lines if those lines should be showing
        if (model.showSpouseLines()) {
            // Get the best spouse event to link to the selected event
            Event spouseEvent = model.getBestSpouseEvent(event);

            // If there is some spouse event to link the selected event to, draw a line between them
            if (spouseEvent != null) {
                drawSpouseLines(event, spouseEvent, model.getSpouseLineColor());
            }
        }

        if (model.showFamilyTreeLines()) {
            drawFamilyTreeLines(event, BASE_FAMILY_TREE_THICKNESS, model.getFamilyTreeLineColor());
        }

        if (model.showLifeStoryLines()) {
            List<Event> orderedFilteredPersonEvents = model.getOrderedFilteredPersonEvents(event.getPerson());
            drawLifeStoryLines(orderedFilteredPersonEvents, model.getLifeStoryLineColor());
        }
    }

    private void drawSpouseLines(Event event, Event spouseEvent, int lineColor) {
        PolylineOptions options = new PolylineOptions().width(5).color(lineColor).geodesic(true);

        // Add the person event as the beginning point
        LatLng personPoint = new LatLng(event.getLatitude(), event.getLongitude());
        options.add(personPoint);

        // Add the spouse event as the ending point
        LatLng spousePoint = new LatLng(spouseEvent.getLatitude(), spouseEvent.getLongitude());
        options.add(spousePoint);

        // Add the line to the map and keep track of the added line in a list of lines
        relationshipLines.add(mMap.addPolyline(options));
    }

    private void drawFamilyTreeLines(Event currentEvent, float lineWidth, int lineColor) {
        // Get the data model
        Model model = Model.getModel();

        // Get the person associated with the event
        Person person = model.findPerson(currentEvent.getPerson());

        // Convert the person's event to a google map latitude and longitude object
        LatLng personPoint = new LatLng(currentEvent.getLatitude(), currentEvent.getLongitude());

        // Get the father's ID
        String fatherId = person.getFather();

        // If the person doesn't have a father, skip this part and move on
        if (fatherId != null) {
            // Get the father's filtered and ordered events
            List<Event> fatherEvents = Model.getModel().getOrderedFilteredPersonEvents(fatherId);

            // If the father has any events to show, link the current event to the best one of the father's events
            if (fatherEvents.size() > 0) {
                // Draw a line between the current event and the birth event of the father of person associated with the current event, putting the current event as the beginning point
                PolylineOptions options = new PolylineOptions().width(lineWidth).color(lineColor).geodesic(true);
                options.add(personPoint);

                // Get the first event in the ordered list
                Event fatherEvent = fatherEvents.get(0);

                // Add the father's event as the ending point
                LatLng motherPoint = new LatLng(fatherEvent.getLatitude(), fatherEvent.getLongitude());
                options.add(motherPoint);

                // Add the line to the map and keep track of the added line in a list of lines
                relationshipLines.add(mMap.addPolyline(options));

                // Recursively do the same thing for the father's family tree lines
                drawFamilyTreeLines(fatherEvent, lineWidth - FAMILY_TREE_LINES_WIDTH_DECREASE_PER_GENERATION, lineColor);
            }
        }

        // Get the mother's ID
        String motherId = person.getMother();

        // If the person doesn't have a mother, skip this part and move on
        if (motherId != null) {
            // Get the mother's filtered and ordered events
            List<Event> motherEvents = Model.getModel().getOrderedFilteredPersonEvents(motherId);

            // If the mother has any events to show, link the current event to the best one of the mother's events
            if (motherEvents.size() > 0) {
                // Draw a line between the current event and the birth event of the mother of person associated with the current event, putting the current event as the beginning point
                PolylineOptions options = new PolylineOptions().width(lineWidth).color(lineColor).geodesic(true);
                options.add(personPoint);

                // Get the first event in the ordered list
                Event motherEvent = motherEvents.get(0);

                // Add the mother's event as the ending point
                LatLng motherPoint = new LatLng(motherEvent.getLatitude(), motherEvent.getLongitude());
                options.add(motherPoint);

                // Add the line to the map and keep track of the added line in a list of lines
                relationshipLines.add(mMap.addPolyline(options));

                // Recursively do the same thing for the father's family tree lines
                drawFamilyTreeLines(motherEvent, lineWidth - FAMILY_TREE_LINES_WIDTH_DECREASE_PER_GENERATION, lineColor);
            }
        }
    }

    private void drawLifeStoryLines(List<Event> orderedFilteredPersonEvents, int lineColor) {
        PolylineOptions options = new PolylineOptions().width(5).color(lineColor).geodesic(true);

        // For each event, add a polyline to it
        for (Event event : orderedFilteredPersonEvents) {
            LatLng personPoint = new LatLng(event.getLatitude(), event.getLongitude());
            options.add(personPoint);
        }

        // Add the line to the map and keep track of the added line in a list of lines
        relationshipLines.add(mMap.addPolyline(options));
    }

    /**
     * Changes the icon for the event data to be either male or female.
     *
     * @param isMale Whether the icon should be male or female.
     */
    private void changeGenderIcon(boolean isMale) {
        // Get the ID for the appropriate gender icon
        int genderIcon;
        if (isMale) {
            genderIcon = R.drawable.ic_men;
        } else {
            genderIcon = R.drawable.ic_female;
        }

        // Draw the gender icon
        mEventDetails.setCompoundDrawablesWithIntrinsicBounds(genderIcon, 0, 0, 0);
    }

    /**
     * Displays the data from an event
     *
     * @param eventId The ID of the event to be displayed
     */
    private void displayEventData(String eventId) {
        // Get the model data
        Model model = Model.getModel();

        // Find the event that needs to be displayed
        Event event = model.findEvent(eventId);

        // If it is null for some reason, don't try to display it
        if (event == null) {
            return;
        }

        // Find the person associated with the event
        Person person = model.findPerson(event.getPerson());

        // Set the ID of the person to be used later if the user wishes to view the person activity
        mClickedPersonId = person.getPersonId();

        // If that person doesn't exist somehow, don't try to display the event data
        if (person == null) {
            return;
        }

        // Get the name of the person, the event type, the city and country, as well as the year
        String eventData = person.getFirstName() + " " + person.getLastName() + "\n" + event.getEventType() + ": " + event.getCity() + ", " + event.getCountry() + " (" + event.getYear() + ")";

        // Put that information into the text view to display to the user
        mEventDetails.setText(eventData);

        // Set a tag for the event details to grab later if necessary
        mEventDetails.setTag(eventId);

        // Find the gender of the person
        char gender = person.getGender();

        // Find out whether the person associated with the event is male or female
        boolean isMale;
        if (Character.toLowerCase(gender) == 'm') {
            isMale = true;
        } else {
            isMale = false;
        }

        // Change the gender icon according to the gender of the person
        changeGenderIcon(isMale);

        // Draw the relationship lines between event markers
        drawMarkerLines(event);
    }

    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (activity.getClass() != MainActivity.class) {
            return;
        }
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.isModelUpdated();
        if (mainActivity.isModelUpdated()) {
            // Redraw the map
            redrawMap();
        }
        mainActivity.mapUpdated();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Get the model
        final Model model = Model.getModel();
        // Set the google map and override event listener for clicking markers
        mMap = googleMap;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                LatLng newMarker = new LatLng(model.findEvent(marker.getTitle()).getLatitude(), model.findEvent(marker.getTitle()).getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLng(newMarker));

                // Get the event ID
                String eventId = marker.getTitle();

                // Display the event data below the map
                displayEventData(eventId);
                return true;
            }
        });

        // Fill up the google map with the user's family history data.
        setMap();

        // Center the map on the event with the event ID that was passed as an argument (if any)
        if (mEventId != null) {
            Event event = model.findEvent(mEventId);
            LatLng newMarker = new LatLng(event.getLatitude(), event.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLng(newMarker));

            // Display the event data below the map
            displayEventData(mEventId);
        }
    }
}
