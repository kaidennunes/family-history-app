package com.example.kaiden.familyhistoryserver.UI.Widgets;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Activities.EventActivity;
import com.example.kaiden.familyhistoryserver.UI.Activities.PersonActivity;

import java.util.HashMap;
import java.util.List;

import Models.Event;
import Models.Person;

/**
 * Created by Kaiden on 3/25/2018.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listHashMap;
    private Person mMainPerson;

    public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<String>> listHashMap, Person mainPerson) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listHashMap = listHashMap;
        this.mMainPerson = mainPerson;
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listHashMap.get(listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group, null);
        }
        TextView listHeader = convertView.findViewById(R.id.expandable_list_header);
        listHeader.setText(headerTitle);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        Model model = Model.getModel();
        final String eventOrPersonId = (String) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);
        }
        TextView listChild = convertView.findViewById(R.id.list_item);
        Person person = model.findPerson(eventOrPersonId);
        if (person != null) {
            setPersonViewInfo(person, listChild);
        } else {
            setEventViewInfo(model.findEvent(eventOrPersonId), listChild);
        }
        return convertView;
    }

    private void setPersonViewInfo(final Person person, TextView listChild) {
        // Get the basic information of the person
        StringBuilder personInfo = new StringBuilder(person.getFirstName() + " " + person.getLastName() + "\n");

        // Depending on whether it is a spouse, father, mother, or child, set the appropriate label for the person
        if (mMainPerson.getSpouse() != null && mMainPerson.getSpouse().equals(person.getPersonId())) {
            personInfo.append(context.getString(R.string.person_spouse));
        } else if (mMainPerson.getFather() != null && mMainPerson.getFather().equals(person.getPersonId())) {
            personInfo.append(context.getString(R.string.person_father));
        } else if (mMainPerson.getMother() != null && mMainPerson.getMother().equals(person.getPersonId())) {
            personInfo.append(context.getString(R.string.person_mother));
        } else {
            personInfo.append(context.getString(R.string.person_child));
        }

        // Set the text of the view
        listChild.setText(personInfo);

        // Set the image for the person view, depending on its gender
        if (Character.toLowerCase(person.getGender()) == 'm') {
            listChild.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_men, 0, 0, 0);
        } else {
            listChild.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_female, 0, 0, 0);
        }

        // Set the click event for the view
        listChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = PersonActivity.newIntent(context, person.getPersonId());
                context.startActivity(intent);
            }
        });
    }

    private void setEventViewInfo(final Event event, TextView listChild) {
        StringBuilder eventInfo = new StringBuilder(event.getEventType() + ": " + event.getCity() + ", " + event.getCountry() + " (" + event.getYear() + ")\n" + mMainPerson.getFirstName() + " " + mMainPerson.getLastName());
        listChild.setText(eventInfo);
        listChild.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_room_black_24dp, 0, 0, 0);

        // Set the click event for the view
        listChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = EventActivity.newIntent(context, event.getEventId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
