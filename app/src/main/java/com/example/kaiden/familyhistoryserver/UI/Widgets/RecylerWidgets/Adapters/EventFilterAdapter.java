package com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.kaiden.familyhistoryserver.Models.FilterModels.EventTypeFilter;
import com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Holders.EventFilterHolder;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Kaiden on 4/12/2018.
 */

public class EventFilterAdapter extends RecyclerView.Adapter<EventFilterHolder> {
    private List<EventTypeFilter> mEventTypeFilters;
    private WeakReference<Context> mContext;

    public EventFilterAdapter(Context context, Map<String, Boolean> eventTypes) {
        // Convert the map to
        mEventTypeFilters = new LinkedList<>();
        for (Map.Entry<String, Boolean> entry : eventTypes.entrySet()) {
            mEventTypeFilters.add(new EventTypeFilter(entry.getKey(), entry.getValue()));
        }

        // Sort the event types filters
        Collections.sort(mEventTypeFilters);

        // Get a weak reference to the request context
        mContext = new WeakReference<>(context);
    }

    @Override
    public EventFilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext.get());
        return new EventFilterHolder(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(EventFilterHolder holder, int position) {
        // Get the appropriate event type filter
        EventTypeFilter eventTypeFilter = mEventTypeFilters.get(position);

        // Get the values of the event type filter
        String eventType = eventTypeFilter.getEventType();
        boolean filterEvent = eventTypeFilter.isFilterEvent();

        // Bind the view holder to the data of the appropriate event type filter
        holder.bind(mContext.get(), eventType, filterEvent);
    }

    @Override
    public int getItemCount() {
        return mEventTypeFilters.size();
    }
}
