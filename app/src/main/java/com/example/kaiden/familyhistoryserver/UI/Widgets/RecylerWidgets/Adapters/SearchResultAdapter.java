package com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResult;
import com.example.kaiden.familyhistoryserver.R;
import com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Holders.SearchResultHolder;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by Kaiden on 4/12/2018.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultHolder> {
    private List<SearchResult> mSearchResults;
    private WeakReference<Context> mContext;

    public SearchResultAdapter(Context context, List<SearchResult> searchResults) {
        mSearchResults = searchResults;
        mContext = new WeakReference<>(context);
    }

    public void updateSearchResults(List<SearchResult> searchResults) {
        mSearchResults = searchResults;
    }

    @Override
    public SearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);
        LayoutInflater layoutInflater = LayoutInflater.from(mContext.get());
        SearchResultHolder searchResultHolder = new SearchResultHolder(layoutInflater, parent, v);

        return searchResultHolder;
    }

    @Override
    public void onBindViewHolder(SearchResultHolder holder, int position) {
        SearchResult searchResult = mSearchResults.get(position);
        holder.bind(searchResult);
    }

    @Override
    public int getItemCount() {
        return mSearchResults.size();
    }
}
