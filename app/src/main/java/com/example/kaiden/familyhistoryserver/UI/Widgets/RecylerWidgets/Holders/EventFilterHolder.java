package com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.R;

/**
 * Created by Kaiden on 4/12/2018.
 */

public class EventFilterHolder extends RecyclerView.ViewHolder {
    private TextView mEventFilterHeader;
    private TextView mEventFilterDescription;
    private Switch mEventFilter;

    private String mEventType;
    private boolean mEventTypeFilter;

    public EventFilterHolder(LayoutInflater inflater, ViewGroup parent) {
        super(inflater.inflate(R.layout.list_item_event_filter, parent, false));

        // Get the widgets for the view holder
        mEventFilterHeader = itemView.findViewById(R.id.event_filter_header);
        mEventFilterDescription = itemView.findViewById(R.id.event_filter_description);
        mEventFilter = itemView.findViewById(R.id.event_filter);
    }

    public void bind(Context context, String eventType, boolean eventTypeFilter) {
        // Set the values needed for the view holder into properties
        mEventType = eventType;
        mEventTypeFilter = eventTypeFilter;

        // Calculate the needed values to be inserted into the widgets
        StringBuilder eventTypeFilterHeader = new StringBuilder();
        eventTypeFilterHeader.append(eventType.substring(0, 1).toUpperCase());
        eventTypeFilterHeader.append(eventType.substring(1));
        eventTypeFilterHeader.append(" ");
        eventTypeFilterHeader.append(context.getResources().getString(R.string.events));

        StringBuilder eventTypeFilterDescription = new StringBuilder();
        eventTypeFilterDescription.append(context.getResources().getString(R.string.filter_by));
        eventTypeFilterDescription.append(" ");
        eventTypeFilterDescription.append(eventType.substring(0, 1).toUpperCase());
        eventTypeFilterDescription.append(eventType.substring(1));
        eventTypeFilterDescription.append(" ");
        eventTypeFilterDescription.append(context.getResources().getString(R.string.events));

        // Insert those values into the widgets
        mEventFilterHeader.setText(eventTypeFilterHeader.toString());
        mEventFilterDescription.setText(eventTypeFilterDescription.toString());
        mEventFilter.setChecked(eventTypeFilter);

        // Set the on click listener for the switch
        mEventFilter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mEventTypeFilter = isChecked;
                Model.getModel().setEventTypeFilter(mEventType, isChecked);
            }
        });
    }
}

