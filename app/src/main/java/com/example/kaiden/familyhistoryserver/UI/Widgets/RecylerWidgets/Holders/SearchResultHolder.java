package com.example.kaiden.familyhistoryserver.UI.Widgets.RecylerWidgets.Holders;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResult;
import com.example.kaiden.familyhistoryserver.R;

/**
 * Created by Kaiden on 4/12/2018.
 */

public class SearchResultHolder extends RecyclerView.ViewHolder {
    private SearchResult mSearchResult;
    private TextView mSearchResultView;

    public SearchResultHolder(LayoutInflater inflater, ViewGroup parent, View view) {
        super(inflater.inflate(R.layout.list_item, parent, false));

        // Get the widget for the view holder
        mSearchResultView = itemView.findViewById(R.id.list_item);

        // When clicked on, launch the activity based on the search result type
        mSearchResultView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSearchResult.launchActivity(mSearchResultView.getContext());
            }
        });
    }

    /**
     * Binds the search result to the view holder.
     *
     * @param searchResult The SearchResult object to be bound to the view holder.
     */
    public void bind(SearchResult searchResult) {
        mSearchResult = searchResult;
        mSearchResultView.setText(searchResult.getDisplayText());
        mSearchResultView.setCompoundDrawablesWithIntrinsicBounds(searchResult.getDrawable(), 0, 0, 0);
    }

}
