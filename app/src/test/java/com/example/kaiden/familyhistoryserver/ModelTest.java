package com.example.kaiden.familyhistoryserver;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.Models.SearchModels.SearchResult;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.UUID;

import Models.Event;
import Models.Person;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by Kaiden on 3/15/2018.
 */

public class ModelTest {
    private final static int NUMBER_OF_PEOPLE = 7;
    private final static int NUMBER_OF_MALE_PEOPLE = 4;
    private final static int NUMBER_OF_FEMALE_PEOPLE = 3;
    private final static int NUMBER_OF_FATHER_SIDE_PEOPLE_WITH_USER = 4;
    private final static int NUMBER_OF_MOTHER_SIDE_PEOPLE_WITH_USER = 4;
    private final static int NUMBER_OF_EVENTS_PER_PERSON = 3;
    private final static int NUMBER_OF_EVENTS = NUMBER_OF_EVENTS_PER_PERSON * NUMBER_OF_PEOPLE;
    private final static int NUMBER_OF_BIRTH_EVENTS = 2;


    private final static String BIRTH_EVENT_TYPE = "Birth";
    private final static String BAPTISM_EVENT_TYPE = "Baptism";
    private final static String CHRISTENING_EVENT_TYPE = "Christening";
    private final static String DEATH_EVENT_TYPE = "Death";
    private final static String TEST_EVENT_TYPE = "Test";

    private Model model;
    private Person user;
    private Event[] events;
    private Person[] people;

    @Before
    public void setModel() {
        // Initialize the user and the test data data structures
        user = new Person("user", "user", "userFirstName", "userLastName", 'm', "person1", "person2", null);
        people = new Person[NUMBER_OF_PEOPLE];
        events = new Event[NUMBER_OF_EVENTS];


        // Set test person data. Create all the people needed
        Person person1 = new Person("person1", user.getPersonId(), "firstName1", "lastName1", 'm', "person3", "person4", "person2");
        Person person2 = new Person("person2", user.getPersonId(), "firstName2", "lastName2", 'f', "person5", "person6", "person1");
        Person person3 = new Person("person3", user.getPersonId(), "firstName3", "lastName3", 'm', "", "", "");
        Person person4 = new Person("person4", user.getPersonId(), "firstName4", "lastName4", 'f', "", "", "");
        Person person5 = new Person("person5", user.getPersonId(), "firstName5", "lastName5", 'm', "", "", "person6");
        Person person6 = new Person("person6", user.getPersonId(), "firstName6", "lastName6", 'f', "", "", "person5");

        // Add the people data to the array (with the user)
        people[0] = user;
        people[1] = person1;
        people[2] = person2;
        people[3] = person3;
        people[4] = person4;
        people[5] = person5;
        people[6] = person6;

        // Set test event data. Create all the events needed
        Event userEvent1 = new Event("event1", user.getPersonId(), user.getPersonId(), 123, 234, "Country1", "", BAPTISM_EVENT_TYPE, 1900);
        Event userEvent2 = new Event("event1", user.getPersonId(), user.getPersonId(), 123, 234, "Country1", "City1", CHRISTENING_EVENT_TYPE, 2000);
        Event userEvent3 = new Event("event1", user.getPersonId(), user.getPersonId(), 123, 234, "Country1", "", DEATH_EVENT_TYPE, 1800);

        Event person1Event1 = new Event("event2", user.getPersonId(), person1.getPersonId(), 123, 234, "Country2", "", BIRTH_EVENT_TYPE, 1800);
        Event person1Event2 = new Event("event2", user.getPersonId(), person1.getPersonId(), 123, 234, "Country2", "", BAPTISM_EVENT_TYPE, 1900);
        Event person1Event3 = new Event("event2", user.getPersonId(), person1.getPersonId(), 123, 234, "", "", DEATH_EVENT_TYPE, 2000);

        Event person2Event1 = new Event("event3", user.getPersonId(), person2.getPersonId(), 123, 234, "Country1", "City3", DEATH_EVENT_TYPE, 1900);
        Event person2Event2 = new Event("event3", user.getPersonId(), person2.getPersonId(), 123, 234, "", "City3", CHRISTENING_EVENT_TYPE, 1888);
        Event person2Event3 = new Event("event3", user.getPersonId(), person2.getPersonId(), 123, 234, "", "City3", BAPTISM_EVENT_TYPE, 1888);

        Event person3Event1 = new Event("event4", user.getPersonId(), person3.getPersonId(), 123, 234, "", "", BIRTH_EVENT_TYPE, 1900);
        Event person3Event2 = new Event("event4", user.getPersonId(), person3.getPersonId(), 123, 234, "firstCountry", "", TEST_EVENT_TYPE, 1800);
        Event person3Event3 = new Event("event4", user.getPersonId(), person3.getPersonId(), 123, 234, "", "", BAPTISM_EVENT_TYPE, 1900);

        Event person4Event1 = new Event("event5", user.getPersonId(), person4.getPersonId(), 123, 234, "", "City5", TEST_EVENT_TYPE, 1900);
        Event person4Event2 = new Event("event5", user.getPersonId(), person4.getPersonId(), 123, 234, "", "", TEST_EVENT_TYPE, 1900);
        Event person4Event3 = new Event("event5", user.getPersonId(), person4.getPersonId(), 123, 234, "", "", TEST_EVENT_TYPE, 1900);

        Event person5Event1 = new Event("event6", user.getPersonId(), person5.getPersonId(), 123, 234, "", "", TEST_EVENT_TYPE, 1900);
        Event person5Event2 = new Event("event6", user.getPersonId(), person5.getPersonId(), 123, 234, "Country6", "", TEST_EVENT_TYPE, 1900);
        Event person5Event3 = new Event("event6", user.getPersonId(), person5.getPersonId(), 123, 234, "", "", TEST_EVENT_TYPE, 1850);

        Event person6Event1 = new Event("event7", user.getPersonId(), person6.getPersonId(), 1233, 234, "", "", TEST_EVENT_TYPE, 1800);
        Event person6Event2 = new Event("event7", user.getPersonId(), person6.getPersonId(), 1235, 234, "", "", TEST_EVENT_TYPE, 1900);
        Event person6Event3 = new Event("event7", user.getPersonId(), person6.getPersonId(), 1231, 234, "", "", TEST_EVENT_TYPE, 1900);


        // Add the event data to the array
        events[0] = userEvent1;
        events[1] = userEvent2;
        events[2] = userEvent3;

        events[3] = person1Event1;
        events[4] = person1Event2;
        events[5] = person1Event3;

        events[6] = person2Event1;
        events[7] = person2Event2;
        events[8] = person2Event3;

        events[9] = person3Event1;
        events[10] = person3Event2;
        events[11] = person3Event3;

        events[12] = person4Event1;
        events[13] = person4Event2;
        events[14] = person4Event3;

        events[15] = person5Event1;
        events[16] = person5Event2;
        events[17] = person5Event3;

        events[18] = person6Event1;
        events[19] = person6Event2;
        events[20] = person6Event3;

        // Store the model
        model = Model.getModel();

        // Reset all the model data, including filters and settings
        model.resetAllModelData();

        // Load the data into the model
        model.loadData(user, events, people);
    }

    @Test
    public void TestRelationships() {
        // Get the children of the user (should be none)
        List<Person> children = model.getChildren(people[0].getPersonId());

        // Make sure there are no children (should be null)
        assertNull(children);

        // Get the children of the person
        children = model.getChildren(people[1].getPersonId());

        // Make sure the person has one child, which should be the user
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[0].getPersonId()));

        // Get the children of the person
        children = model.getChildren(people[2].getPersonId());

        // Make sure the person has one child, which should be the user
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[0].getPersonId()));


        // Get the children of the person
        children = model.getChildren(people[3].getPersonId());

        // Make sure the person has one child, which should be person 1
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[1].getPersonId()));


        // Get the children of the person
        children = model.getChildren(people[4].getPersonId());

        // Make sure the person has one child, which should be person 1
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[1].getPersonId()));


        // Get the children of the person
        children = model.getChildren(people[5].getPersonId());

        // Make sure the person has one child, which should be person 2
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[2].getPersonId()));


        // Get the children of the person
        children = model.getChildren(people[6].getPersonId());

        // Make sure the person has one child, which should be person 2
        assertTrue(children.size() == 1);
        assertTrue(children.get(0).getPersonId().equals(people[2].getPersonId()));


        // Get the best spouse events (meaning, the first ordered and unfiltered event in the spouse's list of events). This tests that the model correctly identifies spouses

        // This is person 1's event being used as a parameter, which should give us the first ordered and unfiltered event in person 2's list of events, if the spouse relationship is correctly handled in the model
        Event spouseEvent = model.getBestSpouseEvent(events[3]);

        // The event in the 8 index of the array corresponds to the earliest event for person 2
        assertTrue(spouseEvent.equals(events[8]));

        // This is person 2's event being used as a parameter (check to make sure the relationship work both ways)
        spouseEvent = model.getBestSpouseEvent(events[8]);

        // The event in the 3 index of the array corresponds to the earliest event for person 1
        assertTrue(spouseEvent.equals(events[3]));


        // This is person 5's event being used as a parameter, which should correspond to the best event for the spouse, person 6
        spouseEvent = model.getBestSpouseEvent(events[16]);

        // The event in the 18 index of the array corresponds to the earliest event for person 6
        assertTrue(spouseEvent.equals(events[18]));

        // This is person 6's event being used as a parameter (check to make sure the relationship work both ways)
        spouseEvent = model.getBestSpouseEvent(events[18]);

        // The event in the 17 index of the array corresponds to the earliest event for person 5
        assertTrue(spouseEvent.equals(events[17]));


        // This is person 4's event being used as a parameter. Person 4 has no spouse, so the returned event should be null
        spouseEvent = model.getBestSpouseEvent(events[14]);

        // The event in the 17 index of the array corresponds to the earliest event for person 5
        assertNull(spouseEvent);


        // Parents are not stored in the model, as it is simple to traverse the map using the mother and father ID's that each person has (or that is null, indicating the end of the tree)
    }

    @Test
    public void TestFilter() {
        // Get all filtered events (they should all be valid)
        List<Event> filteredEvents = model.getAllFilteredEvents();

        // The number of events created should be the same as the number when filtered, when all filters are on
        assertTrue(filteredEvents.size() == NUMBER_OF_EVENTS);

        // Filter the birth event type
        model.setEventTypeFilter(BIRTH_EVENT_TYPE, false);

        // Get the filtered events again
        filteredEvents = model.getAllFilteredEvents();

        // None of the filtered event types should have birth types
        for (Event filteredEvent : filteredEvents) {
            if (filteredEvent.getEventType().toLowerCase().equals(BIRTH_EVENT_TYPE.toLowerCase())) {
                fail();
            }
        }

        // Make sure we have the right number of events
        assertTrue(filteredEvents.size() == NUMBER_OF_EVENTS - NUMBER_OF_BIRTH_EVENTS);

        // Turn off all of the other event types
        model.setEventTypeFilter(BAPTISM_EVENT_TYPE, false);
        model.setEventTypeFilter(CHRISTENING_EVENT_TYPE, false);
        model.setEventTypeFilter(DEATH_EVENT_TYPE, false);
        model.setEventTypeFilter(TEST_EVENT_TYPE, false);


        // Get the filtered events again
        filteredEvents = model.getAllFilteredEvents();

        // There should be no events
        assertTrue(filteredEvents.size() == 0);

        // Turn all the event type filters back on
        model.setEventTypeFilter(BIRTH_EVENT_TYPE, true);
        model.setEventTypeFilter(BAPTISM_EVENT_TYPE, true);
        model.setEventTypeFilter(CHRISTENING_EVENT_TYPE, true);
        model.setEventTypeFilter(DEATH_EVENT_TYPE, true);
        model.setEventTypeFilter(TEST_EVENT_TYPE, true);

        // Turn off the male side
        model.setMaleEventsFilter(false);

        // Get the filtered events
        filteredEvents = model.getAllFilteredEvents();

        // Make sure we have the right number of events
        assertTrue(filteredEvents.size() == NUMBER_OF_FEMALE_PEOPLE * NUMBER_OF_EVENTS_PER_PERSON);

        // Turn on the male side, turn off the female side
        model.setMaleEventsFilter(true);
        model.setFemaleEventsFilter(false);

        // Get the filtered events
        filteredEvents = model.getAllFilteredEvents();

        // Make sure we have the right number of events
        assertTrue(filteredEvents.size() == NUMBER_OF_MALE_PEOPLE * NUMBER_OF_EVENTS_PER_PERSON);

        // Turn the female side back on
        model.setFemaleEventsFilter(true);

        // Turn off father's side
        model.setFathersSideFilter(false);

        // Get the filtered events
        filteredEvents = model.getAllFilteredEvents();

        // Make sure we have the right number of events
        assertTrue(filteredEvents.size() == NUMBER_OF_MOTHER_SIDE_PEOPLE_WITH_USER * NUMBER_OF_EVENTS_PER_PERSON);


        // Go through the filtered events and make sure all of the father side events are not present
        for (Event filteredEvent : filteredEvents) {
            if (filteredEvent.getPerson().equals(people[1].getPersonId())
                    || filteredEvent.getPerson().equals(people[3].getPersonId())
                    || filteredEvent.getPerson().equals(people[4].getPersonId())) {
                fail();
            }
        }

        // Turn on father's side and turn off mother's side
        model.setFathersSideFilter(true);
        model.setMothersSideFilter(false);

        // Get the filtered events
        filteredEvents = model.getAllFilteredEvents();

        // Make sure we have the right number of events
        assertTrue(filteredEvents.size() == NUMBER_OF_FATHER_SIDE_PEOPLE_WITH_USER * NUMBER_OF_EVENTS_PER_PERSON);

        // Go through the filtered events and make sure all of the mother side events are not present
        for (Event filteredEvent : filteredEvents) {
            if (filteredEvent.getPerson().equals(people[2].getPersonId())
                    || filteredEvent.getPerson().equals(people[5].getPersonId())
                    || filteredEvent.getPerson().equals(people[6].getPersonId())) {
                fail();
            }
        }
    }

    @Test
    public void TestSort() {
        // Get the user's person ID
        String personId = people[0].getPersonId();

        // Get the sorted and filtered events for that user (all filters are turned off)
        List<Event> sortedAndFilteredEvents = model.getOrderedFilteredPersonEvents(personId);

        // Make sure that his events are in the correct order
        assertTrue(sortedAndFilteredEvents.get(0).getEventType().equals(BAPTISM_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(1).getEventType().equals(CHRISTENING_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(2).getEventType().equals(DEATH_EVENT_TYPE));


        // Get the person's ID
        String personId1 = people[1].getPersonId();

        // Get the sorted and filtered events for that person (all filters are turned off)
        sortedAndFilteredEvents = model.getOrderedFilteredPersonEvents(personId1);

        // Make sure that the person's events are in the correct order
        assertTrue(sortedAndFilteredEvents.get(0).getEventType().equals(BIRTH_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(1).getEventType().equals(BAPTISM_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(2).getEventType().equals(DEATH_EVENT_TYPE));


        // Get the person's ID
        String personId2 = people[2].getPersonId();

        // Get the sorted and filtered events for that person (all filters are turned off)
        sortedAndFilteredEvents = model.getOrderedFilteredPersonEvents(personId2);

        // Make sure that the person's events are in the correct order
        assertTrue(sortedAndFilteredEvents.get(0).getEventType().equals(BAPTISM_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(1).getEventType().equals(CHRISTENING_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(2).getEventType().equals(DEATH_EVENT_TYPE));


        // Get the person's ID
        String personId3 = people[3].getPersonId();

        // Get the sorted and filtered events for that person (all filters are turned off)
        sortedAndFilteredEvents = model.getOrderedFilteredPersonEvents(personId3);

        // Make sure that the person's events are in the correct order
        assertTrue(sortedAndFilteredEvents.get(0).getEventType().equals(BIRTH_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(1).getEventType().equals(TEST_EVENT_TYPE));
        assertTrue(sortedAndFilteredEvents.get(2).getEventType().equals(BAPTISM_EVENT_TYPE));
    }

    @Test
    public void TestSearch() {
        // (Only baptism and death events should relate to this). Testing general search query
        String searchQuery1 = "a";
        int searchQuery1ResultSize = NUMBER_OF_PEOPLE + 7;

        // Test the expected query and result size
        testSearchQuery(searchQuery1, searchQuery1ResultSize);

        // This should match all the people and no events. Testing first name
        String searchQuery2 = "firstname";
        int searchQuery2ResultSize = NUMBER_OF_PEOPLE;

        // Test the expected query and result size
        testSearchQuery(searchQuery2, searchQuery2ResultSize);

        // This should match the person 2. Testing last name and capitalization
        String searchQuery3 = "lastname2";
        int searchQuery3ResultSize = 1;

        // Test the expected query and result size
        testSearchQuery(searchQuery3, searchQuery3ResultSize);

        // Testing country of event.
        String searchQuery4 = "Country1";
        int searchQuery4ResultSize = 4;

        // Test the expected query and result size
        testSearchQuery(searchQuery4, searchQuery4ResultSize);

        // Testing city of event
        String searchQuery5 = "city3";
        int searchQuery5ResultSize = 3;

        // Test the expected query and result size
        testSearchQuery(searchQuery5, searchQuery5ResultSize);

        // Testing description (event type) of event
        String searchQuery6 = BIRTH_EVENT_TYPE;
        int searchQuery6ResultSize = NUMBER_OF_BIRTH_EVENTS;

        // Test the expected query and result size
        testSearchQuery(searchQuery6, searchQuery6ResultSize);

        // Testing year of event
        String searchQuery7 = "1888";
        int searchQuery7ResultSize = 2;

        // Test the expected query and result size
        testSearchQuery(searchQuery7, searchQuery7ResultSize);

        // Testing another random but valid query
        String searchQuery8 = "first";
        int searchQuery8ResultSize = NUMBER_OF_PEOPLE + 1;

        // Test the expected query and result size
        testSearchQuery(searchQuery8, searchQuery8ResultSize);

        // Testing query that doesn't match anything
        String searchQuery9 = "unmatchablequery";
        int searchQuery9ResultSize = 0;

        // Test the expected query and result size
        testSearchQuery(searchQuery9, searchQuery9ResultSize);

    }

    private void testSearchQuery(String searchQuery, int expectedSearchQuerySize) {
        List<SearchResult> foundData = model.searchModelData(searchQuery);
        assertTrue(foundData.size() == expectedSearchQuerySize);
    }
}
