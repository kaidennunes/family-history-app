package com.example.kaiden.familyhistoryserver;

import Models.Event;
import Models.Person;
import Net.Requests.LoginRequest;
import Net.Requests.RegisterRequest;
import Net.Response.LoginResponse;
import Net.Response.RegisterResponse;

import com.example.kaiden.familyhistoryserver.Models.DataModels.Model;
import com.example.kaiden.familyhistoryserver.Proxy.ServerProxy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static junit.framework.Assert.*;
import static org.junit.Assert.assertArrayEquals;

/**
 * Tests the server proxy class
 * Created by Kaiden on 3/15/2018.
 */

public class ProxyTest {
    private final String SERVER_HOST = "127.0.0.1";
    private final String SERVER_PORT = "8000";
    private final int numberOfEventsGeneratedPerPerson = 3;

    @Test
    public void TestProxy() {
        // Set the server host and port for the proxy
        ServerProxy.setServerProxy(SERVER_HOST, SERVER_PORT);

        // Get the server proxy
        ServerProxy serverProxy = ServerProxy.getServerProxy();

        // Get a random username
        String userNameLogin = UUID.randomUUID().toString();

        // Create an invalid login request
        LoginRequest loginRequestInvalid = new LoginRequest(userNameLogin, "login1");

        // Try to illegally login a user (an unregistered user)
        LoginResponse loginResponseInvalid1 = serverProxy.login(loginRequestInvalid);

        // This should have failed
        assertTrue(loginResponseInvalid1.getHasError());

        // Create a register request
        RegisterRequest registerRequest = new RegisterRequest(userNameLogin, "password", "email", "firstName", "lastName", 'm');

        // Try to register using the proxy
        RegisterResponse registerResponse = serverProxy.register(registerRequest);

        // This should have no error
        assertFalse(registerResponse.getHasError());

        // Synchronize the event and people data from the database to the model
        serverProxy.synchronizeFamilyHistoryData();

        // Get a reference to the model
        Model model = Model.getModel();

        // Get the data from the model
        Map<String, Person> modelPeopleRegister = model.getPeople();
        Map<String, List<Event>> modelEventsRegister = model.getEvents();

        // Initialize the list of people and events
        List<Person> peopleRegister = new ArrayList<Person>();
        List<Event> eventsRegister = new ArrayList<Event>();

        // Add all of the people from the model to the list of people
        for (Map.Entry<String, Person> person : modelPeopleRegister.entrySet()) {
            peopleRegister.add(person.getValue());
        }

        // Add all of the events from the model to the list of events
        for (Map.Entry<String, List<Event>> eventList : modelEventsRegister.entrySet()) {
            // Each person should have a certain number of events
            assertTrue(eventList.getValue().size() == numberOfEventsGeneratedPerPerson);
            for (Event event : eventList.getValue()) {
                eventsRegister.add(event);
            }
        }

        // Try to illegally login a user (wrong password)
        LoginResponse loginResponseInvalid2 = serverProxy.login(loginRequestInvalid);

        // This should have failed
        assertTrue(loginResponseInvalid2.getHasError());

        // Create a valid login request
        LoginRequest loginRequestValid = new LoginRequest(registerRequest.getUserName(), registerRequest.getUserPassword());

        // Try to legally login a user using the proxy
        LoginResponse loginResponseValid = serverProxy.login(loginRequestValid);

        // This should be working
        assertFalse(loginResponseValid.getHasError());

        // The username and person ID of the login and the register information should be the same
        assertEquals(registerResponse.getUserName(), loginResponseValid.getUserName());
        assertEquals(registerResponse.getPersonId(), loginResponseValid.getPersonId());

        // Synchronize the event and people data from the database to the model
        boolean success1 = serverProxy.synchronizeFamilyHistoryData();

        // The synchronization should have been successful
        assertTrue(success1);

        // Get the data from the model
        Map<String, Person> modelPeopleLogin = model.getPeople();
        Map<String, List<Event>> modelEventsLogin = model.getEvents();

        // Initialize the list of people and events
        List<Person> peopleLogin = new ArrayList<Person>();
        List<Event> eventsLogin = new ArrayList<Event>();

        // Add all of the people from the model to the list of people
        for (Map.Entry<String, Person> person : modelPeopleLogin.entrySet()) {
            peopleLogin.add(person.getValue());
        }

        // Add all of the events from the model to the list of events
        for (Map.Entry<String, List<Event>> eventList : modelEventsLogin.entrySet()) {
            // Each person should have a certain number of events
            assertTrue(eventList.getValue().size() == numberOfEventsGeneratedPerPerson);
            for (Event event : eventList.getValue()) {
                eventsLogin.add(event);
            }
        }

        // The events from the login and the register should be the same
        assertEquals(eventsRegister, eventsLogin);

        // The people from the login and the register should be the same
        assertEquals(peopleRegister, peopleLogin);

        // Give the server proxy the wrong connection information
        serverProxy.setServerProxy("Invalid Host","Invalid Port");

        // Try to synchronize the event and people data from the database to the model
        boolean success2 = serverProxy.synchronizeFamilyHistoryData();

        // This should have failed
        assertFalse(success2);
    }
}
